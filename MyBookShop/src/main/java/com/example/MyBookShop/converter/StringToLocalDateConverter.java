package com.example.MyBookShop.converter;

import lombok.SneakyThrows;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
public class StringToLocalDateConverter implements Converter<String, LocalDate> {

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    @SneakyThrows
    @Override
    public LocalDate convert(String date) {
        if (date.isEmpty()) return null;

        date = date.trim();

        // если передано в формате 08.2020 или 8.2020
        if (date.matches("^(((0?)[1-9])|((1)[0-2]))(\\.)\\d{4}$")) {

            // если начинается с одной цифры, например 1.2020
            if (date.matches("^\\d{1}(\\.)\\d{4}$")) {
                return LocalDate.parse("01.0" + date, formatter);
            }

            // если начинается с двух цифр, например 01.2020 или 11.2020
            if (date.matches("^\\d{2}(\\.)\\d{4}$")) {
                return LocalDate.parse("01." + date, formatter);

            }
        }

        // если передано в формате 01.08.2020
        if (date.matches("^((0)[1-9]|[1-2][0-9]|(3)[0-1])(\\.)(((0)[1-9])|((1)[0-2]))(\\.)\\d{4}$")) {
            return LocalDate.parse(date, formatter);
        }

        throw new Exception("Неверный формат даты");
    }
}
