package com.example.MyBookShop.service;

import com.example.MyBookShop.data.entities.Author;
import com.example.MyBookShop.data.repositories.AuthorRepository;
import com.example.MyBookShop.web.dto.AuthorDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Set;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toSet;

@AllArgsConstructor
@Service
public class AuthorServiceImpl implements AuthorService {

    AuthorRepository authorRepository;
    ConvertAuthorStrategy convertAuthorStrategy;

    @Override
    public Set<AuthorDto> getAuthors() {
        return convertAuthorStrategy.convert(authorRepository.findAll());
    }

    @Override
    public AuthorDto getAuthorInfoById(Long id) {
        Author author = authorRepository.getOne(id);

        return convertAuthorStrategy.convert(author);
    }

    @Override
    public Map<String, Set<AuthorDto>> getAuthorsMap() {

        return getAuthors().stream()
                .collect(
                        groupingBy(author -> author.getFullName().substring(0, 1).toUpperCase(),
                                toSet()));
    }
}
