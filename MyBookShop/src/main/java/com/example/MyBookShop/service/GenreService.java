package com.example.MyBookShop.service;

import com.example.MyBookShop.web.dto.GenreDto;

import java.util.List;

public interface GenreService {

    List<GenreDto> getAllGenres();

    GenreDto getPaginatedGenreBySlug(String slug);
}
