package com.example.MyBookShop.service;

public interface RateService {

    void updateRate(Integer rate, String slug, Long userId, String comment);
}
