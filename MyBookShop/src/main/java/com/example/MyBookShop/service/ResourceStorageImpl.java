package com.example.MyBookShop.service;

import lombok.SneakyThrows;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class ResourceStorageImpl implements ResourceStorage {

    @Value("${upload.path}")
    String uploadPath;
    @Value("${books.path}")
    String bookPath;

    @SneakyThrows
    @Override
    public String saveNewBookImage(MultipartFile file, String slug) {

        String resourceUri = null;

        if (!file.isEmpty()) {

            if (!new File(Paths.get(uploadPath).toFile().getAbsolutePath()).exists()) {

                Files.createDirectory(Paths.get(uploadPath));
            }

            String fileName = slug + "." + FilenameUtils.getExtension(file.getOriginalFilename());

            Path path = Paths.get(uploadPath, fileName);
            resourceUri = uploadPath + "/" + fileName;
            file.transferTo(path);
        }

        return resourceUri;
    }
}
