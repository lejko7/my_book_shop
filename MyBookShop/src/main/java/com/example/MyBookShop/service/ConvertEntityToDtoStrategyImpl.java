package com.example.MyBookShop.service;

import com.example.MyBookShop.data.entities.*;
import com.example.MyBookShop.security.BookstoreUserDetails;
import com.example.MyBookShop.web.dto.*;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class ConvertEntityToDtoStrategyImpl implements ConvertEntityToDtoStrategy {

    @Override
    public BookDto convert(Book book) {
        return BookDto
                .builder()
                .id(book.getId())
                .author(AuthorShortDto.builder()
                        .id(book.getAuthor().getId())
                        .fullName(book.getAuthor().getSurname() + " " + book.getAuthor().getName())
                        .build())
                .isBestseller(book.isBestseller())
                .image(book.getImage())
                .publishDate(book.getPublishDate())
                .title(book.getTitle())
                .description(book.getDescription())
                .slug(book.getSlug())
                .price("$" + book.getPrice())
                .discountPrice("$" + getNewPrice(book.getPrice(), book.getDiscount()))
                .discount(book.getDiscount())
                .downloadCount(book.getDownloadCount())
                .genres(getGenres(book.getBookToGenres()))
                .tags(getTags(book.getBookToTags()))
                .bookFiles(book.getBookFiles().stream().map(this::convert).collect(Collectors.toList()))
                .rates(getRatesMap(book.getRates()))
                .rateCount((long) book.getRates().size())
                .rate(calculateRating(book.getRates()))
                .comments(getComments(book))
                .build();
    }

    private List<CommentDto> getComments(Book book) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long id;

        if (principal.equals("anonymousUser")) {
            id = null;
        } else {
            id = principal.getClass().equals(BookstoreUserDetails.class) ?
                    ((BookstoreUserDetails) principal).getUser().getId() : Long.parseLong(((DefaultOAuth2User) principal).getAttribute("id"));
        }

        return book.getComments().stream().map(comment -> {
            CommentDto commentDto = commentToDto(comment, book.getRates().stream()
                    .filter(rate -> rate.getUser() != null && rate.getUser().getId().equals(id))
                    .findFirst().orElse(null));
            commentDto.setRate(
                    calculateRating(
                            book.getRates().stream()
                                    .filter(rate -> rate.getUser() != null && rate.getUser().equals(comment.getUser()))
                                    .collect(Collectors.toList())));

            return commentDto;
        }).collect(Collectors.toList());
    }

    private CommentDto commentToDto(Comment comment, Rate rate) {
        return CommentDto.builder()
                .id(comment.getId())
                .comment(comment.getComment())
                .dateTime(comment.getDateTime())
                .userName(comment.getUser().getName() + " " + comment.getUser().getSurname())
                .rate(rate == null ? 0L : rate.getRate())
                .positiveRates(comment.getCommentRates().stream().filter(commentRate -> commentRate.getRate().equals(1L)).mapToLong(CommentRate::getRate).count())
                .negativeRates(comment.getCommentRates().stream().filter(commentRate -> commentRate.getRate() < (1L)).mapToLong(CommentRate::getRate).count())
                .build();
    }

    private Long calculateRating(List<Rate> rates) {

        if (rates.isEmpty()) {
            return 0L;
        }

        return rates.stream().mapToLong(Rate::getRate).sum() / rates.size();
    }

    private Map<Integer, Long> getRatesMap(List<Rate> rates) {
        Map<Integer, Long> ratesMap = new HashMap<>();


        ratesMap.put(1, rates.stream().filter(rate -> rate.getRate().equals(1)).count());
        ratesMap.put(2, rates.stream().filter(rate -> rate.getRate().equals(2)).count());
        ratesMap.put(3, rates.stream().filter(rate -> rate.getRate().equals(3)).count());
        ratesMap.put(4, rates.stream().filter(rate -> rate.getRate().equals(4)).count());
        ratesMap.put(5, rates.stream().filter(rate -> rate.getRate().equals(5)).count());

        return ratesMap;
    }

    @Override
    public Set<BookDto> convert(List<Book> books) {
        return books.stream().map(this::convert).collect(Collectors.toSet());
    }

    @Override
    public GenreDto convert(Genre genre) {
        return GenreDto.builder()
                .id(genre.getId())
                .genre(genre.getGenre())
                .slug(genre.getSlug())
                .subGenres(getSubGenres(genre.getSubGenres()))
                .books(convert(genre.getBookToGenres().stream().map(BookToGenre::getBook).collect(Collectors.toList())))
                .build();
    }

    @Override
    public TagDto convert(Tag tag) {

        return TagDto.builder()
                .id(tag.getId())
                .tag(tag.getTag())
                .slug(tag.getSlug())
                .books(getBooks(tag.getBookToTags()))
                .build();
    }

    @Override
    public BookFileDto convert(BookFile bookFile) {

        return BookFileDto.builder()
                .id(bookFile.getId())
                .hash(bookFile.getHash())
                .bookType(bookFile.getType().name())
                .path(bookFile.getPath())
                .build();
    }

    public TagShortDto convertToShort(Tag tag) {

        return TagShortDto.builder()
                .id(tag.getId())
                .tag(tag.getTag())
                .slug(tag.getSlug())
                .build();
    }

    private Set<BookDto> getBooks(List<BookToTag> bookToTags) {
        return convert(bookToTags.stream().map(BookToTag::getBook).collect(Collectors.toList()));
    }

    private List<GenreDto> getSubGenres(List<Genre> subGenres) {

        if (subGenres == null || subGenres.isEmpty()) {
            return new ArrayList<>();
        }

        List<GenreDto> genreDtos = new ArrayList<>();

        for (Genre subgenre : subGenres) {
            GenreDto genreDto = convert(subgenre);

            genreDtos.add(genreDto);
        }

        return genreDtos;
    }

    private List<TagShortDto> getTags(List<BookToTag> bookToTags) {
        return bookToTags.stream().map(bookToTag -> convertToShort(bookToTag.getTag())).collect(Collectors.toList());
    }

    private List<String> getGenres(List<BookToGenre> bookToGenres) {
        return bookToGenres.stream().map(bookToGenre -> bookToGenre.getGenre().getGenre()).collect(Collectors.toList());
    }

    private String getNewPrice(Double price, Integer discount) {

        return String.valueOf(roundDouble(price - price * discount / 100));
    }

    private double roundDouble(double value) {

        return Math.round(value * 100.0) / 100.0;
    }
}
