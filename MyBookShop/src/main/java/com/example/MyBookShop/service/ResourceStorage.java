package com.example.MyBookShop.service;

import org.springframework.web.multipart.MultipartFile;

public interface ResourceStorage {

    String saveNewBookImage(MultipartFile file, String slug);

}
