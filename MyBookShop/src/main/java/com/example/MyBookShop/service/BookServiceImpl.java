package com.example.MyBookShop.service;

import com.example.MyBookShop.data.entities.Author;
import com.example.MyBookShop.data.entities.Book;
import com.example.MyBookShop.data.entities.Genre;
import com.example.MyBookShop.data.entities.Tag;
import com.example.MyBookShop.data.repositories.AuthorRepository;
import com.example.MyBookShop.data.repositories.BookRepository;
import com.example.MyBookShop.data.repositories.GenreRepository;
import com.example.MyBookShop.data.repositories.TagRepository;
import com.example.MyBookShop.web.dto.BookDto;
import com.example.MyBookShop.web.dto.BooksPageDto;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@AllArgsConstructor
@Service
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class BookServiceImpl implements BookService {

    BookRepository bookRepository;
    ConvertEntityToDtoStrategy convertEntityToDtoStrategy;
    GenreRepository genreRepository;
    TagRepository tagRepository;
    AuthorRepository authorRepository;
    ResourceStorage resourceStorage;

    @Override
    public Set<BookDto> getBooks() {
        return convertEntityToDtoStrategy.convert(bookRepository.findAll());
    }

    @Override
    public BooksPageDto getPaginatedBooks(Pageable pageable) {
        Page<Book> booksPage = bookRepository.findAll(pageable);

        List<Book> content = booksPage.getContent();

        Set<BookDto> books = convertEntityToDtoStrategy.convert(content);

        return BooksPageDto.builder()
                .count(books.size())
                .books(books)
                .build();
    }

    @Override
    public Set<BookDto> getBooksByAuthor(String authorName) {
        List<Book> books = bookRepository.findBooksByAuthorNameContaining(authorName);

        return convertEntityToDtoStrategy.convert(books);
    }

    @Override
    public Set<BookDto> getBooksByTitle(String title) {
        List<Book> books = bookRepository.findBooksByTitleContaining(title);

        return convertEntityToDtoStrategy.convert(books);
    }

    @Override
    public Set<BookDto> getBooksByPriceBetween(Double min, Double max) {
        List<Book> books = bookRepository.findByPriceBetween(min, max);

        return convertEntityToDtoStrategy.convert(books);
    }

    @Override
    public Set<BookDto> getBooksByPrice(Double price) {
        List<Book> books = bookRepository.findBooksByPrice(price);

        return convertEntityToDtoStrategy.convert(books);
    }

    @Override
    public Set<BookDto> getBestsellers() {
        List<Book> books = bookRepository.getBookBestsellers();

        return convertEntityToDtoStrategy.convert(books);
    }

    @Override
    public Set<BookDto> getBooksWithMaxDiscount() {
        List<Book> books = bookRepository.getBooksWithMaxDiscount();

        return convertEntityToDtoStrategy.convert(books);
    }

    @Override
    public BooksPageDto findBooksByFilter(String filter, Pageable pageable) {
        Set<BookDto> books = convertEntityToDtoStrategy.convert(bookRepository.findBooksByTitleContaining(filter, pageable));

        int size = bookRepository.findBooksByTitleContaining(filter).size();

        return BooksPageDto.builder()
                .count(size)
                .books(books)
                .build();
    }

    @Override
    public BooksPageDto getRecentPaginatedBooks(LocalDate localStartDate, LocalDate localEndDate, Pageable pageable) {

        Date endDate = Date.valueOf(localEndDate == null ? LocalDate.now() : localEndDate);
        Date startDate = Date.valueOf(localStartDate == null ? LocalDate.now().minusYears(1) : localStartDate);

        Set<BookDto> books = convertEntityToDtoStrategy.convert(bookRepository.findBooksByPublishDateBetween(startDate, endDate, pageable));

        int size = bookRepository.findBooksByPublishDateBetween(startDate, endDate, Pageable.unpaged()).size();

        return BooksPageDto.builder()
                .count(size)
                .books(books)
                .build();
    }

    @Override
    public BooksPageDto getPopularPaginatedBooks(Pageable pageable) {

        Set<BookDto> books = convertEntityToDtoStrategy.convert(bookRepository.findBooksByDownloadCountGreaterThan(6000, pageable));

        int size = bookRepository.findBooksByDownloadCountGreaterThan(6000).size();

        return BooksPageDto.builder()
                .count(size)
                .books(books)
                .build();
    }

    @Override
    public BooksPageDto getBooksByGenre(String slug, Pageable pageable) {

        Genre genre = genreRepository.getBySlugEquals(slug);

        Set<BookDto> books = convertEntityToDtoStrategy.convert(bookRepository.findBooksByBookToGenresIn(genre.getBookToGenres(), pageable));

        return BooksPageDto.builder()
                .count(genre.getBookToGenres().size())
                .books(books)
                .build();
    }

    @Override
    public BooksPageDto getBooksByTag(String slug, Pageable pageable) {

        Tag tag = tagRepository.findTagBySlugContaining(slug);

        Set<BookDto> books = convertEntityToDtoStrategy.convert(bookRepository.findBooksByBookToTagsIn(tag.getBookToTags(), pageable));

        return BooksPageDto.builder()
                .count(tag.getBookToTags().size())
                .books(books)
                .build();
    }

    @Override
    public BookDto getBookBySlug(String slug) {

        return convertEntityToDtoStrategy.convert(bookRepository.findBookBySlugContaining(slug));
    }

    @Override
    public BooksPageDto getAuthorBooksById(Long id, Pageable pageable) {

        Author author = authorRepository.getOne(id);

        Set<BookDto> books = convertEntityToDtoStrategy.convert(bookRepository.findBooksByAuthor(author, pageable));

        int size = bookRepository.findBooksByAuthor(author, Pageable.unpaged()).size();

        return BooksPageDto.builder()
                .count(size)
                .books(books)
                .build();
    }

    @Override
    public void saveNewBookImage(MultipartFile file, String slug) {

        String path = resourceStorage.saveNewBookImage(file, slug);

        Book book = bookRepository.findBookBySlugContaining(slug);

        book.setImage(path);

        bookRepository.save(book);
    }

    @Override
    public Set<BookDto> findBooksBySlugs(List<String> cookieSlugs) {

        return convertEntityToDtoStrategy.convert(bookRepository.findBooksBySlugIn(cookieSlugs));
    }
}
