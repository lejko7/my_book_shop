package com.example.MyBookShop.service;

import com.example.MyBookShop.data.entities.Comment;
import com.example.MyBookShop.data.entities.User;
import com.example.MyBookShop.data.repositories.BookRepository;
import com.example.MyBookShop.data.repositories.CommentRepository;
import com.example.MyBookShop.data.repositories.UserRepository;
import com.example.MyBookShop.security.BookstoreUserDetails;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class CommentServiceImpl implements CommentService {

    CommentRepository commentRepository;
    BookRepository bookRepository;
    UserRepository userRepository;

    @Override
    public void createNewComment(String slug, String text) {

        Comment comment = new Comment();

        comment.setComment(text);
        comment.setBook(bookRepository.findBookBySlugContaining(slug));
        comment.setDateTime(LocalDateTime.now());

        comment.setUser(getUser());

        commentRepository.saveAndFlush(comment);
    }

    private User getUser() {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal.getClass().equals(BookstoreUserDetails.class)) {
            return ((BookstoreUserDetails) principal).getUser();
        }

        long id = Long.parseLong(((DefaultOAuth2User) principal).getAttribute("id"));

        return userRepository.getOne(id);
    }
}
