package com.example.MyBookShop.service;

import com.example.MyBookShop.data.entities.Book;
import com.example.MyBookShop.data.entities.BookFile;
import com.example.MyBookShop.data.entities.Genre;
import com.example.MyBookShop.data.entities.Tag;
import com.example.MyBookShop.web.dto.BookDto;
import com.example.MyBookShop.web.dto.BookFileDto;
import com.example.MyBookShop.web.dto.GenreDto;
import com.example.MyBookShop.web.dto.TagDto;

import java.util.List;
import java.util.Set;

public interface ConvertEntityToDtoStrategy {

    BookDto convert(Book book);

    Set<BookDto> convert(List<Book> books);

    GenreDto convert(Genre genre);

    TagDto convert(Tag tag);

    BookFileDto convert(BookFile bookFile);
}
