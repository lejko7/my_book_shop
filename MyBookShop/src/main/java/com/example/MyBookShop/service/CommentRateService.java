package com.example.MyBookShop.service;

public interface CommentRateService {
    void saveComment(Long commentId, Long value);
}
