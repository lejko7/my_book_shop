package com.example.MyBookShop.service;

import com.example.MyBookShop.web.dto.AuthorDto;

import java.util.Map;
import java.util.Set;

public interface AuthorService {

    Set<AuthorDto> getAuthors();

    AuthorDto getAuthorInfoById(Long id);

    Map<String, Set<AuthorDto>> getAuthorsMap();
}
