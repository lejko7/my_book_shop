package com.example.MyBookShop.service;

public interface CommentService {
    void createNewComment(String slug, String text);
}
