package com.example.MyBookShop.service;

import com.example.MyBookShop.data.entities.Book;
import com.example.MyBookShop.data.entities.Cart;
import com.example.MyBookShop.data.repositories.CartRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class CartServiceImpl implements CartService {

    CartRepository cartRepository;

    @Override
    public List<Book> getUserBooksCart(Long id) {

        List<Cart> cart = cartRepository.findAllByUserId(id);

        return cart.stream().map(Cart::getBook).collect(Collectors.toList());
    }
}
