package com.example.MyBookShop.service;

import com.example.MyBookShop.data.entities.Genre;
import com.example.MyBookShop.data.repositories.BookRepository;
import com.example.MyBookShop.data.repositories.GenreRepository;
import com.example.MyBookShop.web.dto.BookDto;
import com.example.MyBookShop.web.dto.GenreDto;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class GenreServiceImpl implements GenreService {

    GenreRepository genreRepository;
    BookRepository bookRepository;
    ConvertEntityToDtoStrategy convertEntityToDtoStrategy;

    @Override
    public List<GenreDto> getAllGenres() {
        return genreRepository.findAllByParentGenreEqualsNull().stream().map(convertEntityToDtoStrategy::convert).collect(Collectors.toList());
    }

    @Override
    public GenreDto getPaginatedGenreBySlug(String slug) {

        Genre genre = genreRepository.getBySlugEquals(slug);

        GenreDto genreDto = convertEntityToDtoStrategy.convert(genre);

        Set<BookDto> bookDtos = convertEntityToDtoStrategy.convert(bookRepository.findBooksByBookToGenresIn(genre.getBookToGenres(), PageRequest.of(0, 5)));

        genreDto.setBooks(bookDtos);

        return genreDto;
    }
}
