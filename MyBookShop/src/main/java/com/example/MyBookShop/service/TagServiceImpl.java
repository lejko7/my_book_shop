package com.example.MyBookShop.service;

import com.example.MyBookShop.data.entities.Tag;
import com.example.MyBookShop.data.repositories.BookRepository;
import com.example.MyBookShop.data.repositories.TagRepository;
import com.example.MyBookShop.web.dto.BookDto;
import com.example.MyBookShop.web.dto.TagDto;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class TagServiceImpl implements TagService {

    TagRepository tagRepository;
    BookRepository bookRepository;
    ConvertEntityToDtoStrategy convertEntityToDtoStrategy;

    @Override
    public List<TagDto> getAllTags() {
        List<Tag> tags = tagRepository.findAll();

        return tags.stream().map(convertEntityToDtoStrategy::convert).collect(Collectors.toList());
    }

    @Override
    public TagDto getTagBySlug(String slug) {
        Tag tag = tagRepository.findTagBySlugContaining(slug);

        TagDto tagDto = convertEntityToDtoStrategy.convert(tag);

        Set<BookDto> bookDtos = convertEntityToDtoStrategy.convert(bookRepository.findBooksByBookToTagsIn(tag.getBookToTags(), PageRequest.of(0, 5)));

        tagDto.setBooks(bookDtos);

        return tagDto;
    }
}
