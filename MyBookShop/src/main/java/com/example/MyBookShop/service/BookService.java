package com.example.MyBookShop.service;

import com.example.MyBookShop.web.dto.BookDto;
import com.example.MyBookShop.web.dto.BooksPageDto;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;


public interface BookService {

    Set<BookDto> getBooks();

    BooksPageDto getPaginatedBooks(Pageable pageable);

    Set<BookDto> getBooksByAuthor(String authorName);

    Set<BookDto> getBooksByTitle(String title);

    Set<BookDto> getBooksByPriceBetween(Double min, Double max);

    Set<BookDto> getBooksByPrice(Double price);

    Set<BookDto> getBestsellers();

    Set<BookDto> getBooksWithMaxDiscount();

    BooksPageDto findBooksByFilter(String title, Pageable pageable);

    BooksPageDto getRecentPaginatedBooks(LocalDate localStartDate, LocalDate localEndDate, Pageable pageable);

    BooksPageDto getPopularPaginatedBooks(Pageable pageable);

    BooksPageDto getBooksByGenre(String slug, Pageable pageable);

    BooksPageDto getBooksByTag(String slug, Pageable pageable);

    BookDto getBookBySlug(String slug);

    BooksPageDto getAuthorBooksById(Long id, Pageable pageable);

    void saveNewBookImage(MultipartFile file, String slug);

    Set<BookDto> findBooksBySlugs(List<String> cookieSlugs);
}
