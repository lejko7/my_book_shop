package com.example.MyBookShop.service;

import com.example.MyBookShop.data.entities.Comment;
import com.example.MyBookShop.data.entities.CommentRate;
import com.example.MyBookShop.data.entities.User;
import com.example.MyBookShop.data.repositories.CommentRateRepository;
import com.example.MyBookShop.data.repositories.CommentRepository;
import com.example.MyBookShop.data.repositories.UserRepository;
import com.example.MyBookShop.security.BookstoreUserDetails;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class CommentRateServiceImpl implements CommentRateService {

    CommentRateRepository commentRateRepository;
    CommentRepository commentRepository;
    UserRepository userRepository;

    @Override
    public void saveComment(Long commentId, Long value) {
        User user = getUser();
        Comment comment = commentRepository.getOne(commentId);
        CommentRate commentRate = getCommentRate(user, comment);

        commentRate.setRate(value);
        commentRate.setComment(comment);
        commentRate.setUser(user);

        commentRateRepository.saveAndFlush(commentRate);
    }

    private CommentRate getCommentRate(User user, Comment comment) {
        CommentRate commentRate = commentRateRepository.findCommentRateByUserEqualsAndCommentEquals(user, comment);

        if (commentRate == null) {
            return new CommentRate();
        }

        return commentRate;
    }

    private User getUser() {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal.getClass().equals(BookstoreUserDetails.class)) {
            return ((BookstoreUserDetails) principal).getUser();
        }

        long id = Long.parseLong(((DefaultOAuth2User) principal).getAttribute("id"));

        return userRepository.getOne(id);
    }
}
