package com.example.MyBookShop.service;

import com.example.MyBookShop.data.entities.Book;
import com.example.MyBookShop.data.entities.Rate;
import com.example.MyBookShop.data.repositories.BookRepository;
import com.example.MyBookShop.data.repositories.RateRepository;
import com.example.MyBookShop.data.repositories.UserRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class RateServiceImpl implements RateService {

    BookRepository bookRepository;
    RateRepository rateRepository;
    UserRepository userRepository;

    @Override
    public void updateRate(Integer rate, String slug, Long userId, String cookie) {

        Book book = bookRepository.findBookBySlugContaining(slug);

        Rate rateEntity;
        if (userId == null) {

            rateEntity = rateRepository.findRateByCookieContainsAndBookEquals(cookie, book);

            if (rateEntity == null) {
                rateEntity = new Rate();

                rateEntity.setBook(book);
                rateEntity.setCookie(cookie);
            }

        } else {

            rateEntity = rateRepository.findRateByUser_IdAndBookEquals(userId, book);

            if (rateEntity == null) {
                rateEntity = new Rate();

                rateEntity.setBook(book);
                rateEntity.setUser(userRepository.getOne(userId));
                rateEntity.setCookie(cookie);
            }

            if (rateEntity.getUser() == null) {
                rateEntity.setUser(userRepository.getOne(userId));
            }

        }
        rateEntity.setRate(rate);
        rateRepository.saveAndFlush(rateEntity);
    }
}
