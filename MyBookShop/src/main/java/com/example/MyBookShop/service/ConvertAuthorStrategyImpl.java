package com.example.MyBookShop.service;

import com.example.MyBookShop.data.entities.Author;
import com.example.MyBookShop.web.dto.AuthorDto;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class ConvertAuthorStrategyImpl implements ConvertAuthorStrategy {

    ConvertEntityToDtoStrategy convertEntityToDtoStrategy;

    @Override
    public AuthorDto convert(Author author) {
        return AuthorDto.builder()
                .id(author.getId())
                .fullName(author.getSurname() + " " + author.getName())
                .biography(author.getBiography())
                .books(convertEntityToDtoStrategy.convert(author.getBooks()))
                .build();
    }

    @Override
    public Set<AuthorDto> convert(Collection<Author> authors) {

        return authors.stream().map(this::convert).collect(Collectors.toSet());
    }

}
