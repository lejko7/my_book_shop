package com.example.MyBookShop.service;

import com.example.MyBookShop.data.entities.User;
import com.example.MyBookShop.security.RegistrationForm;
import com.example.MyBookShop.web.dto.ContactConfirmationPayload;
import com.example.MyBookShop.web.dto.ContactConfirmationResponse;

public interface UserService {
    User getUser(String userName);

    User registration(RegistrationForm registrationForm);

    ContactConfirmationResponse login(ContactConfirmationPayload payload);

    ContactConfirmationResponse jwtLogin(ContactConfirmationPayload payload);

    User getCurrentUser();
}
