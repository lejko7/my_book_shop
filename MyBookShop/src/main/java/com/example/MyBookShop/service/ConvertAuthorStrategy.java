package com.example.MyBookShop.service;

import com.example.MyBookShop.data.entities.Author;
import com.example.MyBookShop.web.dto.AuthorDto;

import java.util.Collection;
import java.util.Set;

public interface ConvertAuthorStrategy {
    AuthorDto convert(Author author);

    Set<AuthorDto> convert(Collection<Author> authors);
}
