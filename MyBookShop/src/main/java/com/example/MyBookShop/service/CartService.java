package com.example.MyBookShop.service;

import com.example.MyBookShop.data.entities.Book;

import java.util.List;

public interface CartService {

    List<Book> getUserBooksCart(Long id);
}
