package com.example.MyBookShop.service;

import com.example.MyBookShop.data.entities.User;
import com.example.MyBookShop.data.repositories.UserRepository;
import com.example.MyBookShop.security.BookstoreUserDetails;
import com.example.MyBookShop.security.BookstoreUserDetailsService;
import com.example.MyBookShop.security.RegistrationForm;
import com.example.MyBookShop.security.jwt.JWTUtil;
import com.example.MyBookShop.web.dto.ContactConfirmationPayload;
import com.example.MyBookShop.web.dto.ContactConfirmationResponse;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Service
public class UserServiceImpl implements UserService {

    UserRepository userRepository;
    PasswordEncoder passwordEncoder;
    AuthenticationManager authenticationManager;
    BookstoreUserDetailsService bookstoreUserDetailsService;
    JWTUtil jwtUtil;

    @Override
    public User getUser(String userName) {

        return userRepository.findUserByLoginEquals(userName);
    }

    @Override
    public User registration(RegistrationForm registrationForm) {
        User user = new User();

        if (userRepository.findUserByEmailEqualsOrLoginEqualsOrPhoneEquals(
                registrationForm.getEmail(), registrationForm.getLogin(), registrationForm.getPhone()) == null) {

            user.setLogin(registrationForm.getLogin());
            user.setEmail(registrationForm.getEmail());
            user.setPassword(passwordEncoder.encode(registrationForm.getPassword()));
            user.setPhone(registrationForm.getPhone());

            userRepository.saveAndFlush(user);
        }

        return user;
    }

    @Override
    public ContactConfirmationResponse login(ContactConfirmationPayload payload) {

        Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(payload.getContact(),
                payload.getCode()));

        SecurityContextHolder.getContext().setAuthentication(authenticate);

        ContactConfirmationResponse response = new ContactConfirmationResponse();

        response.setResult("true");

        return response;
    }

    @Override
    public ContactConfirmationResponse jwtLogin(ContactConfirmationPayload payload) {

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(payload.getContact(),
                payload.getCode()));

        BookstoreUserDetails bookstoreUserDetails = (BookstoreUserDetails) bookstoreUserDetailsService.loadUserByUsername(payload.getContact());

        String jwtToken = jwtUtil.generateToken(bookstoreUserDetails);

        ContactConfirmationResponse response = new ContactConfirmationResponse();

        response.setResult(jwtToken);

        return response;
    }

    @Override
    public User getCurrentUser() {

        BookstoreUserDetails bookstoreUserDetails = (BookstoreUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        return bookstoreUserDetails.getUser();
    }

}
