package com.example.MyBookShop.service;

import com.example.MyBookShop.web.dto.TagDto;

import java.util.List;

public interface TagService {
    List<TagDto> getAllTags();

    TagDto getTagBySlug(String slug);
}
