package com.example.MyBookShop.web.controllers;

import com.example.MyBookShop.service.GenreService;
import com.example.MyBookShop.web.dto.GenreDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Controller
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class GenreController {

    GenreService genreService;

    @ModelAttribute("genresList")
    public List<GenreDto> genresBooks() {
        return genreService.getAllGenres();
    }

    @GetMapping("/genres")
    public String genresPage() {
        return "/genres/index";
    }

    @Operation(description = "Получение страницы с информацией и книгами конкретного жанра")
    @GetMapping("/genres/{slug}")
    public String currentGenrePage(@PathVariable @Parameter(description = "Мнемонический идентификатор жанра") String slug,
                                   Model model) {

        model.addAttribute("genreInfo", genreService.getPaginatedGenreBySlug(slug));

        return "/genres/slug";
    }
}
