package com.example.MyBookShop.web.controllers;

import com.example.MyBookShop.service.BookService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class UploadBooksController {

    BookService bookService;

    @PostMapping("book/{slug}/img/save")
    public String saveNewBookImage(@PathVariable String slug,
                                   @RequestParam MultipartFile file) {

        bookService.saveNewBookImage(file, slug);

        return "redirect:/book/" + slug;
    }
}
