package com.example.MyBookShop.web.controllers;

import com.example.MyBookShop.web.dto.ErrorResponse;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RestControllerAdvice
@AllArgsConstructor
public class ErrorController {

    @ExceptionHandler(Exception.class)
    public ErrorResponse errorResponse(Exception e, RedirectAttributes redirectAttributes) {

        System.out.println(e.getLocalizedMessage() + " in " + e.getClass().getName());

        redirectAttributes.addFlashAttribute("error", e);
        return ErrorResponse.builder()
                .message("Unhandled internal server error")
                .build();
    }
}
