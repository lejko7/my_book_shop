package com.example.MyBookShop.web.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RateCommentRequest {

    Long reviewid;
    Long value;
}
