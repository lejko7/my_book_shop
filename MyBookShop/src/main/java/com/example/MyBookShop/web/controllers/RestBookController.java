package com.example.MyBookShop.web.controllers;

import com.example.MyBookShop.service.BookService;
import com.example.MyBookShop.web.dto.BookDto;
import com.example.MyBookShop.web.dto.BooksPageDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Set;

@RestController
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RequestMapping("/api/books")
@Tag(name = "Book rest controller", description = "Получение книг в body")
public class RestBookController {

    BookService bookService;

    @GetMapping("recommendedBooks")
    @Operation(summary = "Получение списка рекомендованных книг")
    public ResponseEntity<BooksPageDto> recommendedBooks(@ParameterObject @Parameter(description = "Объект пагинации") Pageable pageable) {
        return new ResponseEntity<>(bookService.getPaginatedBooks(pageable), HttpStatus.OK);
    }

    @GetMapping("recentBooks")
    @Operation(summary = "Получение списка новинок")
    public ResponseEntity<BooksPageDto> recentBooks(@RequestParam(required = false) @Parameter(description = "Дата для взятия книг за указанный период, начало") LocalDate from,
                                                    @RequestParam(required = false) @Parameter(description = "Дата для взятия книг за указанный период, конец") LocalDate to,
                                                    @ParameterObject @Parameter(description = "Объект пагинации") Pageable pageable) {
        return new ResponseEntity<>(bookService.getRecentPaginatedBooks(from, to, pageable), HttpStatus.OK);
    }

    @GetMapping("popularBooks")
    public ResponseEntity<BooksPageDto> popularBooks(@ParameterObject @Parameter(description = "Объект для пагинации") Pageable pageable) {
        return new ResponseEntity<>(bookService.getPopularPaginatedBooks(pageable), HttpStatus.OK);
    }

    @GetMapping("author/{authorName}")
    @Operation(summary = "Список книг по части имени автора")
    public ResponseEntity<Set<BookDto>> booksByAuthorName(@PathVariable @Parameter(description = "Часть/полное имя(не фамилия!) автора") String authorName) {
        return new ResponseEntity<>(bookService.getBooksByAuthor(authorName), HttpStatus.OK);
    }

    @GetMapping("/authors/{id}")
    @Operation(summary = "Список книг по id автора")
    public ResponseEntity<BooksPageDto> authorBooksPage(@PathVariable @Parameter(description = "id автора") Long id,
                                                        @ParameterObject @Parameter(description = "Объект пагинации") Pageable pageable) {

        return new ResponseEntity<>(bookService.getAuthorBooksById(id, pageable), HttpStatus.OK);
    }

    @GetMapping("title/{title}")
    @Operation(summary = "Получение списка книг по части названия")
    public ResponseEntity<Set<BookDto>> booksByTitle(@PathVariable @Parameter(description = "Часть/полное наименование книги") String title) {
        return new ResponseEntity<>(bookService.getBooksByTitle(title), HttpStatus.OK);
    }

    @GetMapping("priceBetween")
    @Operation(summary = "Получение списка книг в ценовом диапазоне")
    public ResponseEntity<Set<BookDto>> booksByPriceBetween(@RequestParam @Parameter(description = "Цена минимальная") Double minPrice,
                                                            @RequestParam @Parameter(description = "Цена максимальная") Double maxPrice) {
        return new ResponseEntity<>(bookService.getBooksByPriceBetween(minPrice, maxPrice), HttpStatus.OK);
    }

    @GetMapping("price/{price}")
    @Operation(summary = "Получение списка книг по точной цене")
    public ResponseEntity<Set<BookDto>> booksByPrice(@PathVariable @Parameter(description = "Цена") Double price) {
        return new ResponseEntity<>(bookService.getBooksByPrice(price), HttpStatus.OK);
    }

    @GetMapping("maxDiscount")
    @Operation(summary = "Получение списка книг с максимальной скидкой")
    public ResponseEntity<Set<BookDto>> booksWithMaxDiscount() {
        return new ResponseEntity<>(bookService.getBooksWithMaxDiscount(), HttpStatus.OK);
    }

    @GetMapping("bestsellers")
    @Operation(summary = "Получение списка книг-бестселлеров")
    public ResponseEntity<Set<BookDto>> booksByPrice() {
        return new ResponseEntity<>(bookService.getBestsellers(), HttpStatus.OK);
    }

    @GetMapping("/search/page/{word}")
    public ResponseEntity<BooksPageDto> searchPage(@PathVariable(required = false) @Parameter(description = "Слово для поиска") String word,
                                                   @ParameterObject @Parameter(description = "Объект пагинации") Pageable pageable) {

        return new ResponseEntity<>(bookService.findBooksByFilter(word, pageable), HttpStatus.OK);
    }

    @GetMapping("/genre/{slug}")
    @Operation(description = "Метод для получения книг жанра")
    public ResponseEntity<BooksPageDto> getGenres(@PathVariable @Parameter(description = "Жанр книги") String slug,
                                                  @ParameterObject @Parameter(description = "Объект пагинации") Pageable pageable) {

        return new ResponseEntity<>(bookService.getBooksByGenre(slug, pageable), HttpStatus.OK);
    }

    @GetMapping("/tag/{slug}")
    @Operation(description = "Метод для получения книг по тегу")
    public ResponseEntity<BooksPageDto> getTagsBook(@PathVariable @Parameter(description = "Тег книги") String slug,
                                                    @ParameterObject @Parameter(description = "Объект пагинации") Pageable pageable) {

        return new ResponseEntity<>(bookService.getBooksByTag(slug, pageable), HttpStatus.OK);
    }
}
