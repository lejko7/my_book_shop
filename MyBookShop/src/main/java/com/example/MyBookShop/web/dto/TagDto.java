package com.example.MyBookShop.web.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TagDto {

    @Schema(description = "id тега")
    Long id;

    @Schema(description = "Название тега")
    String tag;

    @Schema(description = "Мнемоническая ссылка на тег")
    String slug;

    @Schema(description = "Список книг")
    Set<BookDto> books;
}
