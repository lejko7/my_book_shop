package com.example.MyBookShop.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

/**
 * Объект с данными об ошибке для передачи на клиент
 */
@Getter
@AllArgsConstructor
@Schema(title = "ErrorResponse", description = "Ошибка")
@Builder
public class ErrorResponse {

    @Schema(title = "Текст ошибки")
    @JsonProperty
    private final String message;
}
