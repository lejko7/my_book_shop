package com.example.MyBookShop.web.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.sql.Date;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BookDto {

    @Schema(description = "id книги")
    Long id;

    @Schema(description = "Название книги")
    String title;

    @Schema(description = "Мнемонический идентификатор книги")
    String slug;

    @Schema(description = "ссылка на изображение книги")
    String image;

    @Schema(description = "Описание книги")
    String description;

    @Schema(description = "Является ли книга бестселлером")
    boolean isBestseller;

    @Schema(description = "Дата пубилкации книги")
    Date publishDate;

    @Schema(description = "Размер скидки")
    Integer discount;

    @Schema(description = "Новая цена исходя из скидок")
    String discountPrice;

    @Schema(description = "Цена без скидок")
    String price;

    @Schema(description = "Количество скачиваний книги")
    Integer downloadCount;

    @Schema(description = "Кроткое об авторе")
    AuthorShortDto author;

    @Schema(description = "Жанры книги")
    List<String> genres;

    @Schema(description = "Теги книги")
    List<TagShortDto> tags;

    @Schema(description = "Файлы книги")
    List<BookFileDto> bookFiles;

    @Schema(description = "Оценки книги")
    Map<Integer, Long> rates;

    @Schema(description = "Комментарии книги")
    List<CommentDto> comments;

    @Schema(description = "Количество оценок книги")
    Long rateCount;

    @Schema(description = "Общая оценка книги")
    Long rate;
}
