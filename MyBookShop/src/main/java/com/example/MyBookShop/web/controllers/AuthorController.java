package com.example.MyBookShop.web.controllers;

import com.example.MyBookShop.service.AuthorService;
import com.example.MyBookShop.web.dto.AuthorDto;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Map;
import java.util.Set;

@Controller
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class AuthorController {

    AuthorService authorService;

    @ModelAttribute("authorsMap")
    public Map<String, Set<AuthorDto>> authorsMap() {
        return authorService.getAuthorsMap();
    }

    @GetMapping("/authors")
    public String authorsPage(Model model) {
        model.addAttribute("authors", authorService.getAuthors());
        return "/authors/index";
    }

    @GetMapping("/authors/{id}")
    public String getSlugAuthors(Model model, @PathVariable Long id) {
        model.addAttribute("authorData", authorService.getAuthorInfoById(id));
        return "/authors/slug";
    }
}
