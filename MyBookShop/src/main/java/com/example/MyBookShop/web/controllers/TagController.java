package com.example.MyBookShop.web.controllers;

import com.example.MyBookShop.service.TagService;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class TagController {

    TagService tagService;

    @GetMapping("/tags/{slug}")
    public String bookPage(@PathVariable @Parameter(description = "Мнемоническая ссылка на тег") String slug,
                           Model model) {

        model.addAttribute("tag", tagService.getTagBySlug(slug));

        return "/tags/index";
    }
}
