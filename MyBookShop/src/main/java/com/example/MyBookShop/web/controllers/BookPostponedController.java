package com.example.MyBookShop.web.controllers;


import com.example.MyBookShop.service.BookService;
import com.example.MyBookShop.web.dto.BookDto;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@Controller
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Tag(name = "Book postponed controller", description = "Работа с отложенными книгами пользователя")
@RequestMapping("books")
public class BookPostponedController {

    BookService bookService;

    @ModelAttribute(name = "bookPostponed")
    public Set<BookDto> bookCart() {
        return new HashSet<>();
    }

    @GetMapping("/postponed")
    public String cartPage(@CookieValue(required = false) String postponedContent, Model model) {

        if (postponedContent == null || postponedContent.equals("")) {
            model.addAttribute("isPostponedEmpty", true);
        } else {
            model.addAttribute("isPostponedEmpty", false);

            postponedContent = postponedContent.startsWith("/") ? postponedContent.substring(1) : postponedContent;
            postponedContent = postponedContent.endsWith("/") ? postponedContent.substring(0, postponedContent.length() - 1) : postponedContent;

            List<String> cookieSlugs = Arrays.asList(postponedContent.split("/"));
            Set<BookDto> bookDtos = bookService.findBooksBySlugs(cookieSlugs);

            model.addAttribute("bookPostponed", bookDtos);
        }

        return "/postponed";
    }

    @PostMapping("/changeBookStatus/postponed/remove/{slug}")
    public String removeFromCart(@PathVariable String slug,
                                 HttpServletResponse httpServletResponse,
                                 Model model,
                                 @CookieValue(required = false) String postponedContent) {

        if (postponedContent != null && !postponedContent.equals("")) {
            List<String> cookieBooks = new ArrayList<>(Arrays.asList(postponedContent.split("/")));
            cookieBooks.remove(slug);
            Cookie cookie = new Cookie("postponedContent", String.join("/", cookieBooks));
            cookie.setPath("/books");
            httpServletResponse.addCookie(cookie);
            model.addAttribute("isPostponedEmpty", false);
        } else {
            model.addAttribute("isPostponedEmpty", true);
        }

        return "redirect:/books/postponed";
    }
}
