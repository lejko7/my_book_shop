package com.example.MyBookShop.web.controllers;

import com.example.MyBookShop.security.BookstoreUserDetails;
import com.example.MyBookShop.service.CommentRateService;
import com.example.MyBookShop.service.CommentService;
import com.example.MyBookShop.service.RateService;
import com.example.MyBookShop.web.dto.BookCommentRequest;
import com.example.MyBookShop.web.dto.RateBookRequest;
import com.example.MyBookShop.web.dto.RateCommentRequest;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.Cookie;

@Controller
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Tag(name = "Book rate controller", description = "Работа с оценками книг пользователя")
@RequestMapping("book")
public class BookRateController {

    RateService rateService;
    CommentService commentService;
    CommentRateService commentRateService;

    @PostMapping("changeBookStatus")
    public String changeBookStatus(@RequestBody RateBookRequest request,
                                   @CookieValue(required = false) Cookie mainCookie) {

        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal().equals("anonymousUser")) {
            rateService.updateRate(request.getValue(), request.getSlug(), null, mainCookie.getValue());
        } else {

            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

            rateService.updateRate(request.getValue(), request.getSlug(), principal.getClass().equals(BookstoreUserDetails.class) ?
                    ((BookstoreUserDetails) principal).getUser().getId() : Long.parseLong(((DefaultOAuth2User) principal).getAttribute("id")), mainCookie.getValue());
        }

        return "redirect:/book/" + request.getSlug();
    }

    @PostMapping("bookReview")
    public String bookReview(@RequestBody BookCommentRequest bookCommentRequest) {

        commentService.createNewComment(bookCommentRequest.getSlug(), bookCommentRequest.getText());

        return "redirect:/book/" + bookCommentRequest.getSlug();
    }

    @PostMapping("rateBookReview")
    public String bookRateReview(@RequestBody RateCommentRequest request) {

        commentRateService.saveComment(request.getReviewid(), request.getValue());

        return "redirect:/";
    }
}
