package com.example.MyBookShop.web.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AuthorShortDto {

    @Schema(description = "id автора")
    Long id;
    @Schema(description = "Фамилия + имя автора")
    String fullName;

    @Override
    public String toString() {
        return fullName;
    }
}
