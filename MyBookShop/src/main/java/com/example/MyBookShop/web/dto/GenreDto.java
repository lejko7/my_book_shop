package com.example.MyBookShop.web.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class GenreDto {

    @Schema(description = "id жанра")
    Long id;

    @Schema(description = "Имя жанра")
    String genre;

    @Schema(description = "Ссылка на жанр")
    String slug;

    @Schema(description = "Список дочерних жанров")
    List<GenreDto> subGenres;

    @Schema(description = "Список книг данного жанра")
    Set<BookDto> books;
}
