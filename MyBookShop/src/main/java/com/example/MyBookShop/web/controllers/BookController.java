package com.example.MyBookShop.web.controllers;

import com.example.MyBookShop.service.AuthorService;
import com.example.MyBookShop.service.BookService;
import com.example.MyBookShop.service.TagService;
import com.example.MyBookShop.web.dto.BookDto;
import com.example.MyBookShop.web.dto.BooksPageDto;
import com.example.MyBookShop.web.dto.TagDto;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.apache.commons.codec.digest.DigestUtils;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Set;

@Controller
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class BookController {

    BookService bookService;
    AuthorService authorService;
    TagService tagService;

    @ModelAttribute("allBooks")
    public Set<BookDto> getAllBooks() {
        return bookService.getBooks();
    }

    @ModelAttribute("recommendedBooks")
    public BooksPageDto recommendedBooks(@ParameterObject @Parameter(description = "Объект пагинации") Pageable pageable) {
        return bookService.getPaginatedBooks(pageable);
    }

    @ModelAttribute("recentBooks")
    public BooksPageDto recentBooks(@ParameterObject @Parameter(description = "Объект пагинации") Pageable pageable) {
        return bookService.getRecentPaginatedBooks(null, null, pageable);
    }

    @ModelAttribute("popularBooks")
    public BooksPageDto popularBooks(@ParameterObject @Parameter(description = "Объект пагинации") Pageable pageable) {
        return bookService.getPopularPaginatedBooks(pageable);
    }

    @ModelAttribute("tagsBooks")
    public BooksPageDto tagsBooks(@ParameterObject @Parameter(description = "Объект пагинации") Pageable pageable) {
        return bookService.getPopularPaginatedBooks(pageable);
    }

    @ModelAttribute("tags")
    public List<TagDto> popularBooks() {
        return tagService.getAllTags();
    }

    @ModelAttribute("word")
    public String getWord() {
        return "";
    }

    @ModelAttribute("searchResults")
    public BooksPageDto getSearchBooks() {
        return BooksPageDto.builder()
                .books(Collections.emptySet())
                .count(0)
                .build();
    }

    @GetMapping("/")
    public String mainPage(
            @CookieValue(required = false) Cookie mainCookie,
            HttpServletResponse httpServletResponse) {

        if (mainCookie == null) {
            Cookie cookie = new Cookie("mainCookie",
                    "unauthorizedUser" +
                            new Random().nextLong() +
                            DigestUtils.sha256Hex("unauthorizedUser" +
                                    new Random().nextLong() +
                                    LocalDate.now()));
            cookie.setPath("/");

            httpServletResponse.addCookie(cookie);
        }

        return "index";
    }

    @GetMapping("/book/{slug}")
    public String bookPage(@PathVariable @Parameter(description = "Мнемоническая ссылка на книгу") String slug,
                           Model model) {

        model.addAttribute("book", bookService.getBookBySlug(slug));

        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal().equals("anonymousUser")) {

            return "/books/slug";
        } else {
            return "/books/slugmy";
        }
    }

    @GetMapping("/popular")
    public String popularPage(Model model) {

        model.addAttribute("books", bookService.getPopularPaginatedBooks(PageRequest.of(0, 5)));

        return "/books/popular";
    }

    @GetMapping("/recent")
    public String recentPage(Model model) {

        model.addAttribute("books", bookService.getRecentPaginatedBooks(null, null, PageRequest.of(0, 5)));

        return "/books/recent";
    }

    @GetMapping("/books/author/{id}")
    public String authorBooksPage(@PathVariable @Parameter(description = "id автора") Long id,
                                  Model model) {

        model.addAttribute("books", bookService.getAuthorBooksById(id, PageRequest.of(0, 5)));
        model.addAttribute("author", authorService.getAuthorInfoById(id));

        return "/books/author";
    }

    @GetMapping(value = {"/search", "/search/{word}"})
    public String searchPage(@PathVariable(required = false) @Parameter(description = "Слово для поиска") String word,
                             @ParameterObject @Parameter(description = "Объект пагинации") Pageable pageable,
                             Model model) {

        model.addAttribute("word", word);
        model.addAttribute("searchResults", bookService.findBooksByFilter(word, pageable));

        return "/search/index";
    }
}
