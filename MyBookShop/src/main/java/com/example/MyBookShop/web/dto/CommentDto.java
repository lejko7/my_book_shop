package com.example.MyBookShop.web.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CommentDto {

    @Schema(description = "id комментария")
    Long id;

    @Schema(description = "Имя пользователя")
    String userName;

    @Schema(description = "Выставленный рейтинг")
    Long rate;

    @Schema(description = "Комментарий")
    String comment;

    @Schema(description = "Время комментария")
    LocalDateTime dateTime;

    @Schema(description = "Количество позитивных лайков")
    Long positiveRates;

    @Schema(description = "Количество негативных лайков")
    Long negativeRates;
}
