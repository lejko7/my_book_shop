package com.example.MyBookShop.web.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BookFileDto {

    @Schema(description = "id файла книги")
    Long id;

    @Schema(description = "Хеш")
    String hash;

    @Schema(description = "Тип книги")
    String bookType;

    @Schema(description = "ссылка на файл книги")
    String path;
}
