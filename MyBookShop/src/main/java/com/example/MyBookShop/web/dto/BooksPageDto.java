package com.example.MyBookShop.web.dto;


import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class BooksPageDto {

    Integer count;

    Set<BookDto> books;
}
