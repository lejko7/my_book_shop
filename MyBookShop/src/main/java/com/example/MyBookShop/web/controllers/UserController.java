package com.example.MyBookShop.web.controllers;

import com.example.MyBookShop.security.RegistrationForm;
import com.example.MyBookShop.service.UserService;
import com.example.MyBookShop.web.dto.ContactConfirmationPayload;
import com.example.MyBookShop.web.dto.ContactConfirmationResponse;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@Controller
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class UserController {

    UserService userService;

    @GetMapping("/signin")
    public String signinPage() {
        return "/signin";
    }

    @GetMapping("/signup")
    public String signupPage(Model model) {

        model.addAttribute("regForm", new RegistrationForm());

        return "/signup";
    }

    @PostMapping("/signin")
    public String signinAction() {

        return "/signin";
    }

    @PostMapping("/signup")
    public String signupAction(RegistrationForm registrationForm, Model model) {

        userService.registration(registrationForm);
        model.addAttribute("regOk", true);

        return "/signin";
    }

    @PostMapping("/approveContact")
    @ResponseBody
    public ContactConfirmationResponse handleApproveContact(@RequestBody ContactConfirmationPayload payload) {

        ContactConfirmationResponse response = new ContactConfirmationResponse();

        response.setResult("true");

        return response;
    }

    @PostMapping("requestContactConfirmation")
    @ResponseBody
    public ContactConfirmationResponse handleRequestContactConfirmation(@RequestBody ContactConfirmationPayload payload) {

        ContactConfirmationResponse response = new ContactConfirmationResponse();

        response.setResult("true");

        return response;
    }

    @PostMapping("login")
    @ResponseBody
    public ContactConfirmationResponse handleLogin(@RequestBody ContactConfirmationPayload payload, HttpServletResponse httpServletResponse) {

        ContactConfirmationResponse response = userService.jwtLogin(payload);
        Cookie cookie = new Cookie("token", response.getResult());

        httpServletResponse.addCookie(cookie);

        return response;
    }

    @GetMapping("/my")
    public String myPage() {

        return "my";
    }

    @GetMapping("/profile")
    public String profilePage() {

        return "profile";
    }
}
