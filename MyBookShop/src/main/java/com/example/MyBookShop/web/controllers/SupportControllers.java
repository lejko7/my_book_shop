package com.example.MyBookShop.web.controllers;

import com.example.MyBookShop.data.entities.Book;
import com.example.MyBookShop.service.CartService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Controller
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class SupportControllers {

    CartService cartService;

    @GetMapping("/cart/{id}")
    public String popularPage(@PathVariable Long id) {
        List<Book> userBooksCart = cartService.getUserBooksCart(id);
        return "/cart";
    }

    @GetMapping("/about")
    public String recentPage() {
        return "/about";
    }

    @GetMapping("/faq")
    public String searchPage() {
        return "/faq";
    }

    @GetMapping("/contacts")
    public String contactsPage() {
        return "/contacts";
    }
}
