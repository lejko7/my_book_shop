package com.example.MyBookShop.web.controllers;

import com.example.MyBookShop.enums.EBookStatus;
import com.example.MyBookShop.service.BookService;
import com.example.MyBookShop.web.dto.BookDto;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@Controller
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Tag(name = "Book cart controller", description = "Работа с корзиной покупок пользователя")
@RequestMapping("books")
public class BookCartController {

    BookService bookService;

    @ModelAttribute(name = "bookCart")
    public Set<BookDto> bookCart() {
        return new HashSet<>();
    }

    @GetMapping("/cart")
    public String cartPage(@CookieValue(required = false) String cartContent, Model model) {

        if (cartContent == null || cartContent.equals("")) {
            model.addAttribute("isCartEmpty", true);
        } else {
            model.addAttribute("isCartEmpty", false);

            cartContent = cartContent.startsWith("/") ? cartContent.substring(1) : cartContent;
            cartContent = cartContent.endsWith("/") ? cartContent.substring(0, cartContent.length() - 1) : cartContent;

            List<String> cookieSlugs = Arrays.asList(cartContent.split("/"));
            Set<BookDto> bookDtos = bookService.findBooksBySlugs(cookieSlugs);

            model.addAttribute("bookCart", bookDtos);


        }

        return "/cart";
    }

    @PostMapping("/changeBookStatus/cart/remove/{slug}")
    public String removeFromCart(@PathVariable String slug,
                                 HttpServletResponse httpServletResponse,
                                 Model model,
                                 @CookieValue(required = false) String cartContent) {

        if (cartContent != null && !cartContent.equals("")) {
            List<String> cookieBooks = new ArrayList<>(Arrays.asList(cartContent.split("/")));
            cookieBooks.remove(slug);
            Cookie cookie = new Cookie("cartContent", String.join("/", cookieBooks));
            cookie.setPath("/books");
            httpServletResponse.addCookie(cookie);
            model.addAttribute("isCartEmpty", false);
        } else {
            model.addAttribute("isCartEmpty", true);
        }

        return "redirect:/books/cart";
    }


    @PostMapping("changeBookStatus/{slug}")
    public String changeBookStatus(@PathVariable String slug,
                                   @CookieValue(required = false) String postponedContent,
                                   @CookieValue(required = false) String cartContent,
                                   HttpServletResponse httpServletResponse,
                                   Model model,
                                   @RequestParam(required = false) EBookStatus status) {

        switch (status) {

            case KEPT:

                if (postponedContent == null || postponedContent.equals("")) {
                    Cookie cookie = new Cookie("postponedContent", slug);
                    cookie.setPath("/books");
                    httpServletResponse.addCookie(cookie);

                    model.addAttribute("isPostponedEmpty", false);
                } else if (!postponedContent.contains(slug)) {
                    StringJoiner stringJoiner = new StringJoiner("/");
                    stringJoiner.add(postponedContent).add(slug);
                    Cookie cookie = new Cookie("postponedContent", stringJoiner.toString());
                    cookie.setPath("/books");
                    httpServletResponse.addCookie(cookie);

                    model.addAttribute("isPostponedEmpty", false);
                }

                break;
            case CART:
                if (cartContent == null || cartContent.equals("")) {
                    Cookie cookie = new Cookie("cartContent", slug);
                    cookie.setPath("/books");
                    httpServletResponse.addCookie(cookie);

                    model.addAttribute("isCartEmpty", false);
                } else if (!cartContent.contains(slug)) {
                    StringJoiner stringJoiner = new StringJoiner("/");
                    stringJoiner.add(cartContent).add(slug);
                    Cookie cookie = new Cookie("cartContent", stringJoiner.toString());
                    cookie.setPath("/books");
                    httpServletResponse.addCookie(cookie);

                    model.addAttribute("isCartEmpty", false);
                }
                break;
            case ARCHIVED:
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + status);
        }

        return "redirect:/book/" + slug;
    }
}
