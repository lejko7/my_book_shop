package com.example.MyBookShop.web.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AuthorDto {

    @Schema(description = "id автора")
    Long id;

    @Schema(description = "Фамилия + имя автора")
    String fullName;

    @Schema(description = "Биография автора")
    String biography;

    @Schema(description = "Список книг автора")
    Set<BookDto> books;

    @Override
    public String toString() {
        return fullName;
    }
}
