package com.example.MyBookShop.web.controllers;

import com.example.MyBookShop.service.GenreService;
import com.example.MyBookShop.web.dto.GenreDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@RequestMapping("/api/genres")
@Tag(name = "Контроллер для жанров", description = "Методы для взаимодействия с жанрами")
public class RestGenreController {

    final GenreService genreService;

    @GetMapping()
    @Operation(description = "Метод для получения всех жанров")
    public ResponseEntity<List<GenreDto>> getGenres() {

        return new ResponseEntity<>(genreService.getAllGenres(), HttpStatus.OK);
    }
}
