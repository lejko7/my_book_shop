package com.example.MyBookShop.enums;


import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
public enum EBookStatus {

    KEPT("Отложено"),
    CART("Корзина"),
    ARCHIVED("Архив");

    final String value;
}
