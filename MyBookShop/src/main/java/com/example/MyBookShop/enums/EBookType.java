package com.example.MyBookShop.enums;


import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
public enum EBookType {

    EPUB("epub"),
    FB2("fb2"),
    PDF("pdf");

    final String value;
}
