package com.example.MyBookShop.data.entities;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "genres")
public class Genre {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "genre")
    String genre;

    @Column
    String slug;

    @ManyToOne
    @JoinColumn(name = "genre_id")
    Genre parentGenre;

    @OneToMany(mappedBy = "parentGenre", fetch = FetchType.LAZY)
    List<Genre> subGenres = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "genre", fetch = FetchType.LAZY)
    List<BookToGenre> bookToGenres = new ArrayList<>();
}
