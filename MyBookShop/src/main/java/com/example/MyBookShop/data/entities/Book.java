package com.example.MyBookShop.data.entities;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "books")
public class Book {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(length = 500)
    String title;

    @Column(length = 500)
    String slug;

    @Column(length = 500)
    String image;

    @Column
    String description;

    @Column(name = "is_bestseller")
    boolean bestseller;

    @Column(name = "pub_date")
    Date publishDate;

    @Column(name = "discount")
    Integer discount;

    @Column
    Double price;

    @Column(name = "downloads_count")
    Integer downloadCount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "author_id", referencedColumnName = "id", nullable = false)
    Author author;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "book", fetch = FetchType.LAZY)
    List<BookToGenre> bookToGenres = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "book", fetch = FetchType.LAZY)
    List<BookToTag> bookToTags = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "book", fetch = FetchType.LAZY)
    List<BookFile> bookFiles = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "book", fetch = FetchType.LAZY)
    List<Rate> rates = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "book", fetch = FetchType.LAZY)
    List<Comment> comments = new ArrayList<>();
}
