package com.example.MyBookShop.data.repositories;

import com.example.MyBookShop.data.entities.BlackListJwt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface BlackListJwtRepository extends JpaRepository<BlackListJwt, Long> {

    BlackListJwt findBlackListJwtByTokenEquals(String token);

    void removeBlackListJwtByDateBefore(Date date);

    List<BlackListJwt> findBlackListJwtsByDateBefore(Date date);

}
