package com.example.MyBookShop.data.repositories;

import com.example.MyBookShop.data.entities.Author;
import com.example.MyBookShop.data.entities.Book;
import com.example.MyBookShop.data.entities.BookToGenre;
import com.example.MyBookShop.data.entities.BookToTag;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

    List<Book> findBooksByAuthorNameContaining(String name);

    List<Book> findBooksByTitleContaining(String title);

    List<Book> findByPriceBetween(Double min, Double max);

    List<Book> findBooksByPrice(Double price);

    @Query("from Book where bestseller = true")
    List<Book> getBookBestsellers();

    @Query(value = "select * from books where discount = (select max(discount) from books)", nativeQuery = true)
    List<Book> getBooksWithMaxDiscount();

    List<Book> findBooksByTitleContaining(String title, Pageable pageable);

    List<Book> findBooksByPublishDateAfter(Date date, Pageable pageable);

    List<Book> findBooksByPublishDateBetween(Date startDate, Date endDate, Pageable pageable);

    List<Book> findBooksByDownloadCountGreaterThan(Integer count, Pageable pageable);

    List<Book> findBooksByDownloadCountGreaterThan(Integer count);

    List<Book> findBooksByBookToGenresIn(List<BookToGenre> bookToGenres, Pageable pageable);

    List<Book> findBooksByBookToTagsIn(List<BookToTag> bookToTags, Pageable pageable);

    Book findBookBySlugContaining(String slug);

    List<Book> findBooksByAuthor(Author author, Pageable pageable);

    List<Book> findBooksBySlugIn(List<String> cookieSlugs);
}
