package com.example.MyBookShop.data.repositories;

import com.example.MyBookShop.data.entities.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GenreRepository extends JpaRepository<Genre, Long> {

    @Query(value = "select * from genres where genre_id is null", nativeQuery = true)
    List<Genre> findAllByParentGenreEqualsNull();

    Genre getBySlugEquals(String slug);
}
