package com.example.MyBookShop.data.repositories;

import com.example.MyBookShop.data.entities.Book;
import com.example.MyBookShop.data.entities.Rate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RateRepository extends JpaRepository<Rate, Long> {

    Rate findRateByCookieContainsAndBookEquals(String cookie, Book book);

    Rate findRateByUser_IdAndBookEquals(Long userId, Book book);
}
