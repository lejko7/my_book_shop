package com.example.MyBookShop.data.repositories;

import com.example.MyBookShop.data.entities.Comment;
import com.example.MyBookShop.data.entities.CommentRate;
import com.example.MyBookShop.data.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRateRepository extends JpaRepository<CommentRate, Long> {

    CommentRate findCommentRateByUserEqualsAndCommentEquals(User user, Comment comment);
}
