package com.example.MyBookShop.data.repositories;

import com.example.MyBookShop.data.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findUserByLoginEquals(String login);

    User findUserByEmailEquals(String email);

    User findUserByEmailEqualsOrLoginEqualsOrPhoneEquals(String email, String login, String phone);

    User findUserByPhoneEquals(String phone);
}
