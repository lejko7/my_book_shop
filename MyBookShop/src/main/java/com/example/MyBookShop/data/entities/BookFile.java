package com.example.MyBookShop.data.entities;

import com.example.MyBookShop.enums.EBookType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.ColumnTransformer;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "book_file")
public class BookFile {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(length = 255)
    String hash;

    @Column(name = "type_value", length = 500)
    @ColumnTransformer(read = "UPPER(type_value)")
    @Enumerated(EnumType.STRING)
    EBookType type;

    @Column(length = 500)
    String path;

    @ManyToOne
    @JoinColumn(name = "book_id", referencedColumnName = "id")
    Book book;
}
