package com.example.MyBookShop.data.repositories;

import com.example.MyBookShop.data.entities.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TagRepository extends JpaRepository<Tag, Long> {

    Tag findTagBySlugContaining(String tag);
}
