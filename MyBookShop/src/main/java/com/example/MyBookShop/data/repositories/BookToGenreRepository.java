package com.example.MyBookShop.data.repositories;

import com.example.MyBookShop.data.entities.BookToGenre;
import com.example.MyBookShop.data.entities.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookToGenreRepository extends JpaRepository<BookToGenre, Long> {

    List<BookToGenre> findAllByGenre(Genre genre);
}
