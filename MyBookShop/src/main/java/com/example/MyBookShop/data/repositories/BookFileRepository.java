package com.example.MyBookShop.data.repositories;

import com.example.MyBookShop.data.entities.BookFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookFileRepository extends JpaRepository<BookFile, Long> {
}
