package com.example.MyBookShop.config;

import com.example.MyBookShop.web.dto.ErrorResponse;
import io.swagger.v3.core.converter.ModelConverters;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.media.Content;
import io.swagger.v3.oas.models.media.MediaType;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.responses.ApiResponse;
import io.swagger.v3.oas.models.responses.ApiResponses;
import io.swagger.v3.oas.models.servers.Server;
import org.springdoc.core.customizers.OpenApiCustomiser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {

    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI().addServersItem(new Server().url("/")).info(new Info().title("BookShop API").version("V1"));
    }

    @Bean
    public OpenApiCustomiser customerGlobalHeaderOpenApiCustomiser() {
        return openApi -> {
            openApi.getComponents().getSchemas().putAll(ModelConverters.getInstance().read(ErrorResponse.class));
            Schema errorResponseSchema = new Schema();
            errorResponseSchema.setName("ErrorResponse");
            errorResponseSchema.set$ref("#/components/schemas/ErrorResponse");
            openApi.getPaths().values().forEach(pathItem -> pathItem.readOperations().forEach(operation -> {
                ApiResponses apiResponses = operation.getResponses();
                apiResponses.addApiResponse("400", createApiResponse("BadRequest", errorResponseSchema));
                apiResponses.addApiResponse("403", createApiResponse("Forbidden", errorResponseSchema));
                apiResponses.addApiResponse("500", createApiResponse("Server Error", errorResponseSchema));
            }));
        };
    }

    private ApiResponse createApiResponse(String message, Schema schema) {
        MediaType mediaType = new MediaType();
        mediaType.schema(schema);
        return new ApiResponse().description(message)
                .content(new Content().addMediaType(org.springframework.http.MediaType.APPLICATION_JSON_VALUE, mediaType));
    }
}
