package com.example.MyBookShop.config;

import com.example.MyBookShop.data.entities.BlackListJwt;
import com.example.MyBookShop.data.repositories.BlackListJwtRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Date;
import java.util.List;

@Configuration
@EnableScheduling
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class DeleteFromBlackListTask {

    BlackListJwtRepository blackListJwtRepository;

    @Scheduled(fixedRate = 600000)
    public void controlBlackListJwtTokens() {

        List<BlackListJwt> blackListJwtsByDateBefore = blackListJwtRepository.findBlackListJwtsByDateBefore(new Date());

        for (BlackListJwt blackListJwt : blackListJwtsByDateBefore) {
            blackListJwtRepository.delete(blackListJwt);
            blackListJwtRepository.flush();
        }
    }
}
