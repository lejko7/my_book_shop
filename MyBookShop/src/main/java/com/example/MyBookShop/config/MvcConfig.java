package com.example.MyBookShop.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {

    @Value("${upload.path}")
    String uploadPath;
    @Value("${books.path}")
    String bookPath;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry
                .addResourceHandler("/" + uploadPath + "/**",
                        "/book/" + uploadPath + "/**",
                        "/search/" + uploadPath + "/**",
                        "/books/" + uploadPath + "/**")
                .addResourceLocations("file:" + uploadPath + "/")
                .setCachePeriod(0)
                .resourceChain(true);
        registry.addResourceHandler("/" + bookPath + "/**", "/book/" + bookPath + "/**")
                .addResourceLocations("file:" + bookPath + "/")
                .setCachePeriod(0)
                .resourceChain(true);
    }
}
