package com.example.MyBookShop.security;

import com.example.MyBookShop.data.entities.User;
import com.example.MyBookShop.data.repositories.UserRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class CustomDefaultOauth2UserService extends DefaultOAuth2UserService {

    UserRepository userRepository;

    @Override
    public OAuth2User loadUser(OAuth2UserRequest userRequest) throws OAuth2AuthenticationException {
        OAuth2User oAuth2User = super.loadUser(userRequest);

        try {
            return processOAuth2User(userRequest, oAuth2User);
        } catch (Exception ex) {
            throw new InternalAuthenticationServiceException(ex.getMessage(), ex.getCause());
        }
    }

    private OAuth2User processOAuth2User(OAuth2UserRequest userRequest, OAuth2User oAuth2User) {
        Long id = Long.parseLong(oAuth2User.getAttributes().get("id").toString());

        // see what other data from userRequest or oAuth2User you need

        User userOptional = userRepository.findUserByLoginEquals(id.toString());

        if (userOptional == null) {
            userOptional = new User();
            userOptional.setEmail(oAuth2User.getAttributes().get("email").toString());

            String[] split = oAuth2User.getAttributes().get("name").toString().split(" ");

            userOptional.setName(split[0]);
            userOptional.setSurname(split[1]);
            userOptional.setLogin(id.toString());
            userOptional.setPassword("");

            // set other needed data

            userRepository.saveAndFlush(userOptional);
        }

        Map<String, Object> attributes = new HashMap<>(oAuth2User.getAttributes());
        attributes.replace("id", String.valueOf(userOptional.getId()));

        return new DefaultOAuth2User(oAuth2User.getAuthorities(), attributes, "id");
    }
}
