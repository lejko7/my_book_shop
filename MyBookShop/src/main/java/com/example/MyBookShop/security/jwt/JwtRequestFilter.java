package com.example.MyBookShop.security.jwt;

import com.example.MyBookShop.data.repositories.BlackListJwtRepository;
import com.example.MyBookShop.security.BookstoreUserDetails;
import com.example.MyBookShop.security.BookstoreUserDetailsService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class JwtRequestFilter extends OncePerRequestFilter {

    BookstoreUserDetailsService bookstoreUserDetailsService;
    JWTUtil jwtUtil;
    BlackListJwtRepository blackListJwtRepository;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String token = null;
        String userName = null;
        Cookie[] cookies = request.getCookies();

        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("token")) {
                    token = cookie.getValue();

                    if (blackListJwtRepository.findBlackListJwtByTokenEquals(token) != null) {
                        throw new ServletException("Token into blacklist!");
                    }

                    userName = jwtUtil.extractUserName(token);
                }

                if (userName != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                    BookstoreUserDetails bookstoreUserDetails = (BookstoreUserDetails) bookstoreUserDetailsService.loadUserByUsername(userName);
                    if (jwtUtil.validateToken(token, bookstoreUserDetails)) {
                        UsernamePasswordAuthenticationToken authenticationToken =
                                new UsernamePasswordAuthenticationToken(bookstoreUserDetails,
                                        null,
                                        bookstoreUserDetails.getAuthorities());

                        authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                    }
                }
            }
        }

        filterChain.doFilter(request, response);
    }
}
