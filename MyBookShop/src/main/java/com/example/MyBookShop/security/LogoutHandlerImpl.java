package com.example.MyBookShop.security;

import com.example.MyBookShop.data.entities.BlackListJwt;
import com.example.MyBookShop.data.repositories.BlackListJwtRepository;
import com.example.MyBookShop.security.jwt.JWTUtil;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class LogoutHandlerImpl implements LogoutHandler {

    BlackListJwtRepository blackListJwtRepository;
    JWTUtil jwtUtil;

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        Cookie[] cookies = request.getCookies();

        String requestUri = request.getRequestURI();

        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("token")) {
                    String token = cookie.getValue();

                    if (requestUri.equals("/logout")) {

                        BlackListJwt blackListJwt = new BlackListJwt();
                        blackListJwt.setToken(token);
                        blackListJwt.setDate(jwtUtil.extractExpiration(token));

                        blackListJwtRepository.saveAndFlush(blackListJwt);
                    }
                }
            }
        }
    }
}
