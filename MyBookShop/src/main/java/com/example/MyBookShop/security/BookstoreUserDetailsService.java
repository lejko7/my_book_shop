package com.example.MyBookShop.security;

import com.example.MyBookShop.data.entities.User;
import com.example.MyBookShop.data.repositories.UserRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class BookstoreUserDetailsService implements UserDetailsService {

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String value) throws UsernameNotFoundException {
        User bookstoreUser = findByValue(value);

        if (bookstoreUser != null) {
            return new BookstoreUserDetails(bookstoreUser);
        } else {
            throw new UsernameNotFoundException("User not found");
        }
    }

    private User findByValue(String value) {
        if (VALID_EMAIL_ADDRESS_REGEX.matcher(value).find()) {

            return userRepository.findUserByEmailEquals(value);
        }

        if (value.matches("[^a-zA-Z]+")) {

            return userRepository.findUserByPhoneEquals(value);
        }

        return userRepository.findUserByLoginEquals(value);
    }
}
