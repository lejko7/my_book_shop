$(document).ready(function () {
    $('a.btn').click(function (event) {
        event.preventDefault();
        $('#myOverlay').fadeIn(297, function () {
            $('#myPopup').css('display', 'block').animate({opacity: 1}, 198);
        });
    });

    $('#myPopupClose, #myOverlay').click(function (event) {
        $('#myPopup').animate({opacity: 1}, 198, function () {
            $(this).css('display', 'none');
            $('#myOverlay').fadeOut(297);
        });
    });
});
