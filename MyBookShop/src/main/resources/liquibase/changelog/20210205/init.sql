create table authors
(
    id         BIGINT PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    last_name  VARCHAR(50) NOT NULL,
    biography  TEXT        NOT NULL
);

CREATE TABLE books
(
    id        BIGSERIAL PRIMARY KEY,
    author_id INT           not null,
    title     VARCHAR(1000) NOT NULL,
    price_old VARCHAR(50) DEFAULT NULL,
    price     VARCHAR(50) DEFAULT NULL,

    FOREIGN KEY (author_id) references authors (id)
);

