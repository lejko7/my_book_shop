insert into books (id, author_id, title, price_old, price)
values (1, 304, 'Jungle Fighters', '$2782.91', '$1549.23');
insert into books (id, author_id, title, price_old, price)
values (2, 241, 'Goldfinger', '$3361.74', '$1359.89');
insert into books (id, author_id, title, price_old, price)
values (3, 192, 'Torch Song', '$4933.16', '$1402.97');
insert into books (id, author_id, title, price_old, price)
values (4, 135, 'Idiot, The (Hakuchi)', '$4881.97', '$510.85');
insert into books (id, author_id, title, price_old, price)
values (5, 609, 'Britannia Hospital', '$3407.88', '$213.90');
insert into books (id, author_id, title, price_old, price)
values (6, 509, 'Hard-Boiled (Lat sau san taam)', '$4921.82', '$1462.66');
insert into books (id, author_id, title, price_old, price)
values (7, 25, 'To Encourage the Others', '$4819.41', '$1903.48');
insert into books (id, author_id, title, price_old, price)
values (8, 8, 'Darjeeling Limited, The', '$2421.62', '$1921.11');
insert into books (id, author_id, title, price_old, price)
values (9, 521, 'Mole People, The', '$2754.28', '$1028.89');
insert into books (id, author_id, title, price_old, price)
values (10, 851, 'Gumshoe', '$2639.04', '$1541.80');
insert into books (id, author_id, title, price_old, price)
values (11, 710, 'Rocker', '$3605.10', '$1045.05');
insert into books (id, author_id, title, price_old, price)
values (12, 390, 'Enthusiasm (Entuziazm: Simfoniya Donbassa)', '$4725.88', '$493.52');
insert into books (id, author_id, title, price_old, price)
values (13, 447, 'Awesome; I Fuckin Shot That!', '$2853.84', '$1139.48');
insert into books (id, author_id, title, price_old, price)
values (14, 987, 'Arsène Lupin Returns', '$3524.94', '$946.68');
insert into books (id, author_id, title, price_old, price)
values (15, 230, 'Nativity Story, The', '$4089.13', '$1099.64');
insert into books (id, author_id, title, price_old, price)
values (16, 847, 'Public Speaking', '$2596.23', '$1987.80');
insert into books (id, author_id, title, price_old, price)
values (17, 952, 'French Lieutenant.s Woman, The', '$3608.70', '$1586.44');
insert into books (id, author_id, title, price_old, price)
values (18, 571, 'That.s Black Entertainment', '$3059.19', '$1499.11');
insert into books (id, author_id, title, price_old, price)
values (19, 855, 'Party, The (Boum, La)', '$4425.97', '$1492.88');
insert into books (id, author_id, title, price_old, price)
values (20, 46, 'December 7th', '$3611.01', '$1724.94');
insert into books (id, author_id, title, price_old, price)
values (21, 341, 'Dish, The', '$4025.75', '$1198.00');
insert into books (id, author_id, title, price_old, price)
values (22, 732, 'Thirst', '$3815.81', '$1820.51');
insert into books (id, author_id, title, price_old, price)
values (23, 795, 'The Great Scout & Cathouse Thursday', '$3180.07', '$1841.48');
insert into books (id, author_id, title, price_old, price)
values (24, 742, 'Go-Getter, The', '$2454.78', '$1619.97');
insert into books (id, author_id, title, price_old, price)
values (25, 451, 'The Man Who Played God', '$3009.85', '$1475.03');
insert into books (id, author_id, title, price_old, price)
values (26, 282, 'Bus 174 (Ônibus 174)', '$3495.29', '$1821.40');
insert into books (id, author_id, title, price_old, price)
values (27, 72, 'Far North', '$4030.32', '$1829.94');
insert into books (id, author_id, title, price_old, price)
values (28, 925, 'Ski School', '$3823.95', '$1918.78');
insert into books (id, author_id, title, price_old, price)
values (29, 886, 'Ghoul, The', '$3709.94', '$1212.15');
insert into books (id, author_id, title, price_old, price)
values (30, 1, 'Number Seventeen (a.k.a. Number 17)', '$2189.70', '$807.36');
insert into books (id, author_id, title, price_old, price)
values (31, 732, 'Heavy Metal 2000', '$2365.54', '$1839.18');
insert into books (id, author_id, title, price_old, price)
values (32, 342, 'Cobweb, The', '$3327.02', '$669.46');
insert into books (id, author_id, title, price_old, price)
values (33, 890, 'Land Ho!', '$3641.61', '$549.68');
insert into books (id, author_id, title, price_old, price)
values (34, 847, 'Grown Ups', '$4445.60', '$469.98');
insert into books (id, author_id, title, price_old, price)
values (35, 429, 'Low Life, The', '$2790.28', '$218.91');
insert into books (id, author_id, title, price_old, price)
values (36, 70, '100 Ways to Murder Your Wife (Sha qi er ren zu)', '$4433.15', '$1550.47');
insert into books (id, author_id, title, price_old, price)
values (37, 444, 'Man of Steel', '$4387.54', '$1930.75');
insert into books (id, author_id, title, price_old, price)
values (38, 101, 'Jungle2Jungle (a.k.a. Jungle 2 Jungle)', '$3794.22', '$1469.66');
insert into books (id, author_id, title, price_old, price)
values (39, 552, 'Fighter, The', '$2046.61', '$389.58');
insert into books (id, author_id, title, price_old, price)
values (40, 44, 'Att stjäla en tjuv', '$4503.41', '$1749.77');
insert into books (id, author_id, title, price_old, price)
values (41, 986, 'Wedlock', '$3117.30', '$866.82');
insert into books (id, author_id, title, price_old, price)
values (42, 548, 'Inn of the Sixth Happiness, The', '$2742.50', '$1693.33');
insert into books (id, author_id, title, price_old, price)
values (43, 30, 'Megaforce', '$3653.38', '$976.73');
insert into books (id, author_id, title, price_old, price)
values (44, 430, 'Mother of George', '$4545.63', '$1355.08');
insert into books (id, author_id, title, price_old, price)
values (45, 364, 'Platinum Blonde', '$4005.93', '$331.86');
insert into books (id, author_id, title, price_old, price)
values (46, 745, 'Beyond the Law (Beyond the Law - Blue)', '$2978.22', '$425.13');
insert into books (id, author_id, title, price_old, price)
values (47, 886, 'Hallelujah!', '$4205.78', '$1490.77');
insert into books (id, author_id, title, price_old, price)
values (48, 639, 'September', '$3711.16', '$1958.03');
insert into books (id, author_id, title, price_old, price)
values (49, 366, 'Tramp, The (Awaara) (Awara)', '$2784.38', '$469.62');
insert into books (id, author_id, title, price_old, price)
values (50, 491, 'Sins of My Father', '$4758.56', '$1676.94');
insert into books (id, author_id, title, price_old, price)
values (51, 539, 'Broken Flowers', '$2674.65', '$359.37');
insert into books (id, author_id, title, price_old, price)
values (52, 793, 'Dolphin Tale', '$3170.12', '$1930.34');
insert into books (id, author_id, title, price_old, price)
values (53, 644, 'Jackass: The Movie', '$2711.13', '$331.24');
insert into books (id, author_id, title, price_old, price)
values (54, 252, 'But Forever in My Mind', '$3150.24', '$770.57');
insert into books (id, author_id, title, price_old, price)
values (55, 273, 'Manon', '$3803.07', '$1882.59');
insert into books (id, author_id, title, price_old, price)
values (56, 247, 'Girl From Nowhere, The', '$4683.02', '$243.16');
insert into books (id, author_id, title, price_old, price)
values (57, 666, 'Pudana Last of the Line (Sukunsa viimeinen)', '$2468.51', '$1362.69');
insert into books (id, author_id, title, price_old, price)
values (58, 650, 'Millions', '$3019.00', '$1641.11');
insert into books (id, author_id, title, price_old, price)
values (59, 773, 'Guru, The', '$4931.72', '$600.72');
insert into books (id, author_id, title, price_old, price)
values (60, 129, 'Stuart Little 2', '$3557.14', '$1148.92');
insert into books (id, author_id, title, price_old, price)
values (61, 913, 'Family Resemblances (Un air de famille)', '$4175.45', '$1769.11');
insert into books (id, author_id, title, price_old, price)
values (62, 283, 'Boyfriends and Girlfriends (a.k.a. My Girlfriend.s Boyfriend) (L.ami de mon amie)', '$3219.36',
        '$1479.03');
insert into books (id, author_id, title, price_old, price)
values (63, 315, 'Losing Isaiah', '$3881.59', '$1261.36');
insert into books (id, author_id, title, price_old, price)
values (64, 710, 'Toy Story Toons: Hawaiian Vacation', '$2735.57', '$1492.54');
insert into books (id, author_id, title, price_old, price)
values (65, 650, 'I.ll See You in My Dreams', '$4622.36', '$1982.59');
insert into books (id, author_id, title, price_old, price)
values (66, 465, 'Onion Movie, The', '$2851.34', '$1748.20');
insert into books (id, author_id, title, price_old, price)
values (67, 589, 'Shade', '$4448.34', '$1782.02');
insert into books (id, author_id, title, price_old, price)
values (68, 505, 'Carbine Williams', '$4664.20', '$1106.87');
insert into books (id, author_id, title, price_old, price)
values (69, 983, 'Where the Money Is', '$2302.84', '$1275.62');
insert into books (id, author_id, title, price_old, price)
values (70, 855, 'Bloody Pit of Horror (Il boia scarlatto) (Virgins for the Hangman)', '$3587.90', '$1532.41');
insert into books (id, author_id, title, price_old, price)
values (71, 997, 'All Good Things', '$3989.42', '$1452.20');
insert into books (id, author_id, title, price_old, price)
values (72, 991, 'Face in the Crowd, A', '$4268.24', '$520.86');
insert into books (id, author_id, title, price_old, price)
values (73, 324, 'Scott Joplin', '$4295.78', '$713.08');
insert into books (id, author_id, title, price_old, price)
values (74, 204, 'Het Vonnis', '$4876.78', '$250.35');
insert into books (id, author_id, title, price_old, price)
values (75, 356, 'Pickpocket', '$4971.88', '$1538.19');
insert into books (id, author_id, title, price_old, price)
values (76, 821, 'Hebrew Hammer, The', '$4526.28', '$276.52');
insert into books (id, author_id, title, price_old, price)
values (77, 636, 'Münchhausen', '$3882.28', '$1250.94');
insert into books (id, author_id, title, price_old, price)
values (78, 941, 'Katt Williams: Priceless: Afterlife', '$2560.68', '$801.04');
insert into books (id, author_id, title, price_old, price)
values (79, 465, 'Police Academy: Mission to Moscow', '$2834.76', '$654.08');
insert into books (id, author_id, title, price_old, price)
values (80, 672, 'Thick-Walled Room, The (Kabe atsuki heya)', '$4772.01', '$1026.91');
insert into books (id, author_id, title, price_old, price)
values (81, 578, 'River Why, The', '$2818.04', '$725.02');
insert into books (id, author_id, title, price_old, price)
values (82, 552, 'Ernest Rides Again', '$2534.06', '$1847.42');
insert into books (id, author_id, title, price_old, price)
values (83, 916, 'Son of God', '$3605.88', '$1826.54');
insert into books (id, author_id, title, price_old, price)
values (84, 108, 'Devil in the Flesh', '$3508.03', '$1226.23');
insert into books (id, author_id, title, price_old, price)
values (85, 12, 'Other Boleyn Girl, The', '$3021.58', '$1803.35');
insert into books (id, author_id, title, price_old, price)
values (86, 118, 'Extraordinary Adventures of Adèle Blanc-Sec, The', '$3971.71', '$1216.43');
insert into books (id, author_id, title, price_old, price)
values (87, 681, 'Very Good Girls', '$2899.37', '$1306.34');
insert into books (id, author_id, title, price_old, price)
values (88, 812, 'The Violent Enemy', '$4855.49', '$772.36');
insert into books (id, author_id, title, price_old, price)
values (89, 965, '¡Alambrista! (Illegal, The)', '$3248.24', '$1052.37');
insert into books (id, author_id, title, price_old, price)
values (90, 414, '9', '$4396.89', '$1513.12');
insert into books (id, author_id, title, price_old, price)
values (91, 281, 'Little Miss Sunshine', '$2846.55', '$1890.58');
insert into books (id, author_id, title, price_old, price)
values (92, 403, 'Invisible Waves', '$4139.37', '$1011.26');
insert into books (id, author_id, title, price_old, price)
values (93, 299, 'Brothers McMullen, The', '$2398.29', '$955.04');
insert into books (id, author_id, title, price_old, price)
values (94, 713, 'Powerpuff Girls, The', '$2903.10', '$1868.03');
insert into books (id, author_id, title, price_old, price)
values (95, 467, 'Bobby Fischer Against the World', '$4037.78', '$439.36');
insert into books (id, author_id, title, price_old, price)
values (96, 576, 'Farewell to Arms, A', '$3968.83', '$824.26');
insert into books (id, author_id, title, price_old, price)
values (97, 295, 'Air Force One', '$4137.62', '$942.14');
insert into books (id, author_id, title, price_old, price)
values (98, 637, 'Navajo Joe', '$4301.57', '$393.82');
insert into books (id, author_id, title, price_old, price)
values (99, 662, 'Grandview, U.S.A.', '$3629.45', '$963.60');
insert into books (id, author_id, title, price_old, price)
values (100, 611, 'Stomp the Yard', '$2704.77', '$810.47');
insert into books (id, author_id, title, price_old, price)
values (101, 543, 'Lifeboat', '$2064.21', '$1277.92');
insert into books (id, author_id, title, price_old, price)
values (102, 147, 'Pillow of Death', '$3992.65', '$927.71');
insert into books (id, author_id, title, price_old, price)
values (103, 388, 'Night and Day (Bam gua nat)', '$2703.69', '$832.71');
insert into books (id, author_id, title, price_old, price)
values (104, 127, 'Itty Bitty Titty Committee', '$4092.69', '$703.20');
insert into books (id, author_id, title, price_old, price)
values (105, 495, 'The Squeeze', '$3520.31', '$1523.93');
insert into books (id, author_id, title, price_old, price)
values (106, 25, 'Mr. Jealousy', '$2031.79', '$636.36');
insert into books (id, author_id, title, price_old, price)
values (107, 376, 'Big Game', '$2357.70', '$1122.85');
insert into books (id, author_id, title, price_old, price)
values (108, 82, 'Hamlet', '$3483.55', '$1644.18');
insert into books (id, author_id, title, price_old, price)
values (109, 346, 'Solas', '$4862.47', '$1429.82');
insert into books (id, author_id, title, price_old, price)
values (110, 54, 'Underworld', '$4270.93', '$1262.73');
insert into books (id, author_id, title, price_old, price)
values (111, 594, 'Robinson Crusoe (Adventures of Robinson Crusoe, The)', '$3521.41', '$1093.57');
insert into books (id, author_id, title, price_old, price)
values (112, 534, 'Aviator, The', '$4949.37', '$703.68');
insert into books (id, author_id, title, price_old, price)
values (113, 63, 'Dinotopia', '$4682.54', '$774.89');
insert into books (id, author_id, title, price_old, price)
values (114, 432, 'Raw Deal: A Question of Consent', '$3592.47', '$914.26');
insert into books (id, author_id, title, price_old, price)
values (115, 223, 'Firecreek', '$4396.60', '$1386.23');
insert into books (id, author_id, title, price_old, price)
values (116, 827, 'Europeans, The', '$2973.33', '$576.04');
insert into books (id, author_id, title, price_old, price)
values (117, 831, 'Rock-a-Bye Baby', '$4077.34', '$1111.94');
insert into books (id, author_id, title, price_old, price)
values (118, 508, 'Killer McCoy', '$4010.77', '$1328.93');
insert into books (id, author_id, title, price_old, price)
values (119, 919, 'Moulin Rouge', '$4438.77', '$1906.66');
insert into books (id, author_id, title, price_old, price)
values (120, 184, 'Airspeed', '$4505.63', '$850.48');
insert into books (id, author_id, title, price_old, price)
values (121, 802, 'Second Man, The (O Defteros Andras)', '$3195.41', '$510.44');
insert into books (id, author_id, title, price_old, price)
values (122, 878, 'Humboldt County', '$3520.94', '$1659.93');
insert into books (id, author_id, title, price_old, price)
values (123, 201, 'Big Circus, The', '$4702.76', '$246.64');
insert into books (id, author_id, title, price_old, price)
values (124, 147, 'No Good Deed', '$3912.20', '$1504.23');
insert into books (id, author_id, title, price_old, price)
values (125, 129, 'Creature with the Atom Brain', '$4692.03', '$1432.44');
insert into books (id, author_id, title, price_old, price)
values (126, 671, 'Ator, the Fighting Eagle (Ator l.invincibile)', '$4041.58', '$1722.72');
insert into books (id, author_id, title, price_old, price)
values (127, 311, 'R.S.V.P. ', '$2044.39', '$1375.79');
insert into books (id, author_id, title, price_old, price)
values (128, 325, 'Highball', '$3239.40', '$1294.22');
insert into books (id, author_id, title, price_old, price)
values (129, 387, 'Voyage to the Bottom of the Sea', '$2653.46', '$383.00');
insert into books (id, author_id, title, price_old, price)
values (130, 734, 'Mooz-lum', '$3554.07', '$1284.32');
insert into books (id, author_id, title, price_old, price)
values (131, 892, 'Day and Night (Dag och natt)', '$3418.41', '$1456.14');
insert into books (id, author_id, title, price_old, price)
values (132, 406, 'Chicago', '$3436.57', '$1189.04');
insert into books (id, author_id, title, price_old, price)
values (133, 657, 'Black Mask (Hak hap)', '$3191.67', '$315.61');
insert into books (id, author_id, title, price_old, price)
values (134, 275, 'Vanity Fair', '$3871.60', '$266.13');
insert into books (id, author_id, title, price_old, price)
values (135, 25, 'Ruby Sparks', '$3456.24', '$1280.91');
insert into books (id, author_id, title, price_old, price)
values (136, 705, 'Narcissus', '$3337.71', '$1341.38');
insert into books (id, author_id, title, price_old, price)
values (137, 396, 'Road House', '$2781.51', '$1163.71');
insert into books (id, author_id, title, price_old, price)
values (138, 357, 'Bob & Carol & Ted & Alice', '$4481.93', '$750.92');
insert into books (id, author_id, title, price_old, price)
values (139, 274, 'Fine, Totally Fine (Zenzen Daijobu)', '$2700.79', '$1926.35');
insert into books (id, author_id, title, price_old, price)
values (140, 228, 'Infestation', '$3605.41', '$1301.33');
insert into books (id, author_id, title, price_old, price)
values (141, 102, 'Balzac and the Little Chinese Seamstress (Xiao cai feng)', '$4027.25', '$364.63');
insert into books (id, author_id, title, price_old, price)
values (142, 740, 'Definitely, Maybe', '$4043.37', '$1793.76');
insert into books (id, author_id, title, price_old, price)
values (143, 696, 'Inhale', '$2470.45', '$566.33');
insert into books (id, author_id, title, price_old, price)
values (144, 478, 'Paranoia', '$4394.95', '$1655.86');
insert into books (id, author_id, title, price_old, price)
values (145, 298, 'Adventures of Mary-Kate and Ashley, The: The Case of the United States Navy Adventure', '$4637.93',
        '$1296.31');
insert into books (id, author_id, title, price_old, price)
values (146, 135, 'Fail Safe', '$4864.46', '$1726.08');
insert into books (id, author_id, title, price_old, price)
values (147, 76, 'Lost Horizon', '$4185.08', '$825.56');
insert into books (id, author_id, title, price_old, price)
values (148, 705, 'Mystery Street', '$3962.76', '$311.76');
insert into books (id, author_id, title, price_old, price)
values (149, 313, 'Stargate', '$2823.84', '$240.13');
insert into books (id, author_id, title, price_old, price)
values (150, 901, 'Oh, Woe Is Me (Hélas pour moi)', '$3213.43', '$814.14');
insert into books (id, author_id, title, price_old, price)
values (151, 583, 'Bad Education (La mala educación)', '$4429.12', '$330.27');
insert into books (id, author_id, title, price_old, price)
values (152, 239, 'Little Witches', '$4547.18', '$885.17');
insert into books (id, author_id, title, price_old, price)
values (153, 867, 'Marriage Made in Heaven, A (Rab Ne Bana Di Jodi)', '$2760.12', '$700.74');
insert into books (id, author_id, title, price_old, price)
values (154, 993, 'Kill Your Darlings', '$4677.24', '$1517.97');
insert into books (id, author_id, title, price_old, price)
values (155, 23, 'Road to Rio', '$3461.68', '$600.37');
insert into books (id, author_id, title, price_old, price)
values (156, 16, 'Natural, The', '$2682.97', '$1030.33');
insert into books (id, author_id, title, price_old, price)
values (157, 843, 'So Undercover', '$4653.40', '$1482.84');
insert into books (id, author_id, title, price_old, price)
values (158, 610, 'Hell Is for Heroes', '$2682.31', '$1139.03');
insert into books (id, author_id, title, price_old, price)
values (159, 823, 'Sayonara', '$2345.63', '$308.03');
insert into books (id, author_id, title, price_old, price)
values (160, 494, 'Women Aren.t Funny', '$4260.38', '$1892.14');
insert into books (id, author_id, title, price_old, price)
values (161, 167, 'Fast Food', '$2491.49', '$1712.92');
insert into books (id, author_id, title, price_old, price)
values (162, 654, 'Latitude Zero (Ido zero daisakusen)', '$3383.15', '$250.08');
insert into books (id, author_id, title, price_old, price)
values (163, 906, 'Nathalie...', '$4607.15', '$876.54');
insert into books (id, author_id, title, price_old, price)
values (164, 637, 'Baby.s Day Out', '$2755.20', '$886.56');
insert into books (id, author_id, title, price_old, price)
values (165, 476, 'Machine Girl, The (Kataude mashin gâru)', '$2550.69', '$1859.72');
insert into books (id, author_id, title, price_old, price)
values (166, 553, 'Shock and Awe: The Story of Electricity', '$2698.32', '$1333.49');
insert into books (id, author_id, title, price_old, price)
values (167, 811, 'Red Cliff Part II (Chi Bi Xia: Jue Zhan Tian Xia)', '$3106.05', '$1007.47');
insert into books (id, author_id, title, price_old, price)
values (168, 86, 'My Blue Heaven', '$2362.79', '$647.37');
insert into books (id, author_id, title, price_old, price)
values (169, 373, 'Borrowers, The', '$4920.13', '$295.67');
insert into books (id, author_id, title, price_old, price)
values (170, 225, 'Hunky Dory', '$2554.74', '$1157.10');
insert into books (id, author_id, title, price_old, price)
values (171, 335, 'Bomb the System', '$2952.31', '$1280.46');
insert into books (id, author_id, title, price_old, price)
values (172, 164, 'Dr. Goldfoot and the Girl Bombs (Le spie vengono dal semifreddo)', '$3772.05', '$373.50');
insert into books (id, author_id, title, price_old, price)
values (173, 317, 'Unforgettable', '$4237.60', '$1221.89');
insert into books (id, author_id, title, price_old, price)
values (174, 284, 'Moby Dick', '$3221.95', '$211.71');
insert into books (id, author_id, title, price_old, price)
values (175, 114, 'Christmas Story (Joulutarina)', '$2823.90', '$326.37');
insert into books (id, author_id, title, price_old, price)
values (176, 309, 'My Sister Eileen', '$3250.93', '$711.29');
insert into books (id, author_id, title, price_old, price)
values (177, 248, 'A Girl Walks Home Alone at Night', '$4354.82', '$836.76');
insert into books (id, author_id, title, price_old, price)
values (178, 269, 'Sudden Impact', '$4726.09', '$1715.94');
insert into books (id, author_id, title, price_old, price)
values (179, 61, 'The Boy in the Mirror', '$3061.79', '$765.51');
insert into books (id, author_id, title, price_old, price)
values (180, 200, 'Agatha', '$3564.69', '$401.46');
insert into books (id, author_id, title, price_old, price)
values (181, 16, '4 Little Girls', '$4072.73', '$1074.46');
insert into books (id, author_id, title, price_old, price)
values (182, 804, 'Today.s Special', '$2044.31', '$590.05');
insert into books (id, author_id, title, price_old, price)
values (183, 477, 'Hollidaysburg', '$3199.23', '$878.62');
insert into books (id, author_id, title, price_old, price)
values (184, 480, 'Superstar: The Karen Carpenter Story', '$4913.87', '$1127.64');
insert into books (id, author_id, title, price_old, price)
values (185, 491, 'New Rulers of the World, The', '$4466.36', '$1842.68');
insert into books (id, author_id, title, price_old, price)
values (186, 730, 'Leech Woman, The', '$3276.71', '$947.48');
insert into books (id, author_id, title, price_old, price)
values (187, 830, 'Dedication', '$4043.15', '$763.58');
insert into books (id, author_id, title, price_old, price)
values (188, 360, 'Gray.s Anatomy', '$2870.17', '$972.42');
insert into books (id, author_id, title, price_old, price)
values (189, 572, 'Cain and Mabel', '$3844.37', '$809.43');
insert into books (id, author_id, title, price_old, price)
values (190, 700, 'Mission to Mir', '$3152.13', '$409.28');
insert into books (id, author_id, title, price_old, price)
values (191, 552, 'Ambush (Mai fu)', '$3039.75', '$793.58');
insert into books (id, author_id, title, price_old, price)
values (192, 680, 'Birdy', '$3462.32', '$264.43');
insert into books (id, author_id, title, price_old, price)
values (193, 857, 'Armageddon 2012', '$2683.82', '$1081.40');
insert into books (id, author_id, title, price_old, price)
values (194, 10, 'Hands Over the City (Le mani sulla città)', '$3269.14', '$297.30');
insert into books (id, author_id, title, price_old, price)
values (195, 212, 'Outside Ozona', '$3899.90', '$1653.51');
insert into books (id, author_id, title, price_old, price)
values (196, 217, 'Bye Bye, Love', '$4681.68', '$571.38');
insert into books (id, author_id, title, price_old, price)
values (197, 108, 'You.re Telling Me!', '$3298.64', '$1555.43');
insert into books (id, author_id, title, price_old, price)
values (198, 514, 'Mother, The', '$3527.24', '$1621.92');
insert into books (id, author_id, title, price_old, price)
values (199, 748, 'Versailles', '$3754.67', '$1720.52');
insert into books (id, author_id, title, price_old, price)
values (200, 393, 'Hidden, The', '$2309.84', '$1036.58');
insert into books (id, author_id, title, price_old, price)
values (201, 849, '10 Items or Less', '$2179.10', '$838.26');
insert into books (id, author_id, title, price_old, price)
values (202, 503, 'Fate Is the Hunter', '$2713.04', '$1287.20');
insert into books (id, author_id, title, price_old, price)
values (203, 422, 'Farewell to Arms, A', '$3586.33', '$994.38');
insert into books (id, author_id, title, price_old, price)
values (204, 895, 'Don.t Deliver Us from Evil (Mais ne nous délivrez pas du mal)', '$2546.28', '$1980.63');
insert into books (id, author_id, title, price_old, price)
values (205, 672, 'Hardcore', '$4936.91', '$1111.29');
insert into books (id, author_id, title, price_old, price)
values (206, 292, 'Donner Party, The', '$3611.48', '$1205.46');
insert into books (id, author_id, title, price_old, price)
values (207, 400, 'Atragon (Kaitei Gunkan)', '$3852.35', '$375.62');
insert into books (id, author_id, title, price_old, price)
values (208, 399, 'American Dreams in China', '$4120.42', '$965.39');
insert into books (id, author_id, title, price_old, price)
values (209, 59, 'Virgin Suicides, The', '$3579.02', '$1900.99');
insert into books (id, author_id, title, price_old, price)
values (210, 868, 'Virtual JFK: Vietnam If Kennedy Had Lived', '$4306.03', '$877.75');
insert into books (id, author_id, title, price_old, price)
values (211, 808, '2LDK', '$2988.48', '$438.77');
insert into books (id, author_id, title, price_old, price)
values (212, 543, 'Mystics in Bali (Leák)', '$3310.59', '$1841.08');
insert into books (id, author_id, title, price_old, price)
values (213, 816, 'History of the Eagles', '$2357.46', '$1516.57');
insert into books (id, author_id, title, price_old, price)
values (214, 724, 'Antisocial', '$3691.02', '$1779.00');
insert into books (id, author_id, title, price_old, price)
values (215, 683, '61*', '$4843.01', '$708.85');
insert into books (id, author_id, title, price_old, price)
values (216, 504, 'Monster X Strikes Back: Attack the G8 Summit, The (Girara no gyakushû: Tôya-ko Samitto kikiippatsu)',
        '$4679.19', '$1071.98');
insert into books (id, author_id, title, price_old, price)
values (217, 992, 'Naan Kadavul', '$4610.32', '$1680.78');
insert into books (id, author_id, title, price_old, price)
values (218, 779, 'Bloodlust: Subspecies III', '$4728.20', '$1453.90');
insert into books (id, author_id, title, price_old, price)
values (219, 643, 'Born American', '$2811.99', '$970.32');
insert into books (id, author_id, title, price_old, price)
values (220, 258, 'Clean Slate', '$2279.87', '$234.41');
insert into books (id, author_id, title, price_old, price)
values (221, 235, 'One Day in September', '$4979.92', '$1620.85');
insert into books (id, author_id, title, price_old, price)
values (222, 494, 'Wal-Mart: The High Cost of Low Price', '$3816.94', '$1510.17');
insert into books (id, author_id, title, price_old, price)
values (223, 393, 'Medicine Man', '$4357.24', '$1016.77');
insert into books (id, author_id, title, price_old, price)
values (224, 511, 'Crime Wave', '$4575.39', '$429.12');
insert into books (id, author_id, title, price_old, price)
values (225, 217, 'Young Guns', '$3581.97', '$1729.06');
insert into books (id, author_id, title, price_old, price)
values (226, 295, 'Frank McKlusky, C.I.', '$4719.62', '$1685.22');
insert into books (id, author_id, title, price_old, price)
values (227, 402, 'Secret Garden, The', '$3302.06', '$487.42');
insert into books (id, author_id, title, price_old, price)
values (228, 723, 'California', '$3905.63', '$1521.89');
insert into books (id, author_id, title, price_old, price)
values (229, 346, 'Pellet (Bola, El)', '$3664.48', '$1690.53');
insert into books (id, author_id, title, price_old, price)
values (230, 112, 'Move (3 Zimmer/Küche/Bad)', '$2382.33', '$1507.64');
insert into books (id, author_id, title, price_old, price)
values (231, 334, 'Caveman', '$4870.41', '$1185.61');
insert into books (id, author_id, title, price_old, price)
values (232, 983, 'Playing by Heart', '$3187.27', '$1814.10');
insert into books (id, author_id, title, price_old, price)
values (233, 80, 'Love Liza', '$2329.15', '$1232.12');
insert into books (id, author_id, title, price_old, price)
values (234, 107, 'High Risk', '$2589.89', '$791.89');
insert into books (id, author_id, title, price_old, price)
values (235, 583, 'Bartleby', '$3681.51', '$1541.03');
insert into books (id, author_id, title, price_old, price)
values (236, 716, 'Pistol Opera (Pisutoru opera)', '$4354.54', '$1208.38');
insert into books (id, author_id, title, price_old, price)
values (237, 181, 'I Don.t Want to Sleep Alone (Hei yan quan)', '$4362.87', '$1301.46');
insert into books (id, author_id, title, price_old, price)
values (238, 932, 'Curb Dance', '$2748.57', '$342.68');
insert into books (id, author_id, title, price_old, price)
values (239, 609, 'Where Danger Lives', '$2904.31', '$1940.91');
insert into books (id, author_id, title, price_old, price)
values (240, 133, 'Password: Uccidete agente Gordon', '$4703.42', '$571.30');
insert into books (id, author_id, title, price_old, price)
values (241, 924, 'Disney Princess Collection: Jasmine.s Enchanted Tales: Jasmine.s Wish', '$2605.52', '$1414.50');
insert into books (id, author_id, title, price_old, price)
values (242, 120, 'Union, The', '$3661.15', '$770.86');
insert into books (id, author_id, title, price_old, price)
values (243, 814, 'Ghost Ship, The', '$2585.76', '$848.63');
insert into books (id, author_id, title, price_old, price)
values (244, 707, 'Exodus: Gods and Kings', '$4228.30', '$201.91');
insert into books (id, author_id, title, price_old, price)
values (245, 399, 'Flipper', '$3705.20', '$210.86');
insert into books (id, author_id, title, price_old, price)
values (246, 80, 'Animal', '$4903.86', '$1472.72');
insert into books (id, author_id, title, price_old, price)
values (247, 31, 'Pompeii', '$4639.39', '$1154.12');
insert into books (id, author_id, title, price_old, price)
values (248, 966, 'Eye In The Sky (Gun chung)', '$4966.46', '$787.25');
insert into books (id, author_id, title, price_old, price)
values (249, 328, 'Kimberly', '$4934.54', '$903.79');
insert into books (id, author_id, title, price_old, price)
values (250, 499, 'Tuck Everlasting', '$3279.67', '$1105.32');
insert into books (id, author_id, title, price_old, price)
values (251, 896, 'Happy Together', '$3437.40', '$1103.70');
insert into books (id, author_id, title, price_old, price)
values (252, 930, 'Incredible Burt Wonderstone, The', '$4424.79', '$1363.67');
insert into books (id, author_id, title, price_old, price)
values (253, 690, 'Brother (Brat)', '$3922.06', '$1348.96');
insert into books (id, author_id, title, price_old, price)
values (254, 598, 'Carried Away', '$3319.38', '$1468.03');
insert into books (id, author_id, title, price_old, price)
values (255, 471, 'Across the Wide Missouri', '$4691.31', '$699.33');
insert into books (id, author_id, title, price_old, price)
values (256, 96, 'In Their Skin', '$2054.68', '$280.95');
insert into books (id, author_id, title, price_old, price)
values (257, 450, 'In Cold Blood', '$3130.36', '$1355.62');
insert into books (id, author_id, title, price_old, price)
values (258, 79, 'Bring It On: All or Nothing', '$2706.73', '$503.21');
insert into books (id, author_id, title, price_old, price)
values (259, 996, 'Punk in London', '$2906.98', '$1074.91');
insert into books (id, author_id, title, price_old, price)
values (260, 545, 'Feast of All Saints', '$4702.49', '$1309.14');
insert into books (id, author_id, title, price_old, price)
values (261, 399, 'Save Me', '$3431.34', '$650.19');
insert into books (id, author_id, title, price_old, price)
values (262, 606, 'Forget Paris', '$4139.49', '$330.21');
insert into books (id, author_id, title, price_old, price)
values (263, 683, 'Lebanon, Pa.', '$2004.98', '$723.87');
insert into books (id, author_id, title, price_old, price)
values (264, 702, 'Swimming Pool', '$4384.55', '$449.57');
insert into books (id, author_id, title, price_old, price)
values (265, 147, 'Trader Horn', '$4352.27', '$427.51');
insert into books (id, author_id, title, price_old, price)
values (266, 747, 'Gray.s Anatomy', '$4021.22', '$1220.41');
insert into books (id, author_id, title, price_old, price)
values (267, 310, 'Naked', '$2649.31', '$1503.15');
insert into books (id, author_id, title, price_old, price)
values (268, 691, 'Eye for an Eye, An (Silmä silmästä)', '$4085.07', '$914.62');
insert into books (id, author_id, title, price_old, price)
values (269, 955, 'Deficit (Déficit)', '$2748.73', '$297.21');
insert into books (id, author_id, title, price_old, price)
values (270, 660, 'Phenix City Story, The', '$4433.41', '$1073.18');
insert into books (id, author_id, title, price_old, price)
values (271, 50, 'BURN-E', '$4519.97', '$816.01');
insert into books (id, author_id, title, price_old, price)
values (272, 724, 'Harry Potter and the Goblet of Fire', '$3474.47', '$1948.45');
insert into books (id, author_id, title, price_old, price)
values (273, 525, 'Daddy Day Camp', '$2494.05', '$912.36');
insert into books (id, author_id, title, price_old, price)
values (274, 179, 'Confidential Report', '$2442.10', '$1629.31');
insert into books (id, author_id, title, price_old, price)
values (275, 347, 'Buddy Buddy', '$3288.16', '$850.55');
insert into books (id, author_id, title, price_old, price)
values (276, 580, 'Watcher in the Woods, The', '$3202.61', '$1400.15');
insert into books (id, author_id, title, price_old, price)
values (277, 332, 'Storm Warning', '$3932.98', '$1395.34');
insert into books (id, author_id, title, price_old, price)
values (278, 818, 'Rosewood', '$2551.03', '$1291.02');
insert into books (id, author_id, title, price_old, price)
values (279, 825, 'Tie That Binds, The', '$2715.73', '$628.66');
insert into books (id, author_id, title, price_old, price)
values (280, 810, 'Rocks in my Pockets', '$2639.92', '$1042.68');
insert into books (id, author_id, title, price_old, price)
values (281, 299, 'Quarantine', '$3429.95', '$1361.38');
insert into books (id, author_id, title, price_old, price)
values (282, 932, 'Main Event, The', '$2227.57', '$1416.91');
insert into books (id, author_id, title, price_old, price)
values (283, 650, 'Edie & Pen', '$4399.72', '$786.12');
insert into books (id, author_id, title, price_old, price)
values (284, 991, 'High Strung', '$3395.22', '$1334.32');
insert into books (id, author_id, title, price_old, price)
values (285, 194, 'Below', '$3228.03', '$707.04');
insert into books (id, author_id, title, price_old, price)
values (286, 606, 'London to Brighton', '$4020.58', '$1065.07');
insert into books (id, author_id, title, price_old, price)
values (287, 417, 'Tea and Sympathy', '$4761.54', '$1469.56');
insert into books (id, author_id, title, price_old, price)
values (288, 656, 'Two Escobars, The', '$3731.38', '$1269.45');
insert into books (id, author_id, title, price_old, price)
values (289, 601, 'Addams Family Values', '$2365.17', '$1002.45');
insert into books (id, author_id, title, price_old, price)
values (290, 540, 'Tokyo Story (Tôkyô monogatari)', '$2387.44', '$1322.67');
insert into books (id, author_id, title, price_old, price)
values (291, 858, 'Knight Moves', '$2928.52', '$265.54');
insert into books (id, author_id, title, price_old, price)
values (292, 969, 'Lisbela e o Prisioneiro (Lisbela and the Prisoner)', '$2679.92', '$487.75');
insert into books (id, author_id, title, price_old, price)
values (293, 528, 'Devil and Miss Jones, The', '$4723.76', '$1524.41');
insert into books (id, author_id, title, price_old, price)
values (294, 411, 'The Great Scout & Cathouse Thursday', '$2058.78', '$1889.31');
insert into books (id, author_id, title, price_old, price)
values (295, 187, 'Wish You Were Here', '$3060.90', '$768.54');
insert into books (id, author_id, title, price_old, price)
values (296, 792, 'Zotz!', '$2714.13', '$1756.72');
insert into books (id, author_id, title, price_old, price)
values (297, 342, 'The Aggression Scale', '$3610.64', '$1926.88');
insert into books (id, author_id, title, price_old, price)
values (298, 287, 'Cocaine Cowboys', '$2963.41', '$1523.34');
insert into books (id, author_id, title, price_old, price)
values (299, 879, 'Jack Frost 2: Revenge of the Mutant Killer Snowman', '$3588.31', '$248.65');
insert into books (id, author_id, title, price_old, price)
values (300, 906, 'Old Dogs', '$3485.33', '$807.27');
insert into books (id, author_id, title, price_old, price)
values (301, 924, 'Kiss Kiss - Bang Bang', '$4596.12', '$1464.29');
insert into books (id, author_id, title, price_old, price)
values (302, 450, 'Air Crew', '$4905.94', '$1681.34');
insert into books (id, author_id, title, price_old, price)
values (303, 719, 'Defending Your Life', '$2270.33', '$706.19');
insert into books (id, author_id, title, price_old, price)
values (304, 450, 'Crimes of Passion', '$2783.67', '$1990.36');
insert into books (id, author_id, title, price_old, price)
values (305, 111, 'My Demon Lover', '$2950.86', '$207.06');
insert into books (id, author_id, title, price_old, price)
values (306, 327, 'Children of Dune', '$3802.80', '$975.12');
insert into books (id, author_id, title, price_old, price)
values (307, 831, 'Two Days', '$3607.12', '$1956.84');
insert into books (id, author_id, title, price_old, price)
values (308, 689, 'Sharky.s Machine', '$4643.59', '$233.49');
insert into books (id, author_id, title, price_old, price)
values (309, 856, 'Affluenza', '$4953.14', '$573.96');
insert into books (id, author_id, title, price_old, price)
values (310, 657, 'Garfield.s Pet Force', '$4692.91', '$761.13');
insert into books (id, author_id, title, price_old, price)
values (311, 314, 'Go West', '$2092.77', '$448.25');
insert into books (id, author_id, title, price_old, price)
values (312, 282, 'Iceman', '$3119.82', '$904.61');
insert into books (id, author_id, title, price_old, price)
values (313, 673, 'Shine a Light', '$2975.69', '$1246.94');
insert into books (id, author_id, title, price_old, price)
values (314, 467, 'Burning, The', '$2447.78', '$1383.64');
insert into books (id, author_id, title, price_old, price)
values (315, 199, '1492: Conquest of Paradise', '$2936.45', '$1643.09');
insert into books (id, author_id, title, price_old, price)
values (316, 556, 'Trance', '$3451.23', '$276.44');
insert into books (id, author_id, title, price_old, price)
values (317, 252, 'Beautiful', '$3479.52', '$1947.46');
insert into books (id, author_id, title, price_old, price)
values (318, 197, 'We Need to Talk About Kevin', '$4799.13', '$343.31');
insert into books (id, author_id, title, price_old, price)
values (319, 37, 'To Encourage the Others', '$3930.77', '$868.37');
insert into books (id, author_id, title, price_old, price)
values (320, 315, 'One Wonderful Sunday (Subarashiki nichiyobi)', '$4351.54', '$208.38');
insert into books (id, author_id, title, price_old, price)
values (321, 465, 'Rage', '$2028.33', '$904.89');
insert into books (id, author_id, title, price_old, price)
values (322, 2, 'Deficit (Déficit)', '$3033.35', '$212.88');
insert into books (id, author_id, title, price_old, price)
values (323, 33, 'Pelle Svanslös i Amerikatt', '$4209.34', '$1541.78');
insert into books (id, author_id, title, price_old, price)
values (324, 234, 'Day and Night (Le jour et la nuit)', '$3529.42', '$1762.27');
insert into books (id, author_id, title, price_old, price)
values (325, 165, 'Lady and the Reaper, The (Dama y la muerte, La)', '$2211.94', '$1840.52');
insert into books (id, author_id, title, price_old, price)
values (326, 953, 'Momma.s Man', '$4905.36', '$1197.90');
insert into books (id, author_id, title, price_old, price)
values (327, 770, 'Lethal Weapon', '$4896.79', '$1477.47');
insert into books (id, author_id, title, price_old, price)
values (328, 631, 'Howard the Duck', '$3754.35', '$1044.37');
insert into books (id, author_id, title, price_old, price)
values (329, 393, 'Tango Lesson, The', '$4390.10', '$842.53');
insert into books (id, author_id, title, price_old, price)
values (330, 263, 'Zorn.s Lemma', '$2106.86', '$325.26');
insert into books (id, author_id, title, price_old, price)
values (331, 447, 'Portrait of Jennie', '$2852.96', '$1550.32');
insert into books (id, author_id, title, price_old, price)
values (332, 532, 'Suspicious River', '$2100.89', '$1860.22');
insert into books (id, author_id, title, price_old, price)
values (333, 994, 'One for the Money', '$2186.73', '$288.21');
insert into books (id, author_id, title, price_old, price)
values (334, 665, 'Death by China ', '$3270.35', '$678.16');
insert into books (id, author_id, title, price_old, price)
values (335, 363, 'Loners (Samotári)', '$4392.99', '$1652.70');
insert into books (id, author_id, title, price_old, price)
values (336, 382, 'Ploy', '$3550.71', '$1775.29');
insert into books (id, author_id, title, price_old, price)
values (337, 374, 'Four Men and a Prayer', '$3547.30', '$742.96');
insert into books (id, author_id, title, price_old, price)
values (338, 186, 'Great Escape, The', '$3752.22', '$442.45');
insert into books (id, author_id, title, price_old, price)
values (339, 995, 'American Psycho II: All American Girl', '$2844.05', '$439.92');
insert into books (id, author_id, title, price_old, price)
values (340, 236, 'Giant Mechanical Man, The', '$4483.82', '$1730.57');
insert into books (id, author_id, title, price_old, price)
values (341, 477, 'Today You Die', '$2562.68', '$1009.02');
insert into books (id, author_id, title, price_old, price)
values (342, 262, 'Seconds', '$4192.03', '$748.66');
insert into books (id, author_id, title, price_old, price)
values (343, 226, 'For Greater Glory: The True Story of Cristiada', '$2029.07', '$1081.01');
insert into books (id, author_id, title, price_old, price)
values (344, 854, 'Cutlet for Three (Ein Schnitzel für drei)', '$4240.40', '$344.18');
insert into books (id, author_id, title, price_old, price)
values (345, 169, 'Nicholas on Holiday', '$2619.11', '$1093.68');
insert into books (id, author_id, title, price_old, price)
values (346, 958, 'Lisztomania', '$2287.97', '$618.02');
insert into books (id, author_id, title, price_old, price)
values (347, 88, 'Joulupukki ja noitarumpu', '$4574.48', '$1985.43');
insert into books (id, author_id, title, price_old, price)
values (348, 855, 'Requiem for a Heavyweight', '$3616.84', '$1590.22');
insert into books (id, author_id, title, price_old, price)
values (349, 741, 'Fast & Furious (Fast and the Furious 4, The)', '$3913.11', '$787.08');
insert into books (id, author_id, title, price_old, price)
values (350, 383, 'Caught Up', '$2616.30', '$1366.32');
insert into books (id, author_id, title, price_old, price)
values (351, 800, 'Straightheads (Closure)', '$2924.26', '$1460.49');
insert into books (id, author_id, title, price_old, price)
values (352, 577, 'Mission to Moscow', '$2366.58', '$639.08');
insert into books (id, author_id, title, price_old, price)
values (353, 442, 'That Lady in Ermine', '$3167.63', '$265.65');
insert into books (id, author_id, title, price_old, price)
values (354, 131, 'Amnèsia', '$3967.45', '$250.57');
insert into books (id, author_id, title, price_old, price)
values (355, 974, 'Charlie Chan at the Race Track', '$2252.55', '$1479.64');
insert into books (id, author_id, title, price_old, price)
values (356, 838, 'The Photographer', '$4543.93', '$214.15');
insert into books (id, author_id, title, price_old, price)
values (357, 833, 'Shrink', '$3566.92', '$484.08');
insert into books (id, author_id, title, price_old, price)
values (358, 679, 'Beefcake', '$4683.80', '$664.34');
insert into books (id, author_id, title, price_old, price)
values (359, 103, 'Runaway Train', '$4377.93', '$799.07');
insert into books (id, author_id, title, price_old, price)
values (360, 254, 'Drunks', '$3695.87', '$474.60');
insert into books (id, author_id, title, price_old, price)
values (361, 203, 'Seven Sinners', '$3882.88', '$1439.14');
insert into books (id, author_id, title, price_old, price)
values (362, 92, 'Bio Zombie (Sun faa sau si)', '$3644.52', '$796.91');
insert into books (id, author_id, title, price_old, price)
values (363, 294, 'Knowing', '$2680.38', '$555.37');
insert into books (id, author_id, title, price_old, price)
values (364, 554, 'Inside Job', '$3112.98', '$1645.77');
insert into books (id, author_id, title, price_old, price)
values (365, 65, 'Freddy.s Dead: The Final Nightmare (Nightmare on Elm Street Part 6: Freddy.s Dead, A)', '$3293.29',
        '$516.51');
insert into books (id, author_id, title, price_old, price)
values (366, 24, 'Fat Albert', '$3054.67', '$552.05');
insert into books (id, author_id, title, price_old, price)
values (367, 444, 'Hellraiser: Hellseeker', '$3738.68', '$368.25');
insert into books (id, author_id, title, price_old, price)
values (368, 512, 'Silver Streak', '$2262.35', '$836.77');
insert into books (id, author_id, title, price_old, price)
values (369, 30, 'Viva Villa!', '$4217.03', '$316.06');
insert into books (id, author_id, title, price_old, price)
values (370, 866, 'Nobody.s Fool', '$4694.88', '$273.30');
insert into books (id, author_id, title, price_old, price)
values (371, 169, 'Mad Love (Juana la Loca)', '$4866.53', '$263.81');
insert into books (id, author_id, title, price_old, price)
values (372, 394, 'Intruder, The', '$4913.17', '$933.64');
insert into books (id, author_id, title, price_old, price)
values (373, 309, 'Sergio', '$2344.23', '$1610.46');
insert into books (id, author_id, title, price_old, price)
values (374, 202, 'Norma Rae', '$3774.58', '$1535.55');
insert into books (id, author_id, title, price_old, price)
values (375, 291, 'Pride and Glory', '$4057.22', '$1654.58');
insert into books (id, author_id, title, price_old, price)
values (376, 24, 'Desperate Search', '$3025.20', '$1959.71');
insert into books (id, author_id, title, price_old, price)
values (377, 599, 'Kumail Nanjiani: Beta Male ', '$3265.65', '$1470.39');
insert into books (id, author_id, title, price_old, price)
values (378, 526, 'Find Me Guilty', '$4472.41', '$1615.00');
insert into books (id, author_id, title, price_old, price)
values (379, 912, 'Cousin Bette', '$4591.58', '$697.07');
insert into books (id, author_id, title, price_old, price)
values (380, 800, 'Fun Size', '$2789.80', '$937.23');
insert into books (id, author_id, title, price_old, price)
values (381, 253, 'Blue Streak', '$3727.27', '$431.13');
insert into books (id, author_id, title, price_old, price)
values (382, 186, 'Yank in the R.A.F., A', '$3089.21', '$467.37');
insert into books (id, author_id, title, price_old, price)
values (383, 853, 'Wedding Singer, The', '$3190.65', '$661.34');
insert into books (id, author_id, title, price_old, price)
values (384, 693, 'Angel Baby', '$3438.74', '$732.96');
insert into books (id, author_id, title, price_old, price)
values (385, 405, 'Rebirth of Mothra II', '$3765.74', '$1696.63');
insert into books (id, author_id, title, price_old, price)
values (386, 902, 'Vanishing on 7th Street', '$4264.68', '$273.66');
insert into books (id, author_id, title, price_old, price)
values (387, 350, 'Land of the Lost', '$3616.03', '$1865.17');
insert into books (id, author_id, title, price_old, price)
values (388, 963, 'Barnens ö', '$4646.46', '$1578.11');
insert into books (id, author_id, title, price_old, price)
values (389, 449, 'Oh, God! Book II', '$4220.92', '$1367.51');
insert into books (id, author_id, title, price_old, price)
values (390, 409, 'Crows Zero (Kurôzu zero)', '$4035.50', '$965.79');
insert into books (id, author_id, title, price_old, price)
values (391, 152, 'Vampire Journals', '$2749.68', '$1551.61');
insert into books (id, author_id, title, price_old, price)
values (392, 82, 'Shark Skin Man and Peach Hip Girl (Samehada otoko to momojiri onna)', '$4401.37', '$1822.19');
insert into books (id, author_id, title, price_old, price)
values (393, 600, 'There Goes My Heart', '$3830.29', '$1589.92');
insert into books (id, author_id, title, price_old, price)
values (394, 188, 'Good Night to Die, A', '$4182.21', '$1991.78');
insert into books (id, author_id, title, price_old, price)
values (395, 498, 'Way Back, The', '$3089.87', '$1677.38');
insert into books (id, author_id, title, price_old, price)
values (396, 63, 'I Am Bruce Lee', '$4584.98', '$511.14');
insert into books (id, author_id, title, price_old, price)
values (397, 470, 'Two Can Play That Game', '$2721.04', '$569.07');
insert into books (id, author_id, title, price_old, price)
values (398, 308, 'Dallas: War of the Ewings', '$4224.56', '$644.56');
insert into books (id, author_id, title, price_old, price)
values (399, 942, '3some (Castillos de cartón)', '$4124.36', '$1289.58');
insert into books (id, author_id, title, price_old, price)
values (400, 700, 'Texas Across the River', '$3454.71', '$1871.30');
insert into books (id, author_id, title, price_old, price)
values (401, 922, 'August Evening', '$4598.20', '$1465.67');
insert into books (id, author_id, title, price_old, price)
values (402, 836, 'Merrily We Live', '$3103.73', '$510.42');
insert into books (id, author_id, title, price_old, price)
values (403, 808, 'Monsignor', '$3394.30', '$296.96');
insert into books (id, author_id, title, price_old, price)
values (404, 542, 'Rage in Heaven', '$3619.18', '$1039.53');
insert into books (id, author_id, title, price_old, price)
values (405, 422, 'Sylvia Scarlett', '$3840.71', '$922.43');
insert into books (id, author_id, title, price_old, price)
values (406, 910, 'How I Got Into College', '$3466.81', '$1614.40');
insert into books (id, author_id, title, price_old, price)
values (407, 441, 'Harem', '$3910.18', '$355.11');
insert into books (id, author_id, title, price_old, price)
values (408, 919, 'Don.t Go Breaking My Heart (Daan gyun naam yu)', '$3988.41', '$1982.31');
insert into books (id, author_id, title, price_old, price)
values (409, 330, 'Action in the North Atlantic', '$3910.20', '$1734.63');
insert into books (id, author_id, title, price_old, price)
values (410, 743, 'Jay-Z: Made in America', '$4998.15', '$680.30');
insert into books (id, author_id, title, price_old, price)
values (411, 420, 'Marriage Italian Style (Matrimonio all.italiana)', '$4690.40', '$1305.37');
insert into books (id, author_id, title, price_old, price)
values (412, 709, 'Dry Cleaning (Nettoyage à sec)', '$3497.86', '$1670.43');
insert into books (id, author_id, title, price_old, price)
values (413, 605, 'Let.s Not Get Angry (Ne nous fâchons pas)', '$2245.66', '$812.13');
insert into books (id, author_id, title, price_old, price)
values (414, 704, 'Devil Girl From Mars', '$3873.81', '$646.80');
insert into books (id, author_id, title, price_old, price)
values (415, 763, 'Rough Night in Jericho', '$3815.22', '$437.88');
insert into books (id, author_id, title, price_old, price)
values (416, 993, 'How They Get There', '$2974.42', '$665.20');
insert into books (id, author_id, title, price_old, price)
values (417, 522, 'Commitments, The', '$4634.81', '$1180.89');
insert into books (id, author_id, title, price_old, price)
values (418, 761, 'Legends of the Fall', '$3926.56', '$1841.24');
insert into books (id, author_id, title, price_old, price)
values (419, 456, 'Lost in the Desert', '$4433.15', '$1575.72');
insert into books (id, author_id, title, price_old, price)
values (420, 234, 'Krrish 3', '$2914.99', '$1735.30');
insert into books (id, author_id, title, price_old, price)
values (421, 392, 'Boys, The', '$2559.94', '$1900.10');
insert into books (id, author_id, title, price_old, price)
values (422, 864, 'Hamiltons, The', '$2681.25', '$1579.93');
insert into books (id, author_id, title, price_old, price)
values (423, 616, 'Mind Game', '$2377.31', '$1735.70');
insert into books (id, author_id, title, price_old, price)
values (424, 834, 'Exit', '$2979.29', '$1036.01');
insert into books (id, author_id, title, price_old, price)
values (425, 452, 'Survive Style 5+', '$2031.33', '$1614.04');
insert into books (id, author_id, title, price_old, price)
values (426, 919, 'Alias the Doctor', '$3976.55', '$1703.07');
insert into books (id, author_id, title, price_old, price)
values (427, 115, 'Horse.s Mouth, The', '$4066.05', '$1473.21');
insert into books (id, author_id, title, price_old, price)
values (428, 472, 'Swan and the Wanderer, The (Kulkuri ja joutsen)', '$4327.77', '$1333.73');
insert into books (id, author_id, title, price_old, price)
values (429, 604, 'Drawn Together Movie: The Movie!, The', '$3505.60', '$926.83');
insert into books (id, author_id, title, price_old, price)
values (430, 386, 'Sing a Song of Sex (Treatise on Japanese Bawdy Songs, A) (Nihon shunka-kô)', '$3728.42', '$406.34');
insert into books (id, author_id, title, price_old, price)
values (431, 596, 'Executive Suite', '$4957.89', '$1928.37');
insert into books (id, author_id, title, price_old, price)
values (432, 338, 'Bad Country', '$3041.61', '$1262.10');
insert into books (id, author_id, title, price_old, price)
values (433, 49, 'Renaissance Man', '$2399.38', '$1083.04');
insert into books (id, author_id, title, price_old, price)
values (434, 141, 'Myth of the American Sleepover, The', '$4886.99', '$1209.01');
insert into books (id, author_id, title, price_old, price)
values (435, 774, 'Bloodsport 2 (a.k.a. Bloodsport II: The Next Kumite)', '$3861.33', '$1062.98');
insert into books (id, author_id, title, price_old, price)
values (436, 14, 'Full Speed (À toute vitesse)', '$3975.29', '$857.47');
insert into books (id, author_id, title, price_old, price)
values (437, 762, 'Delusion', '$3107.96', '$445.34');
insert into books (id, author_id, title, price_old, price)
values (438, 725, 'Jeopardy (a.k.a. A Woman in Jeopardy)', '$4460.13', '$1687.63');
insert into books (id, author_id, title, price_old, price)
values (439, 189, 'Valley of the Dolls', '$3739.40', '$775.76');
insert into books (id, author_id, title, price_old, price)
values (440, 82, 'Stolen (Stolen Lives)', '$3296.07', '$723.14');
insert into books (id, author_id, title, price_old, price)
values (441, 424, '200 M.P.H.', '$2021.26', '$1063.68');
insert into books (id, author_id, title, price_old, price)
values (442, 252, 'Sideways', '$4671.97', '$1744.12');
insert into books (id, author_id, title, price_old, price)
values (443, 587, 'Fate (Yazgi)', '$2643.35', '$993.91');
insert into books (id, author_id, title, price_old, price)
values (444, 483, 'Zoom', '$3791.53', '$1285.98');
insert into books (id, author_id, title, price_old, price)
values (445, 257, 'People Under the Stairs, The', '$3818.88', '$1757.37');
insert into books (id, author_id, title, price_old, price)
values (446, 975, 'Straight to Hell', '$2721.03', '$740.87');
insert into books (id, author_id, title, price_old, price)
values (447, 270, 'R.I.P.D.', '$2683.74', '$1901.70');
insert into books (id, author_id, title, price_old, price)
values (448, 91, 'Tribe, The (Plemya)', '$2191.20', '$1800.56');
insert into books (id, author_id, title, price_old, price)
values (449, 960, 'Leave The World Behind', '$4041.16', '$263.92');
insert into books (id, author_id, title, price_old, price)
values (450, 574, 'Finding Nemo', '$3294.49', '$1719.39');
insert into books (id, author_id, title, price_old, price)
values (451, 646, 'Hard, Fast and Beautiful', '$3909.08', '$1371.08');
insert into books (id, author_id, title, price_old, price)
values (452, 120, 'Special Forces (Forces spéciales)', '$4878.93', '$783.22');
insert into books (id, author_id, title, price_old, price)
values (453, 405, 'Crowd Roars, The', '$3405.19', '$1268.32');
insert into books (id, author_id, title, price_old, price)
values (454, 787, 'All I Want (Try Seventeen)', '$3527.95', '$711.15');
insert into books (id, author_id, title, price_old, price)
values (455, 34, 'Bride of Chucky (Child.s Play 4)', '$4109.57', '$863.46');
insert into books (id, author_id, title, price_old, price)
values (456, 446, 'Girls in Prison', '$2989.05', '$1431.89');
insert into books (id, author_id, title, price_old, price)
values (457, 942, 'Fingers', '$4517.75', '$1949.36');
insert into books (id, author_id, title, price_old, price)
values (458, 279, 'Littlest Rebel, The', '$4213.13', '$584.54');
insert into books (id, author_id, title, price_old, price)
values (459, 570, 'Many Rivers to Cross', '$4281.75', '$739.86');
insert into books (id, author_id, title, price_old, price)
values (460, 965, 'Reconstruction', '$2028.95', '$1141.55');
insert into books (id, author_id, title, price_old, price)
values (461, 726, 'Falling Up', '$2296.66', '$1252.57');
insert into books (id, author_id, title, price_old, price)
values (462, 222, 'Art of Getting By, The', '$2705.00', '$1286.34');
insert into books (id, author_id, title, price_old, price)
values (463, 82, 'Kiss of the Dragon', '$3803.17', '$757.37');
insert into books (id, author_id, title, price_old, price)
values (464, 257, 'Night, The (Notte, La)', '$2356.63', '$1623.86');
insert into books (id, author_id, title, price_old, price)
values (465, 608, 'Men at Work', '$2322.41', '$1433.92');
insert into books (id, author_id, title, price_old, price)
values (466, 161, 'Gorillas in the Mist', '$2861.94', '$1313.87');
insert into books (id, author_id, title, price_old, price)
values (467, 220, 'Candy', '$2963.65', '$851.21');
insert into books (id, author_id, title, price_old, price)
values (468, 134, 'Avventura, L. (Adventure, The)', '$4899.38', '$349.65');
insert into books (id, author_id, title, price_old, price)
values (469, 100, 'Collateral', '$2277.09', '$454.68');
insert into books (id, author_id, title, price_old, price)
values (470, 646, 'Love That Boy', '$4379.00', '$1094.20');
insert into books (id, author_id, title, price_old, price)
values (471, 320, 'Nursery University', '$4095.18', '$1303.14');
insert into books (id, author_id, title, price_old, price)
values (472, 637, 'Megaforce', '$4113.86', '$1104.63');
insert into books (id, author_id, title, price_old, price)
values (473, 377, 'Flying Home', '$2603.60', '$532.89');
insert into books (id, author_id, title, price_old, price)
values (474, 748, 'Class of Nuke .Em High', '$4605.49', '$1836.70');
insert into books (id, author_id, title, price_old, price)
values (475, 106, 'Nancy Drew and the Hidden Staircase', '$4071.45', '$1537.03');
insert into books (id, author_id, title, price_old, price)
values (476, 123, 'Liberty Stands Still', '$4744.93', '$1088.16');
insert into books (id, author_id, title, price_old, price)
values (477, 873, 'Iris', '$2435.07', '$1388.72');
insert into books (id, author_id, title, price_old, price)
values (478, 507, 'Rendezvous', '$2603.31', '$755.34');
insert into books (id, author_id, title, price_old, price)
values (479, 758, 'American Nightmare, The', '$3318.58', '$661.32');
insert into books (id, author_id, title, price_old, price)
values (480, 190, 'Frankie and Alice', '$4140.82', '$1109.10');
insert into books (id, author_id, title, price_old, price)
values (481, 760, 'Crazy in Alabama', '$2061.16', '$1181.42');
insert into books (id, author_id, title, price_old, price)
values (482, 544, 'Think Like a Man Too', '$3391.57', '$1863.39');
insert into books (id, author_id, title, price_old, price)
values (483, 847, 'Blackbird, The', '$3640.51', '$315.61');
insert into books (id, author_id, title, price_old, price)
values (484, 22, 'Made in Hong Kong (Xiang Gang zhi zao)', '$3951.80', '$1533.42');
insert into books (id, author_id, title, price_old, price)
values (485, 278, 'Kevin Hart: Seriously Funny', '$3106.42', '$1856.86');
insert into books (id, author_id, title, price_old, price)
values (486, 330, 'Commander Hamilton (Hamilton)', '$2834.93', '$870.11');
insert into books (id, author_id, title, price_old, price)
values (487, 546, 'Always Leave Them Laughing', '$4002.29', '$1978.95');
insert into books (id, author_id, title, price_old, price)
values (488, 126, 'Designated Mourner, The', '$4754.41', '$1416.06');
insert into books (id, author_id, title, price_old, price)
values (489, 298, 'Woman of the Year', '$2827.30', '$428.00');
insert into books (id, author_id, title, price_old, price)
values (490, 54, 'Seventh Sign, The', '$4467.14', '$1328.60');
insert into books (id, author_id, title, price_old, price)
values (491, 798, 'Read It and Weep', '$2930.35', '$1162.45');
insert into books (id, author_id, title, price_old, price)
values (492, 969, 'Devil.s Backbone, The (Espinazo del diablo, El)', '$2562.59', '$977.69');
insert into books (id, author_id, title, price_old, price)
values (493, 851, 'The Mysterious Island', '$4518.86', '$522.87');
insert into books (id, author_id, title, price_old, price)
values (494, 550, 'Hope Floats', '$4907.95', '$1252.02');
insert into books (id, author_id, title, price_old, price)
values (495, 115, 'Nutcracker, The', '$2343.73', '$420.49');
insert into books (id, author_id, title, price_old, price)
values (496, 106, 'Remember Sunday', '$2862.22', '$1779.27');
insert into books (id, author_id, title, price_old, price)
values (497, 586, 'De nieuwe Wildernis', '$3754.93', '$1528.48');
insert into books (id, author_id, title, price_old, price)
values (498, 913, '976-EVIL', '$2175.18', '$627.24');
insert into books (id, author_id, title, price_old, price)
values (499, 648, 'Contact High', '$3526.46', '$1206.89');
insert into books (id, author_id, title, price_old, price)
values (500, 768, 'Bird of Paradise', '$3012.11', '$997.15');
