insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (1, 'Hail Caesar', true, '2020/03/13', 'book-lic-255', 'http://dummyimage.com/320x755.png/5fa2dd/ffffff',
        'Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.',
        361.21, 81, 667);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (2, 'Lady Takes a Chance, A', false, '2020/10/27', 'book-xid-439',
        'http://dummyimage.com/552x717.png/5fa2dd/ffffff',
        'Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet.',
        2183.90, 26, 889);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (3, '''Til There Was You', false, '2018/09/16', 'book-mtg-529',
        'http://dummyimage.com/741x730.png/ff4444/ffffff',
        'Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus.',
        148.64, 48, 817);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (4, 'Girl', false, '2019/07/02', 'book-ied-989', 'http://dummyimage.com/538x767.png/ff4444/ffffff',
        'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.',
        3413.51, 1, 398);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (5, 'Ghost, The', true, '2018/11/14', 'book-ztn-539', 'http://dummyimage.com/794x657.png/5fa2dd/ffffff',
        'Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.',
        2756.58, 96, 257);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (6, 'City of Men (Cidade dos Homens)', true, '2019/07/12', 'book-kco-736',
        'http://dummyimage.com/380x334.png/cc0000/ffffff',
        'Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
        197.89, 2, 524);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (7, 'Strayed (égarés, Les)', true, '2019/11/09', 'book-yfr-259',
        'http://dummyimage.com/773x597.png/ff4444/ffffff',
        'Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.',
        1888.97, 56, 566);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (8, 'Private Romeo', true, '2019/08/12', 'book-fpn-752', 'http://dummyimage.com/514x745.png/cc0000/ffffff',
        'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.',
        2752.13, 64, 398);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (9, 'The Amazing Spider-Man 2', true, '2018/12/21', 'book-sez-885',
        'http://dummyimage.com/800x654.png/5fa2dd/ffffff',
        'Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius.',
        3326.45, 39, 747);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (10, 'In This World', true, '2020/01/17', 'book-vbi-618', 'http://dummyimage.com/607x364.png/5fa2dd/ffffff',
        'Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis.',
        990.35, 6, 391);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (11, 'Misérables, Les', false, '2019/02/19', 'book-vvx-886', 'http://dummyimage.com/737x721.png/cc0000/ffffff',
        'Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.',
        2730.15, 8, 686);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (12, 'Matrix, The', false, '2019/05/26', 'book-wgf-862', 'http://dummyimage.com/676x286.png/cc0000/ffffff',
        'Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo.',
        1332.01, 70, 310);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (13, 'Undercover Man', false, '2020/05/22', 'book-syt-392', 'http://dummyimage.com/683x743.png/dddddd/000000',
        'Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor.',
        1222.90, 15, 643);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (14, 'Ulzana''s Raid', true, '2019/08/09', 'book-oof-865', 'http://dummyimage.com/363x683.png/cc0000/ffffff',
        'Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis.',
        3838.71, 53, 609);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (15, 'My Son (Mon fils à moi)', true, '2018/07/15', 'book-rjl-317',
        'http://dummyimage.com/638x575.png/dddddd/000000',
        'Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.',
        3798.55, 24, 815);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (16, 'Over the Brooklyn Bridge', false, '2019/03/07', 'book-nbm-162',
        'http://dummyimage.com/279x448.png/dddddd/000000',
        'Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.',
        1739.22, 89, 553);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (17, 'Cast Away', true, '2019/08/21', 'book-vuq-925', 'http://dummyimage.com/796x550.png/dddddd/000000',
        'Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.',
        4522.51, 39, 129);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (18, 'Mr. Pip', true, '2018/08/04', 'book-htf-490', 'http://dummyimage.com/484x751.png/cc0000/ffffff',
        'Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.',
        1100.42, 42, 136);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (19, 'Thing About My Folks, The', false, '2020/09/12', 'book-ebi-750',
        'http://dummyimage.com/587x494.png/5fa2dd/ffffff',
        'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.',
        885.01, 71, 370);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (20, 'Hard Way, The', false, '2020/05/17', 'book-mpy-764', 'http://dummyimage.com/795x288.png/ff4444/ffffff',
        'Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique.',
        3981.42, 32, 717);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (21, 'Peacekeeper, The', false, '2018/03/09', 'book-sya-548', 'http://dummyimage.com/709x661.png/5fa2dd/ffffff',
        'Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.',
        4269.63, 40, 122);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (22, 'Drawing Restraint 9', true, '2020/03/06', 'book-brt-561',
        'http://dummyimage.com/653x602.png/ff4444/ffffff',
        'Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus.',
        3524.94, 34, 726);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (23, 'Poor Little Rich Girl', true, '2019/09/17', 'book-njj-796',
        'http://dummyimage.com/563x676.png/cc0000/ffffff',
        'Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis.',
        2740.74, 76, 714);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (24, 'The Horseplayer', true, '2018/09/20', 'book-uus-923', 'http://dummyimage.com/709x703.png/dddddd/000000',
        'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.',
        1212.52, 46, 749);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (25, 'Bells of St. Mary''s, The', false, '2021/01/17', 'book-sny-050',
        'http://dummyimage.com/467x567.png/dddddd/000000',
        'Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus.',
        681.39, 88, 919);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (26, 'River, The (He liu)', true, '2020/12/09', 'book-ldo-543',
        'http://dummyimage.com/590x555.png/5fa2dd/ffffff',
        'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.',
        4500.35, 5, 512);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (27, 'Double, The', true, '2019/08/02', 'book-ymk-523', 'http://dummyimage.com/623x439.png/5fa2dd/ffffff',
        'Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante.',
        1213.90, 4, 11);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (28, 'Legally Blonde', true, '2019/05/18', 'book-tcq-586', 'http://dummyimage.com/707x322.png/5fa2dd/ffffff',
        'Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.',
        2035.77, 75, 75);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (29, 'Tremors II: Aftershocks', true, '2018/05/19', 'book-vys-483',
        'http://dummyimage.com/609x355.png/cc0000/ffffff',
        'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.',
        3173.78, 11, 480);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (30, 'Ball, The (Le bal)', false, '2018/04/21', 'book-rrv-057',
        'http://dummyimage.com/517x738.png/dddddd/000000',
        'Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti.',
        2352.23, 15, 947);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (31, 'Shadowlands', true, '2020/04/09', 'book-tzz-479', 'http://dummyimage.com/389x520.png/dddddd/000000',
        'Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl.',
        1854.21, 93, 850);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (32, 'Going Overboard', false, '2019/10/27', 'book-gyc-131', 'http://dummyimage.com/567x433.png/dddddd/000000',
        'Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus.',
        4284.85, 57, 253);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (33, 'Mind Game', true, '2019/06/21', 'book-eev-475', 'http://dummyimage.com/756x759.png/ff4444/ffffff',
        'Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est.',
        1771.26, 30, 582);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (34, 'Family Law (Derecho de familia)', true, '2019/11/27', 'book-cxy-862',
        'http://dummyimage.com/325x630.png/ff4444/ffffff',
        'Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor. Duis mattis egestas metus.',
        1103.69, 54, 453);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (35, 'Bella', true, '2019/07/27', 'book-ggs-751', 'http://dummyimage.com/387x595.png/5fa2dd/ffffff',
        'Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla.',
        2677.19, 25, 391);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (36, 'Alamo, The', true, '2019/07/20', 'book-ysy-735', 'http://dummyimage.com/294x670.png/5fa2dd/ffffff',
        'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue.',
        2981.39, 59, 97);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (37, 'Little Secrets', true, '2019/12/10', 'book-oia-531', 'http://dummyimage.com/638x304.png/ff4444/ffffff',
        'Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti.',
        4720.20, 59, 351);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (38, 'A Girl Walks Home Alone at Night', false, '2018/12/07', 'book-egi-976',
        'http://dummyimage.com/674x547.png/ff4444/ffffff',
        'In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.',
        1953.94, 0, 898);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (39, 'Cercle Rouge, Le (Red Circle, The)', true, '2018/06/13', 'book-pob-268',
        'http://dummyimage.com/737x383.png/5fa2dd/ffffff',
        'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo.',
        1457.62, 20, 889);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (40, 'Sleepwalkers', false, '2019/09/26', 'book-mkh-995', 'http://dummyimage.com/282x533.png/dddddd/000000',
        'Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.',
        4631.73, 82, 101);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (41, 'Test Pilot', false, '2018/03/07', 'book-fdh-255', 'http://dummyimage.com/412x501.png/dddddd/000000',
        'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst.',
        825.25, 7, 110);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (42, 'Change of Plans (Le code a changé)', false, '2018/04/04', 'book-huw-284',
        'http://dummyimage.com/500x548.png/dddddd/000000',
        'Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.',
        2245.39, 74, 126);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (43, 'Scenes from a Mall', true, '2018/03/11', 'book-rkk-426', 'http://dummyimage.com/697x334.png/ff4444/ffffff',
        'Praesent lectus. Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.',
        1241.09, 19, 648);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (44, 'Green Hornet, The', true, '2020/07/27', 'book-hoo-410', 'http://dummyimage.com/573x733.png/5fa2dd/ffffff',
        'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.',
        3372.04, 39, 405);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (45, 'Night of the Creeps', false, '2018/11/24', 'book-yln-431',
        'http://dummyimage.com/761x383.png/ff4444/ffffff',
        'Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis.',
        3221.99, 60, 782);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (46, 'Master, The', true, '2021/01/11', 'book-mjb-716', 'http://dummyimage.com/644x362.png/5fa2dd/ffffff',
        'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.',
        1341.04, 90, 314);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (47, 'Music Man, The', false, '2018/10/16', 'book-vto-012', 'http://dummyimage.com/300x647.png/5fa2dd/ffffff',
        'Proin risus. Praesent lectus. Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.',
        4042.69, 26, 579);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (48, 'Lake Placid', false, '2018/02/21', 'book-agj-272', 'http://dummyimage.com/429x310.png/dddddd/000000',
        'Proin risus. Praesent lectus. Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend.',
        2052.39, 55, 895);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (49, 'Butchered', true, '2019/01/20', 'book-dib-469', 'http://dummyimage.com/699x406.png/5fa2dd/ffffff',
        'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy.',
        484.47, 17, 125);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (50, 'Charm School (Niñas mal)', true, '2019/03/16', 'book-lde-624',
        'http://dummyimage.com/250x404.png/5fa2dd/ffffff',
        'Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.',
        4699.51, 96, 491);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (51, 'Blonde Venus', true, '2018/03/04', 'book-vpu-801', 'http://dummyimage.com/583x686.png/cc0000/ffffff',
        'Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst.',
        1234.80, 16, 657);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (52, 'Todos eran culpables', false, '2019/06/28', 'book-wzi-570',
        'http://dummyimage.com/478x484.png/cc0000/ffffff',
        'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.',
        1988.40, 14, 789);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (53, 'Heavy Metal Parking Lot', false, '2018/05/30', 'book-hqf-817',
        'http://dummyimage.com/372x263.png/ff4444/ffffff',
        'Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia.',
        3546.75, 95, 784);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (54, 'Frozen Fever', false, '2018/12/15', 'book-wco-313', 'http://dummyimage.com/669x706.png/ff4444/ffffff',
        'Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor.',
        682.39, 20, 659);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (55, 'Den enskilde medborgaren', false, '2019/11/12', 'book-krv-798',
        'http://dummyimage.com/442x252.png/dddddd/000000',
        'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.',
        510.21, 33, 920);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (56, 'Governess, The', false, '2019/04/05', 'book-coc-422', 'http://dummyimage.com/253x464.png/cc0000/ffffff',
        'Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti.',
        1763.91, 92, 201);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (57, 'Savage Streets', false, '2019/12/18', 'book-hqj-698', 'http://dummyimage.com/691x746.png/cc0000/ffffff',
        'Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis.',
        847.78, 97, 869);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (58, 'Gorillas in the Mist', false, '2018/12/16', 'book-xny-715',
        'http://dummyimage.com/790x264.png/dddddd/000000',
        'Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis. Donec semper sapien a libero.',
        1471.47, 95, 828);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (59, 'Greedy', true, '2020/10/09', 'book-qzf-706', 'http://dummyimage.com/337x609.png/dddddd/000000',
        'Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo.',
        1203.26, 82, 454);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (60, 'On the Other Side of the Tracks (De l''autre côté du périph)', false, '2019/05/31', 'book-zro-664',
        'http://dummyimage.com/469x330.png/cc0000/ffffff',
        'Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat.',
        2842.12, 49, 372);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (61, 'Master of the Flying Guillotine (Du bi quan wang da po xue di zi)', true, '2019/01/01', 'book-xyq-126',
        'http://dummyimage.com/573x363.png/ff4444/ffffff',
        'Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum.',
        2723.97, 99, 709);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (62, 'Night of the Running Man', true, '2020/10/09', 'book-oei-850',
        'http://dummyimage.com/434x506.png/cc0000/ffffff',
        'Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.',
        439.08, 54, 875);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (63, 'Badge, The', true, '2018/05/12', 'book-esq-936', 'http://dummyimage.com/439x308.png/dddddd/000000',
        'Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo.',
        1940.87, 21, 517);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (64, 'Nothing But Ghosts (Nichts als Gespenster)', true, '2018/12/30', 'book-zal-574',
        'http://dummyimage.com/550x637.png/dddddd/000000',
        'Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt.',
        1200.98, 46, 364);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (65, 'Szamanka', false, '2019/06/10', 'book-www-557', 'http://dummyimage.com/472x292.png/cc0000/ffffff',
        'Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices.',
        4580.28, 71, 887);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (66, 'Etoiles: Dancers of the Paris Opera Ballet (Tout Près des étoiles)', false, '2018/10/09', 'book-reb-725',
        'http://dummyimage.com/699x298.png/dddddd/000000',
        'In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi.',
        4306.00, 31, 990);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (67, 'Ivan''s Childhood (a.k.a. My Name is Ivan) (Ivanovo detstvo)', false, '2021/01/25', 'book-zly-548',
        'http://dummyimage.com/706x702.png/cc0000/ffffff',
        'Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.',
        1110.81, 71, 428);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (68, 'Brute Force', false, '2018/03/22', 'book-tzx-223', 'http://dummyimage.com/296x496.png/5fa2dd/ffffff',
        'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue.',
        3759.52, 44, 958);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (69, 'Month in the Country, A', true, '2018/11/03', 'book-tst-703',
        'http://dummyimage.com/278x597.png/ff4444/ffffff',
        'Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti.',
        4924.58, 36, 751);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (70, 'Guardian, The', false, '2018/05/08', 'book-agh-639', 'http://dummyimage.com/321x742.png/5fa2dd/ffffff',
        'Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue.',
        2824.95, 39, 53);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (71, 'Around the Bend', true, '2020/02/12', 'book-wyw-304', 'http://dummyimage.com/700x652.png/5fa2dd/ffffff',
        'Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis.',
        3290.59, 18, 795);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (72, 'Belly of the Beast', true, '2019/11/07', 'book-duc-531', 'http://dummyimage.com/561x571.png/ff4444/ffffff',
        'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus.',
        4256.92, 3, 429);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (73, 'Canyons, The', true, '2018/11/15', 'book-obq-692', 'http://dummyimage.com/636x289.png/cc0000/ffffff',
        'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis. Donec semper sapien a libero.',
        2430.45, 41, 240);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (74, 'Big White, The', true, '2020/09/26', 'book-atg-026', 'http://dummyimage.com/531x429.png/5fa2dd/ffffff',
        'Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis.',
        3099.91, 90, 489);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (75, 'Patsy', true, '2018/03/10', 'book-kdh-837', 'http://dummyimage.com/664x677.png/5fa2dd/ffffff',
        'Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis.',
        3672.67, 32, 912);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (76, 'Change of Plans (Le code a changé)', false, '2019/06/01', 'book-xpm-946',
        'http://dummyimage.com/633x304.png/ff4444/ffffff',
        'Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat.',
        438.44, 59, 108);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (77, 'Young Frankenstein', true, '2018/04/03', 'book-vrz-173', 'http://dummyimage.com/295x487.png/ff4444/ffffff',
        'Praesent lectus. Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend.',
        4201.35, 47, 754);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (78, 'Mail Early for Xmas', true, '2020/01/19', 'book-zed-082',
        'http://dummyimage.com/505x280.png/dddddd/000000',
        'Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis.',
        2152.34, 83, 252);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (79, 'Iron Giant, The', false, '2020/08/15', 'book-cah-163', 'http://dummyimage.com/322x555.png/5fa2dd/ffffff',
        'In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit.',
        2441.82, 53, 166);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (80, 'Kidnapped', false, '2019/08/19', 'book-pxd-967', 'http://dummyimage.com/770x283.png/cc0000/ffffff',
        'Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.',
        3629.90, 63, 929);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (81, 'Incredible Journey, The', true, '2019/05/15', 'book-ruy-459',
        'http://dummyimage.com/322x744.png/cc0000/ffffff',
        'Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo.',
        2287.78, 64, 796);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (82, 'Love at Large', false, '2020/09/09', 'book-msj-499', 'http://dummyimage.com/593x586.png/dddddd/000000',
        'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.',
        4540.17, 30, 121);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (83, 'Wedding Trough (Vase de noces)', false, '2019/03/21', 'book-cpc-985',
        'http://dummyimage.com/337x504.png/5fa2dd/ffffff',
        'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum.',
        1847.09, 70, 537);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (84, 'Omen, The', true, '2020/03/25', 'book-ttb-433', 'http://dummyimage.com/432x758.png/dddddd/000000',
        'Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst.',
        4394.58, 35, 465);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (85, 'Carefree', false, '2021/02/15', 'book-wzm-345', 'http://dummyimage.com/345x428.png/5fa2dd/ffffff',
        'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi.',
        1495.45, 38, 640);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (86, 'At the Circus', false, '2020/02/22', 'book-vgu-771', 'http://dummyimage.com/255x643.png/dddddd/000000',
        'Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim.',
        885.05, 43, 669);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (87, 'Little Nicholas (Le petit Nicolas)', true, '2019/03/18', 'book-oga-520',
        'http://dummyimage.com/308x290.png/5fa2dd/ffffff',
        'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc.',
        2406.94, 49, 666);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (88, 'Strangers When We Meet', true, '2019/07/02', 'book-ayc-894',
        'http://dummyimage.com/757x500.png/ff4444/ffffff',
        'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci.',
        2449.00, 24, 200);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (89, 'Fantomas vs. Scotland Yard', true, '2019/02/11', 'book-qxr-485',
        'http://dummyimage.com/717x759.png/5fa2dd/ffffff',
        'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl.',
        4333.00, 33, 116);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (90, 'Allotment Wives', false, '2020/03/07', 'book-eop-205', 'http://dummyimage.com/285x288.png/5fa2dd/ffffff',
        'Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl.',
        2802.35, 40, 831);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (91, 'Letter from an Unknown Woman', true, '2019/08/31', 'book-rhd-797',
        'http://dummyimage.com/295x712.png/cc0000/ffffff',
        'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy.',
        1731.49, 52, 94);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (92, 'Ghost and Mrs. Muir, The', true, '2020/03/16', 'book-iki-156',
        'http://dummyimage.com/292x737.png/5fa2dd/ffffff',
        'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.',
        1135.57, 99, 393);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (93, 'Moving Out', true, '2020/11/10', 'book-jqi-371', 'http://dummyimage.com/730x722.png/5fa2dd/ffffff',
        'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.',
        2505.65, 29, 506);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (94, 'Nativity 2: Danger in the Manger!', false, '2020/06/23', 'book-xxv-575',
        'http://dummyimage.com/325x726.png/cc0000/ffffff',
        'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante.',
        2279.45, 80, 783);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (95, 'Tribulation', true, '2019/08/17', 'book-cly-413', 'http://dummyimage.com/300x685.png/5fa2dd/ffffff',
        'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.',
        3334.14, 18, 1);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (96, 'Blue Chips', true, '2018/05/19', 'book-tbj-487', 'http://dummyimage.com/488x487.png/5fa2dd/ffffff',
        'Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.',
        2871.91, 97, 66);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (97, 'My Blue Heaven', true, '2020/10/18', 'book-afr-318', 'http://dummyimage.com/311x576.png/dddddd/000000',
        'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.',
        3077.68, 65, 394);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (98, 'Love in Another Language (Baska Dilde Ask)', true, '2020/10/27', 'book-ctu-522',
        'http://dummyimage.com/380x336.png/cc0000/ffffff',
        'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.',
        4277.67, 56, 734);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (99, '7 Faces of Dr. Lao', false, '2019/07/18', 'book-ymn-819',
        'http://dummyimage.com/556x455.png/5fa2dd/ffffff',
        'Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus.',
        3351.31, 59, 547);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (100, 'Scream 4', true, '2019/10/05', 'book-lyl-055', 'http://dummyimage.com/251x498.png/dddddd/000000',
        'Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla.',
        1986.34, 4, 372);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (101, 'Colonel Chabert, Le', true, '2018/10/27', 'book-sto-934',
        'http://dummyimage.com/342x642.png/ff4444/ffffff',
        'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus.',
        1726.73, 56, 497);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (102, 'Joyriders, The', false, '2019/03/21', 'book-zpf-851', 'http://dummyimage.com/428x724.png/dddddd/000000',
        'Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.',
        685.03, 66, 547);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (103, 'Wieners', false, '2018/04/17', 'book-ylu-676', 'http://dummyimage.com/709x528.png/cc0000/ffffff',
        'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.',
        2982.41, 1, 562);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (104, 'House of the Spirits, The', true, '2018/10/03', 'book-aqp-740',
        'http://dummyimage.com/458x456.png/ff4444/ffffff',
        'Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque.',
        332.94, 44, 922);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (105, 'They Call Him Bulldozer (Lo chiamavano Bulldozer)', false, '2020/12/02', 'book-bli-936',
        'http://dummyimage.com/646x662.png/cc0000/ffffff',
        'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus.',
        1022.53, 96, 549);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (106, 'Fire with Fire', true, '2019/07/22', 'book-vew-505', 'http://dummyimage.com/281x257.png/5fa2dd/ffffff',
        'Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique.',
        670.15, 75, 551);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (107, 'Collector, The (Komornik)', true, '2020/09/26', 'book-epx-369',
        'http://dummyimage.com/278x320.png/dddddd/000000',
        'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque.',
        1209.69, 30, 127);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (108, 'Leviathan', true, '2018/11/12', 'book-pbz-511', 'http://dummyimage.com/368x796.png/dddddd/000000',
        'Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy.',
        3485.06, 26, 369);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (109, 'Cirque du Soleil: Dralion', false, '2020/05/15', 'book-fww-002',
        'http://dummyimage.com/668x720.png/ff4444/ffffff',
        'Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci.',
        1632.87, 66, 28);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (110, 'Night Train to Munich', false, '2019/05/11', 'book-ghq-139',
        'http://dummyimage.com/790x766.png/cc0000/ffffff',
        'Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique.',
        3537.49, 80, 959);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (111, 'All Blossoms Again: Pedro Costa, Director (Tout refleurit: Pedro Costa, cinéaste)', false, '2019/10/30',
        'book-gxl-522', 'http://dummyimage.com/402x327.png/cc0000/ffffff',
        'Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique.',
        3631.43, 54, 557);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (112, 'Love, etc.', true, '2020/09/11', 'book-vos-773', 'http://dummyimage.com/449x507.png/ff4444/ffffff',
        'In congue. Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.',
        3840.28, 30, 869);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (113, 'Blood Relatives (Liens de sang, Les)', true, '2020/05/11', 'book-btr-893',
        'http://dummyimage.com/711x691.png/dddddd/000000',
        'In congue. Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.',
        4536.12, 59, 95);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (114, 'Winter Sleep (Kis Uykusu)', false, '2020/08/01', 'book-ule-536',
        'http://dummyimage.com/292x460.png/5fa2dd/ffffff',
        'Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.',
        3060.47, 68, 598);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (115, 'Starcrash (a.k.a. Star Crash)', true, '2020/02/06', 'book-qps-563',
        'http://dummyimage.com/595x345.png/5fa2dd/ffffff',
        'Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum.',
        282.81, 86, 125);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (116, 'No One Writes to the Colonel (El coronel no tiene quien le escriba)', false, '2018/09/20', 'book-bru-343',
        'http://dummyimage.com/280x260.png/cc0000/ffffff',
        'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.',
        1172.58, 18, 331);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (117, 'Associate, The (Associé, L'')', true, '2018/05/21', 'book-zeu-619',
        'http://dummyimage.com/592x317.png/cc0000/ffffff',
        'Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis.',
        4339.81, 21, 431);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (118, 'Hatchet III', false, '2019/03/25', 'book-eze-030', 'http://dummyimage.com/584x724.png/dddddd/000000',
        'Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius.',
        2274.70, 45, 411);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (119, 'Metro', false, '2019/07/31', 'book-vla-753', 'http://dummyimage.com/754x604.png/dddddd/000000',
        'Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.',
        4730.61, 41, 131);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (120, 'Svengali', true, '2018/11/11', 'book-qcy-202', 'http://dummyimage.com/415x666.png/dddddd/000000',
        'Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.',
        919.59, 25, 882);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (121, 'Empire', true, '2019/02/09', 'book-ilr-846', 'http://dummyimage.com/367x529.png/dddddd/000000',
        'Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est.',
        1547.25, 44, 831);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (122, '33 Scenes from Life (33 sceny z zycia)', true, '2019/06/24', 'book-bpm-921',
        'http://dummyimage.com/495x386.png/dddddd/000000',
        'In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit.',
        2811.80, 3, 409);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (123, 'Bill Cunningham New York', true, '2021/01/24', 'book-uzx-688',
        'http://dummyimage.com/401x759.png/cc0000/ffffff',
        'Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue.',
        3355.81, 25, 644);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (124, 'Daybreakers', false, '2019/12/16', 'book-iwd-239', 'http://dummyimage.com/625x682.png/cc0000/ffffff',
        'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus.',
        3151.38, 46, 681);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (125, 'Mike''s New Car', false, '2019/12/23', 'book-eoy-745', 'http://dummyimage.com/670x668.png/ff4444/ffffff',
        'Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam. Nam tristique tortor eu pede.',
        1872.93, 48, 139);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (126, 'Footlight Parade', false, '2020/02/23', 'book-ggo-404', 'http://dummyimage.com/627x800.png/cc0000/ffffff',
        'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.',
        2922.18, 83, 675);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (127, 'Behind the Mask: The Rise of Leslie Vernon', true, '2019/08/13', 'book-dtq-315',
        'http://dummyimage.com/334x704.png/5fa2dd/ffffff',
        'Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.',
        4283.56, 2, 317);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (128, 'Robocroc', false, '2019/08/03', 'book-mbr-594', 'http://dummyimage.com/525x738.png/ff4444/ffffff',
        'Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.',
        2657.80, 10, 167);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (129, 'Trouble with the Curve', false, '2018/12/10', 'book-ugr-175',
        'http://dummyimage.com/569x425.png/5fa2dd/ffffff',
        'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.',
        1182.74, 46, 846);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (130, 'Greaser''s Palace', false, '2018/11/08', 'book-tei-515',
        'http://dummyimage.com/427x337.png/5fa2dd/ffffff',
        'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.',
        1759.29, 41, 821);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (131, 'Dersu Uzala', true, '2020/06/28', 'book-ehp-866', 'http://dummyimage.com/449x414.png/ff4444/ffffff',
        'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl.',
        628.09, 92, 270);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (132, 'Friends with Benefits', false, '2020/08/03', 'book-smf-407',
        'http://dummyimage.com/323x339.png/dddddd/000000',
        'Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.',
        1775.10, 77, 67);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (133, 'Loaf and Camouflage', true, '2020/08/15', 'book-qax-808',
        'http://dummyimage.com/280x461.png/dddddd/000000',
        'Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti.',
        2207.96, 30, 579);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (134, 'Last Resort', false, '2018/05/07', 'book-kpg-151', 'http://dummyimage.com/549x472.png/ff4444/ffffff',
        'In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit.',
        373.82, 14, 170);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (135, 'Day After, The', true, '2019/04/12', 'book-nmm-153', 'http://dummyimage.com/725x593.png/ff4444/ffffff',
        'Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum.',
        504.98, 85, 710);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (136, 'Brick Lane', true, '2020/01/06', 'book-wff-426', 'http://dummyimage.com/329x361.png/5fa2dd/ffffff',
        'Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.',
        1994.49, 45, 353);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (137, 'John Adams', false, '2018/04/30', 'book-zrw-082', 'http://dummyimage.com/320x326.png/dddddd/000000',
        'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue.',
        1583.06, 72, 621);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (138, 'The Broken Jug', true, '2018/04/20', 'book-cex-546', 'http://dummyimage.com/434x453.png/cc0000/ffffff',
        'Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst.',
        4751.32, 90, 353);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (139, 'Son''s Room, The (Stanza del figlio, La)', false, '2019/02/10', 'book-fhh-131',
        'http://dummyimage.com/319x747.png/5fa2dd/ffffff',
        'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia.',
        4266.57, 70, 578);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (140, 'The Iron Commissioner', false, '2019/01/02', 'book-fxv-716',
        'http://dummyimage.com/488x267.png/5fa2dd/ffffff',
        'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.',
        4890.34, 9, 990);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (141, 'Life Apart: Hasidism in America, A', false, '2020/01/29', 'book-zkd-474',
        'http://dummyimage.com/314x457.png/dddddd/000000',
        'Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.',
        2316.62, 96, 464);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (142, 'Rosencrantz and Guildenstern Are Dead', true, '2018/09/21', 'book-dia-752',
        'http://dummyimage.com/332x413.png/ff4444/ffffff',
        'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus.',
        1173.00, 9, 823);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (143, 'Boogie Man: The Lee Atwater Story', false, '2019/11/07', 'book-urn-971',
        'http://dummyimage.com/528x275.png/ff4444/ffffff',
        'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.',
        3501.22, 97, 489);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (144, 'Ballet Shoes', true, '2020/02/11', 'book-apf-616', 'http://dummyimage.com/284x690.png/cc0000/ffffff',
        'Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo.',
        1913.97, 30, 22);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (145, 'Nomad (Köshpendiler)', true, '2020/03/08', 'book-exd-709',
        'http://dummyimage.com/379x296.png/cc0000/ffffff',
        'Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.',
        4201.22, 95, 673);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (146, 'God told Me To', true, '2019/05/02', 'book-knc-381', 'http://dummyimage.com/459x631.png/dddddd/000000',
        'Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo.',
        282.73, 55, 163);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (147, 'I Like Killing Flies', true, '2018/03/10', 'book-aqh-014',
        'http://dummyimage.com/696x554.png/cc0000/ffffff',
        'Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo.',
        2950.71, 80, 427);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (148, 'True Grit', true, '2019/12/10', 'book-zfd-123', 'http://dummyimage.com/789x769.png/cc0000/ffffff',
        'Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat.',
        2793.08, 42, 854);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (149, 'Bikes vs Cars', false, '2019/08/21', 'book-znb-610', 'http://dummyimage.com/308x642.png/dddddd/000000',
        'Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus.',
        1391.96, 12, 541);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (150, 'Catfish', false, '2019/03/30', 'book-txo-435', 'http://dummyimage.com/635x295.png/5fa2dd/ffffff',
        'Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh.',
        4533.83, 50, 842);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (151, 'Väreitä', true, '2019/05/04', 'book-mpk-397', 'http://dummyimage.com/351x699.png/5fa2dd/ffffff',
        'Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.',
        2799.33, 62, 524);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (152, 'Frankie and Alice', false, '2018/03/31', 'book-igg-855',
        'http://dummyimage.com/649x436.png/cc0000/ffffff',
        'Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.',
        770.58, 95, 151);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (153, 'Evil That Men Do, The', true, '2020/05/20', 'book-ahw-160',
        'http://dummyimage.com/458x400.png/ff4444/ffffff',
        'Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.',
        1639.29, 90, 430);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (154, 'Glimmer Man, The', false, '2018/06/04', 'book-bgj-450', 'http://dummyimage.com/503x287.png/cc0000/ffffff',
        'Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices.',
        4902.70, 16, 985);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (155, 'Prey for Rock & Roll', true, '2018/05/21', 'book-mnx-757',
        'http://dummyimage.com/523x530.png/ff4444/ffffff',
        'Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat.',
        2167.14, 92, 251);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (156, 'Men Who Stare at Goats, The', true, '2020/10/10', 'book-ytz-218',
        'http://dummyimage.com/288x322.png/cc0000/ffffff',
        'Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis.',
        1191.40, 38, 408);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (157, 'Arthur', false, '2020/09/16', 'book-ybv-754', 'http://dummyimage.com/638x527.png/5fa2dd/ffffff',
        'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus.',
        4614.43, 23, 270);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (158, 'Returner (Ritaanaa)', false, '2020/05/03', 'book-awk-537',
        'http://dummyimage.com/375x348.png/dddddd/000000',
        'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.',
        2863.56, 42, 222);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (159, 'Raven, The', true, '2019/09/16', 'book-noj-892', 'http://dummyimage.com/770x766.png/5fa2dd/ffffff',
        'Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices.',
        3185.60, 33, 918);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (160, 'Carrington', false, '2019/07/09', 'book-xvs-895', 'http://dummyimage.com/768x546.png/ff4444/ffffff',
        'Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.',
        1879.55, 3, 852);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (161, 'Sword of the Beast (Kedamono no ken)', false, '2019/05/17', 'book-tco-929',
        'http://dummyimage.com/299x326.png/ff4444/ffffff',
        'Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.',
        2277.68, 21, 250);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (162, 'Shaft''s Big Score!', true, '2018/07/19', 'book-pxf-423',
        'http://dummyimage.com/625x659.png/dddddd/000000',
        'Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque.',
        3997.45, 91, 879);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (163, 'British Intelligence', false, '2021/02/05', 'book-pqq-727',
        'http://dummyimage.com/683x311.png/cc0000/ffffff',
        'Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue.',
        1087.89, 69, 347);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (164, 'Madeline', false, '2019/08/30', 'book-mpx-464', 'http://dummyimage.com/745x363.png/cc0000/ffffff',
        'Proin risus. Praesent lectus. Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend.',
        3149.39, 4, 739);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (165, 'Suddenly, Last Summer', true, '2018/09/24', 'book-uiz-232',
        'http://dummyimage.com/383x741.png/5fa2dd/ffffff',
        'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.',
        661.61, 84, 396);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (166, 'Carnosaur 2', false, '2019/04/27', 'book-yzx-138', 'http://dummyimage.com/642x669.png/dddddd/000000',
        'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.',
        4354.53, 64, 260);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (167, 'Swedish Auto', false, '2019/06/21', 'book-dog-422', 'http://dummyimage.com/772x383.png/ff4444/ffffff',
        'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.',
        3044.01, 83, 87);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (168, 'Win a Date with Tad Hamilton!', true, '2020/07/13', 'book-hql-764',
        'http://dummyimage.com/740x556.png/5fa2dd/ffffff',
        'Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus.',
        2676.02, 20, 531);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (169, 'Tobor the Great', true, '2018/09/06', 'book-lte-106', 'http://dummyimage.com/529x791.png/ff4444/ffffff',
        'Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis.',
        1779.16, 48, 814);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (170, 'Anazapta (Black Plague)', false, '2019/08/06', 'book-rde-049',
        'http://dummyimage.com/574x478.png/5fa2dd/ffffff',
        'Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.',
        914.33, 2, 55);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (171, 'The Elephant Man', true, '2020/07/14', 'book-xjf-724', 'http://dummyimage.com/763x548.png/ff4444/ffffff',
        'Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo.',
        3106.00, 45, 787);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (172, 'Boiling Point (3-4 x jûgatsu)', false, '2020/02/26', 'book-xuq-438',
        'http://dummyimage.com/382x292.png/dddddd/000000',
        'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.',
        310.21, 12, 100);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (173, 'Teenage Mutant Ninja Turtles III', true, '2020/01/22', 'book-tdt-877',
        'http://dummyimage.com/740x544.png/dddddd/000000',
        'Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.',
        4601.46, 49, 287);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (174, 'Pearls of the Crown, The (Les perles de la couronne)', false, '2018/11/07', 'book-daj-017',
        'http://dummyimage.com/259x464.png/ff4444/ffffff',
        'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.',
        3886.32, 34, 196);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (175, 'Keeping Up with the Steins', false, '2018/11/24', 'book-eep-477',
        'http://dummyimage.com/750x538.png/cc0000/ffffff',
        'Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue.',
        2390.48, 64, 232);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (176, 'Twilight of the Golds, The', true, '2019/01/18', 'book-gde-548',
        'http://dummyimage.com/789x606.png/ff4444/ffffff',
        'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus.',
        2662.90, 86, 268);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (177, 'Ballad of Nessie, The', true, '2018/11/06', 'book-fon-198',
        'http://dummyimage.com/476x518.png/ff4444/ffffff',
        'Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis.',
        1114.80, 81, 199);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (178, 'Hatchet III', true, '2019/07/12', 'book-jyj-331', 'http://dummyimage.com/785x768.png/5fa2dd/ffffff',
        'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
        715.59, 83, 282);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (179, 'Men with Brooms', true, '2020/10/25', 'book-tpy-817', 'http://dummyimage.com/393x373.png/5fa2dd/ffffff',
        'Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam.',
        2107.16, 98, 379);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (180, 'Edie & Pen', false, '2020/12/12', 'book-daa-980', 'http://dummyimage.com/579x516.png/cc0000/ffffff',
        'Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius.',
        4864.62, 94, 801);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (181, 'Holy Motors', true, '2020/11/26', 'book-crg-935', 'http://dummyimage.com/350x690.png/5fa2dd/ffffff',
        'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus.',
        1611.23, 1, 259);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (182, 'Bandits', false, '2019/02/13', 'book-yex-648', 'http://dummyimage.com/539x755.png/cc0000/ffffff',
        'Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.',
        1029.57, 25, 760);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (183, 'Ass Backwards', false, '2019/01/20', 'book-yxb-499', 'http://dummyimage.com/571x568.png/ff4444/ffffff',
        'Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus.',
        4995.88, 29, 287);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (184, 'Scent of Green Papaya, The (Mùi du du xhan - L''odeur de la papaye verte)', true, '2019/12/21',
        'book-wix-418', 'http://dummyimage.com/323x741.png/dddddd/000000',
        'Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst.',
        2658.82, 95, 844);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (185, 'Earth Dies Screaming, The', false, '2019/10/16', 'book-eio-253',
        'http://dummyimage.com/368x471.png/5fa2dd/ffffff',
        'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.',
        4036.50, 86, 370);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (186, 'Imposter, The', false, '2018/06/04', 'book-lqc-428', 'http://dummyimage.com/478x674.png/5fa2dd/ffffff',
        'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem.',
        3103.02, 67, 195);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (187, 'I Love You, I Love You (Je t''aime je t''aime)', true, '2020/07/26', 'book-yhx-663',
        'http://dummyimage.com/623x645.png/dddddd/000000',
        'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor.',
        4619.50, 64, 997);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (188, 'Story of Maths, The', false, '2020/10/24', 'book-ari-114',
        'http://dummyimage.com/722x589.png/cc0000/ffffff',
        'Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio.',
        2787.88, 10, 512);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (189, 'Swan Princess, The', false, '2018/03/04', 'book-egf-825',
        'http://dummyimage.com/417x624.png/cc0000/ffffff',
        'Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.',
        1690.50, 95, 540);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (190, 'Solid Gold Cadillac, The', true, '2019/11/13', 'book-iiu-580',
        'http://dummyimage.com/655x460.png/5fa2dd/ffffff',
        'Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante.',
        2703.39, 85, 124);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (191, 'Celebrity', false, '2019/07/16', 'book-jbu-914', 'http://dummyimage.com/560x719.png/ff4444/ffffff',
        'Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros.',
        4051.97, 82, 66);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (192, 'Broken Vessels', true, '2020/09/29', 'book-rnz-768', 'http://dummyimage.com/704x373.png/5fa2dd/ffffff',
        'Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo.',
        1350.37, 9, 518);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (193, 'Cocaine Cowboys', false, '2021/02/07', 'book-xue-273', 'http://dummyimage.com/783x676.png/cc0000/ffffff',
        'Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum.',
        2279.83, 18, 430);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (194, 'Gang Tapes', false, '2019/10/24', 'book-ysz-352', 'http://dummyimage.com/741x704.png/cc0000/ffffff',
        'Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum.',
        4237.90, 23, 577);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (195, 'Nobody''s Fool', false, '2020/06/13', 'book-ztz-791', 'http://dummyimage.com/564x590.png/ff4444/ffffff',
        'Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum.',
        4390.96, 91, 339);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (196, 'Night of the Living Dead', true, '2019/04/08', 'book-dxj-816',
        'http://dummyimage.com/431x648.png/cc0000/ffffff',
        'Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis.',
        4621.43, 17, 227);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (197, 'Watermark', false, '2021/02/15', 'book-tjb-090', 'http://dummyimage.com/751x420.png/cc0000/ffffff',
        'Fusce consequat. Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus.',
        564.43, 35, 684);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (198, 'Blood on Satan''s Claw (a.k.a. Satan''s Skin)', false, '2020/05/16', 'book-fcn-567',
        'http://dummyimage.com/612x512.png/5fa2dd/ffffff',
        'Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci.',
        3348.41, 71, 975);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (199, 'Galaxy Quest', false, '2019/12/08', 'book-jog-781', 'http://dummyimage.com/346x565.png/ff4444/ffffff',
        'Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.',
        4217.68, 67, 887);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (200, 'Attraction', true, '2019/10/26', 'book-bro-216', 'http://dummyimage.com/513x480.png/cc0000/ffffff',
        'Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus.',
        4564.81, 31, 405);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (201, 'Mars Attacks!', false, '2020/11/22', 'book-igy-793', 'http://dummyimage.com/542x766.png/dddddd/000000',
        'Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est.',
        4340.65, 91, 546);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (202, 'Blonde Crazy', false, '2019/02/09', 'book-lou-437', 'http://dummyimage.com/553x768.png/5fa2dd/ffffff',
        'Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue.',
        3124.84, 27, 961);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (203, 'Last of the Red Hot Lovers', false, '2021/02/14', 'book-zsa-964',
        'http://dummyimage.com/491x622.png/5fa2dd/ffffff',
        'Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit.',
        1471.56, 16, 873);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (204, 'Crouching Tiger, Hidden Dragon (Wo hu cang long)', true, '2020/04/12', 'book-xvj-735',
        'http://dummyimage.com/554x391.png/dddddd/000000',
        'Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus.',
        2849.55, 93, 12);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (205, 'Our Modern Maidens', true, '2020/03/21', 'book-wtn-214',
        'http://dummyimage.com/427x297.png/cc0000/ffffff',
        'Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.',
        4904.93, 31, 759);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (206, 'Poltergeist II: The Other Side', true, '2018/11/30', 'book-xmm-348',
        'http://dummyimage.com/573x549.png/ff4444/ffffff',
        'Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.',
        617.98, 37, 550);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (207, 'American Raspberry (Prime Time) (Funny America)', true, '2018/12/18', 'book-hfs-001',
        'http://dummyimage.com/682x444.png/ff4444/ffffff',
        'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.',
        4404.86, 42, 872);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (208, 'See This Movie', false, '2018/04/20', 'book-oku-658', 'http://dummyimage.com/557x796.png/dddddd/000000',
        'In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa.',
        3875.40, 46, 651);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (209, 'Taxidermia', true, '2020/11/14', 'book-xbr-651', 'http://dummyimage.com/503x689.png/dddddd/000000',
        'Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.',
        3829.37, 60, 953);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (210, 'Naked', true, '2019/02/04', 'book-odc-926', 'http://dummyimage.com/293x250.png/dddddd/000000',
        'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti.',
        3823.44, 33, 862);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (211, 'New Daughter, The', true, '2018/06/12', 'book-ocy-255', 'http://dummyimage.com/375x711.png/5fa2dd/ffffff',
        'Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.',
        1173.34, 57, 914);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (212, 'Rules of Attraction, The', true, '2019/01/13', 'book-pdy-989',
        'http://dummyimage.com/567x349.png/dddddd/000000',
        'Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue.',
        1940.62, 1, 571);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (213, 'Go for It', false, '2018/08/26', 'book-qjz-195', 'http://dummyimage.com/655x684.png/5fa2dd/ffffff',
        'Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt.',
        2478.59, 53, 168);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (214, 'Miss Météo', false, '2019/10/05', 'book-cgy-027', 'http://dummyimage.com/644x398.png/cc0000/ffffff',
        'Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet.',
        606.16, 57, 556);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (215, 'Pride of the Marines', false, '2019/11/22', 'book-uzf-447',
        'http://dummyimage.com/751x324.png/ff4444/ffffff',
        'Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim.',
        4370.44, 55, 863);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (216, 'Kiss Me, Guido', false, '2019/04/03', 'book-xyw-251', 'http://dummyimage.com/736x450.png/5fa2dd/ffffff',
        'Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus.',
        1858.16, 71, 518);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (217, 'I''ll Sleep When I''m Dead', true, '2018/07/19', 'book-uik-590',
        'http://dummyimage.com/694x442.png/dddddd/000000',
        'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla.',
        210.88, 63, 441);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (218, 'I Married a Witch', true, '2018/09/29', 'book-fkz-159', 'http://dummyimage.com/720x379.png/dddddd/000000',
        'Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante.',
        4860.40, 53, 855);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (219, 'Front of the Class', true, '2018/08/14', 'book-zgv-527',
        'http://dummyimage.com/513x497.png/cc0000/ffffff',
        'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.',
        141.44, 96, 337);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (220, 'Brothers in Trouble', true, '2019/05/02', 'book-dtf-111',
        'http://dummyimage.com/647x414.png/dddddd/000000',
        'Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.',
        2785.99, 89, 430);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (221, 'Town, The', false, '2020/06/18', 'book-myk-320', 'http://dummyimage.com/689x475.png/dddddd/000000',
        'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.',
        3501.69, 0, 264);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (222, 'Cloud Atlas', true, '2019/01/16', 'book-bbb-526', 'http://dummyimage.com/502x446.png/dddddd/000000',
        'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia.',
        2986.86, 73, 807);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (223, 'Peepli Live', false, '2020/06/15', 'book-pum-048', 'http://dummyimage.com/366x464.png/5fa2dd/ffffff',
        'Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula.',
        4457.11, 32, 299);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (224, 'Education, An', true, '2019/05/20', 'book-gdo-350', 'http://dummyimage.com/380x407.png/cc0000/ffffff',
        'Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim.',
        1667.16, 98, 655);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (225, 'How to Stop Being a Loser', false, '2020/11/26', 'book-wvd-115',
        'http://dummyimage.com/780x315.png/cc0000/ffffff',
        'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo.',
        2989.84, 50, 193);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (226, 'Seven Girlfriends', true, '2018/04/14', 'book-lbh-000', 'http://dummyimage.com/440x423.png/5fa2dd/ffffff',
        'Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum.',
        671.09, 97, 964);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (227, 'Shoppen ', false, '2018/06/16', 'book-pxe-012', 'http://dummyimage.com/380x354.png/ff4444/ffffff',
        'Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.',
        3392.19, 36, 127);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (228, 'Wonderland', false, '2018/11/25', 'book-ifv-094', 'http://dummyimage.com/610x479.png/ff4444/ffffff',
        'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum.',
        4665.60, 56, 416);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (229, 'I Am', false, '2020/05/30', 'book-rmh-272', 'http://dummyimage.com/530x407.png/dddddd/000000',
        'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.',
        2523.35, 67, 325);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (230, '24th Day, The', true, '2019/10/28', 'book-ues-423', 'http://dummyimage.com/542x772.png/dddddd/000000',
        'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.',
        975.27, 66, 280);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (231, 'Written on the Wind', false, '2018/08/30', 'book-tvd-363',
        'http://dummyimage.com/328x419.png/dddddd/000000',
        'Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est.',
        1483.74, 5, 755);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (232, 'The Challenge', true, '2020/03/25', 'book-gtb-896', 'http://dummyimage.com/433x448.png/5fa2dd/ffffff',
        'In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue.',
        3874.55, 56, 251);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (233, 'Formosa Betrayed', true, '2018/05/20', 'book-zqs-082', 'http://dummyimage.com/522x790.png/5fa2dd/ffffff',
        'Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.',
        2603.15, 53, 141);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (234, 'Broderskab (Brotherhood)', true, '2019/01/15', 'book-yzj-754',
        'http://dummyimage.com/425x481.png/5fa2dd/ffffff',
        'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.',
        791.36, 13, 843);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (235, 'Inheritance, The (Karami-ai)', false, '2019/11/14', 'book-fiv-835',
        'http://dummyimage.com/681x362.png/5fa2dd/ffffff',
        'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum.',
        560.74, 71, 442);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (236, 'Last Night', true, '2019/01/29', 'book-ugo-855', 'http://dummyimage.com/747x694.png/5fa2dd/ffffff',
        'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.',
        1595.70, 43, 29);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (237, 'Power of Kangwon Province, The (Kangwon-do ui him)', true, '2018/12/07', 'book-gvi-354',
        'http://dummyimage.com/623x414.png/cc0000/ffffff',
        'Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum.',
        2329.25, 47, 803);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (238, 'Come Sweet Death (Komm, süsser Tod)', false, '2018/03/18', 'book-dik-549',
        'http://dummyimage.com/574x456.png/5fa2dd/ffffff',
        'Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam. Nam tristique tortor eu pede.',
        3585.69, 24, 427);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (239, 'That''s Black Entertainment', true, '2021/01/18', 'book-gqz-415',
        'http://dummyimage.com/471x381.png/ff4444/ffffff',
        'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.',
        2273.40, 7, 817);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (240, 'Glass Menagerie, The', false, '2020/04/09', 'book-mbh-982',
        'http://dummyimage.com/335x651.png/dddddd/000000',
        'Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim.',
        1679.03, 23, 246);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (241, 'Wheel, The (La Roue)', true, '2018/05/24', 'book-fcr-579',
        'http://dummyimage.com/433x677.png/ff4444/ffffff',
        'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien.',
        3093.47, 93, 556);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (242, 'Journey of Hope (Reise der Hoffnung)', true, '2019/07/16', 'book-dyy-594',
        'http://dummyimage.com/428x382.png/dddddd/000000',
        'Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.',
        243.28, 71, 490);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (243, 'Riki-Oh: The Story of Ricky (Lik Wong)', true, '2020/08/01', 'book-jqi-622',
        'http://dummyimage.com/254x559.png/5fa2dd/ffffff',
        'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.',
        3975.54, 76, 746);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (244, 'Revenge of the Nerds III: The Next Generation', true, '2018/11/26', 'book-zpf-070',
        'http://dummyimage.com/391x643.png/5fa2dd/ffffff',
        'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue.',
        943.05, 36, 987);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (245, 'Fighting Prince of Donegal, The', false, '2019/01/25', 'book-ztb-353',
        'http://dummyimage.com/773x642.png/cc0000/ffffff',
        'Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat.',
        1174.24, 60, 43);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (246, 'Where the Money Is', false, '2019/06/07', 'book-cwa-328',
        'http://dummyimage.com/545x616.png/dddddd/000000',
        'Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst.',
        2137.47, 63, 665);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (247, 'Transformers: Dark of the Moon', true, '2019/10/17', 'book-ybu-632',
        'http://dummyimage.com/267x696.png/ff4444/ffffff',
        'Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc.',
        3379.50, 82, 503);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (248, 'Dust to Glory', true, '2019/10/25', 'book-fri-102', 'http://dummyimage.com/396x639.png/5fa2dd/ffffff',
        'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor. Duis mattis egestas metus.',
        943.64, 51, 927);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (249, 'Diario de un skin', false, '2020/09/16', 'book-pfs-388',
        'http://dummyimage.com/723x740.png/ff4444/ffffff',
        'Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.',
        1227.38, 13, 308);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (250, 'Taxing Woman, A (Marusa no onna)', true, '2018/10/01', 'book-ffn-204',
        'http://dummyimage.com/565x635.png/5fa2dd/ffffff',
        'Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum.',
        3697.31, 29, 261);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (251, 'Puppet Master: The Legacy (Puppet Master 8)', false, '2019/10/24', 'book-fea-834',
        'http://dummyimage.com/597x668.png/cc0000/ffffff',
        'Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat.',
        269.81, 12, 874);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (252, 'Barney''s Version', false, '2021/01/20', 'book-ugl-835',
        'http://dummyimage.com/357x726.png/ff4444/ffffff',
        'Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio.',
        3241.00, 62, 137);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (253, 'Proof of Life', true, '2018/10/13', 'book-sdz-145', 'http://dummyimage.com/459x424.png/ff4444/ffffff',
        'Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.',
        1868.45, 25, 204);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (254, 'Strait-Jacket', false, '2018/08/09', 'book-icn-123', 'http://dummyimage.com/393x612.png/5fa2dd/ffffff',
        'Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.',
        3383.82, 63, 591);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (255, 'Secret, A (Un secret)', true, '2021/01/24', 'book-nhr-330',
        'http://dummyimage.com/532x638.png/ff4444/ffffff',
        'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.',
        3214.81, 38, 9);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (256, 'Night of the Living Dorks', true, '2018/08/24', 'book-fbm-811',
        'http://dummyimage.com/381x647.png/5fa2dd/ffffff',
        'Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices.',
        622.47, 46, 851);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (257, 'Amish Grace', false, '2020/08/19', 'book-bte-896', 'http://dummyimage.com/621x360.png/ff4444/ffffff',
        'Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam.',
        2248.23, 5, 234);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (258, 'Ingeborg Holm', false, '2020/12/09', 'book-qju-033', 'http://dummyimage.com/739x436.png/cc0000/ffffff',
        'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl.',
        3541.19, 12, 131);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (259, 'Apollo 18', false, '2020/01/22', 'book-xph-150', 'http://dummyimage.com/469x581.png/dddddd/000000',
        'Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo.',
        118.40, 48, 1000);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (260, 'Great Balls of Fire!', true, '2019/08/05', 'book-ujh-447',
        'http://dummyimage.com/472x333.png/dddddd/000000',
        'Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus.',
        829.11, 32, 210);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (261, 'Last Valley, The', true, '2020/02/14', 'book-oqi-217', 'http://dummyimage.com/423x696.png/ff4444/ffffff',
        'In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.',
        1929.07, 78, 244);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (262, 'Congo', true, '2019/10/20', 'book-nwv-471', 'http://dummyimage.com/488x723.png/dddddd/000000',
        'Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.',
        729.21, 21, 130);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (263, 'Joyeux Noël (Merry Christmas)', true, '2020/03/03', 'book-neu-249',
        'http://dummyimage.com/557x604.png/cc0000/ffffff',
        'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum.',
        4669.35, 27, 96);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (264, 'Magic Gloves, The (Los guantes mágicos)', true, '2020/03/16', 'book-olf-512',
        'http://dummyimage.com/574x626.png/5fa2dd/ffffff',
        'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus.',
        1182.12, 64, 25);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (265, 'Beast from 20,000 Fathoms, The', false, '2018/08/07', 'book-ubd-403',
        'http://dummyimage.com/766x418.png/5fa2dd/ffffff',
        'Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam.',
        3981.95, 90, 880);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (266, 'Separate Lies', true, '2019/04/17', 'book-bim-761', 'http://dummyimage.com/685x435.png/ff4444/ffffff',
        'Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus.',
        1403.89, 69, 834);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (267, 'Return of the Magnificent Seven, The (a.k.a. Return of the Seven)', true, '2018/11/07', 'book-obf-292',
        'http://dummyimage.com/747x390.png/cc0000/ffffff',
        'Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue.',
        774.36, 44, 617);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (268, 'Violent Years, The', false, '2018/10/23', 'book-oqq-555',
        'http://dummyimage.com/338x533.png/cc0000/ffffff',
        'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci.',
        1960.80, 47, 312);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (269, 'Boys Life 4: Four Play', false, '2020/11/01', 'book-wxz-859',
        'http://dummyimage.com/441x613.png/cc0000/ffffff',
        'In congue. Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.',
        3147.99, 29, 993);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (270, 'General Died at Dawn, The', false, '2020/04/10', 'book-jpf-489',
        'http://dummyimage.com/354x382.png/5fa2dd/ffffff',
        'Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis.',
        794.03, 19, 639);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (271, 'War of the Worlds, The', false, '2020/02/19', 'book-zod-409',
        'http://dummyimage.com/399x787.png/ff4444/ffffff',
        'Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam. Nam tristique tortor eu pede.',
        2957.88, 16, 137);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (272, 'Meltdown (Shu dan long wei)', true, '2018/09/29', 'book-xlh-212',
        'http://dummyimage.com/614x478.png/5fa2dd/ffffff',
        'Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis.',
        2393.24, 21, 700);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (273, 'Ffolkes', true, '2018/07/01', 'book-eqz-388', 'http://dummyimage.com/534x522.png/cc0000/ffffff',
        'Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
        3798.84, 98, 246);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (274, 'Mysterious Mr. Moto', true, '2018/12/16', 'book-ucn-207',
        'http://dummyimage.com/353x739.png/ff4444/ffffff',
        'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.',
        574.84, 83, 969);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (275, 'Moment to Remember, A (Nae meorisokui jiwoogae)', false, '2019/12/02', 'book-yjl-272',
        'http://dummyimage.com/421x430.png/ff4444/ffffff',
        'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.',
        4495.11, 47, 508);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (276, '$9.99', true, '2019/10/23', 'book-tek-790', 'http://dummyimage.com/790x470.png/dddddd/000000',
        'Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc.',
        4569.07, 21, 970);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (277, 'John Carter', false, '2020/10/21', 'book-fpg-385', 'http://dummyimage.com/372x454.png/cc0000/ffffff',
        'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio.',
        3184.74, 85, 649);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (278, 'Antoine and Colette (Antoine et Colette)', false, '2020/12/25', 'book-uva-893',
        'http://dummyimage.com/356x553.png/cc0000/ffffff',
        'Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus.',
        4127.56, 94, 282);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (279, 'Split Second', true, '2019/10/12', 'book-jbz-657', 'http://dummyimage.com/361x602.png/5fa2dd/ffffff',
        'Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis.',
        2055.33, 58, 657);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (280, 'Sticky Fingers of Time, The', true, '2019/09/04', 'book-zqh-692',
        'http://dummyimage.com/449x571.png/5fa2dd/ffffff',
        'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio.',
        1229.10, 88, 436);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (281, 'Platinum Blonde', false, '2019/06/18', 'book-yih-066', 'http://dummyimage.com/794x741.png/dddddd/000000',
        'Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem.',
        229.11, 15, 527);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (282, 'Ruhr', true, '2018/06/09', 'book-wlh-697', 'http://dummyimage.com/663x613.png/dddddd/000000',
        'Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis.',
        767.21, 31, 994);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (283, 'Blue Angel, The (Blaue Engel, Der)', true, '2019/07/11', 'book-uwx-429',
        'http://dummyimage.com/597x555.png/dddddd/000000',
        'Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit.',
        381.78, 84, 765);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (284, 'Barbarosa', false, '2019/01/30', 'book-yqq-913', 'http://dummyimage.com/599x314.png/5fa2dd/ffffff',
        'In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus.',
        860.58, 13, 425);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (285, 'Sarafina!', false, '2019/03/13', 'book-jjj-541', 'http://dummyimage.com/389x619.png/5fa2dd/ffffff',
        'Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim.',
        3337.38, 43, 77);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (286, 'Chariots of the Gods (Erinnerungen an die Zukunft)', false, '2020/08/10', 'book-alu-933',
        'http://dummyimage.com/530x374.png/ff4444/ffffff',
        'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.',
        2165.52, 37, 655);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (287, 'Gumby: The Movie', true, '2018/09/16', 'book-yfl-338', 'http://dummyimage.com/489x739.png/dddddd/000000',
        'Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.',
        3074.00, 25, 319);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (288, 'Not Suitable for Children', true, '2020/05/11', 'book-nah-169',
        'http://dummyimage.com/590x658.png/dddddd/000000',
        'Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante.',
        4889.39, 54, 67);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (289, 'Winning of Barbara Worth, The', true, '2020/01/19', 'book-hhy-370',
        'http://dummyimage.com/474x488.png/ff4444/ffffff',
        'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy.',
        4867.26, 4, 62);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (290, 'Herod''s Law (Ley de Herodes, La)', true, '2019/11/04', 'book-xfo-101',
        'http://dummyimage.com/425x632.png/dddddd/000000',
        'Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula.',
        1406.43, 17, 170);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (291, 'Fistful of Dollars, A (Per un pugno di dollari)', false, '2019/07/05', 'book-riq-963',
        'http://dummyimage.com/436x750.png/dddddd/000000',
        'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus.',
        3156.88, 39, 747);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (292, 'Vanishing, The (Spoorloos)', false, '2019/06/25', 'book-lva-624',
        'http://dummyimage.com/258x705.png/5fa2dd/ffffff',
        'Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.',
        3139.84, 75, 969);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (293, 'Certain Kind Of Death, A', false, '2018/04/16', 'book-pti-144',
        'http://dummyimage.com/299x715.png/ff4444/ffffff',
        'Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.',
        2178.29, 3, 282);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (294, 'Garfield: A Tail of Two Kitties', false, '2020/05/22', 'book-zhz-001',
        'http://dummyimage.com/265x420.png/5fa2dd/ffffff',
        'Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus.',
        3907.34, 50, 808);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (295, 'Sleep Room, The', false, '2020/11/22', 'book-zhy-157', 'http://dummyimage.com/780x300.png/dddddd/000000',
        'Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.',
        2490.64, 84, 640);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (296, 'Mulan', false, '2020/05/11', 'book-aor-990', 'http://dummyimage.com/737x789.png/5fa2dd/ffffff',
        'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros.',
        2106.35, 49, 331);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (297, 'Seafarers, The', true, '2018/10/05', 'book-hgk-402', 'http://dummyimage.com/457x288.png/ff4444/ffffff',
        'Praesent lectus. Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend.',
        4606.91, 26, 394);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (298, 'Home of the Brave', false, '2019/01/08', 'book-ddo-633',
        'http://dummyimage.com/260x425.png/dddddd/000000',
        'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est.',
        584.10, 89, 558);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (299, 'Stuff and Dough (Marfa si banii)', false, '2018/11/21', 'book-obs-011',
        'http://dummyimage.com/708x281.png/5fa2dd/ffffff',
        'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue.',
        863.36, 59, 735);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (300, 'Peaceful Warrior', false, '2020/11/09', 'book-gbw-042', 'http://dummyimage.com/288x667.png/5fa2dd/ffffff',
        'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.',
        2856.98, 56, 465);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (301, 'Alive', true, '2018/10/30', 'book-bmg-807', 'http://dummyimage.com/445x734.png/ff4444/ffffff',
        'Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna.',
        4821.21, 41, 519);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (302, 'Love in the Afternoon (Chloe in the Afternoon)  (L''amour l''après-midi)', false, '2019/09/06',
        'book-eqb-482', 'http://dummyimage.com/368x779.png/5fa2dd/ffffff',
        'Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula.',
        2115.00, 35, 548);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (303, 'House of Games', false, '2020/01/31', 'book-lfi-471', 'http://dummyimage.com/490x263.png/ff4444/ffffff',
        'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue.',
        1264.09, 97, 975);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (304, 'Goodbye Charlie', false, '2020/01/26', 'book-ujr-957', 'http://dummyimage.com/767x378.png/5fa2dd/ffffff',
        'Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis.',
        576.57, 38, 591);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (305, 'Same Same But Different', false, '2018/09/19', 'book-aky-973',
        'http://dummyimage.com/639x729.png/ff4444/ffffff',
        'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
        2993.76, 48, 543);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (306, 'Dancer Upstairs, The', true, '2020/09/25', 'book-cdk-325',
        'http://dummyimage.com/664x739.png/5fa2dd/ffffff',
        'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo.',
        2269.33, 42, 953);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (307, 'The Circle', true, '2020/11/22', 'book-dfm-209', 'http://dummyimage.com/310x770.png/cc0000/ffffff',
        'Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
        2727.13, 31, 799);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (308, 'People Like Us', false, '2018/07/18', 'book-dpk-667', 'http://dummyimage.com/416x645.png/cc0000/ffffff',
        'Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices.',
        438.61, 12, 150);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (309, 'Boys Life 4: Four Play', true, '2020/12/07', 'book-sgv-442',
        'http://dummyimage.com/690x304.png/cc0000/ffffff',
        'In congue. Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna.',
        2835.03, 70, 292);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (310, 'Justin Bieber''s Believe', true, '2019/01/14', 'book-rrz-615',
        'http://dummyimage.com/343x598.png/ff4444/ffffff',
        'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum.',
        1495.66, 29, 919);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (311, 'Ice Cream Man', true, '2020/10/17', 'book-ogm-538', 'http://dummyimage.com/800x551.png/ff4444/ffffff',
        'Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.',
        4110.71, 35, 119);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (312, 'Faces of Death', false, '2021/01/05', 'book-nsx-185', 'http://dummyimage.com/743x629.png/cc0000/ffffff',
        'Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst.',
        1165.68, 1, 301);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (313, 'Say Anything...', true, '2018/11/23', 'book-phn-428', 'http://dummyimage.com/674x631.png/ff4444/ffffff',
        'Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo.',
        804.00, 82, 867);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (314, 'Night on Earth', true, '2019/03/19', 'book-jjl-096', 'http://dummyimage.com/583x723.png/dddddd/000000',
        'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo.',
        2162.73, 46, 994);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (315, 'Memories of Me', true, '2019/04/18', 'book-lrs-652', 'http://dummyimage.com/403x508.png/5fa2dd/ffffff',
        'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio.',
        318.22, 18, 473);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (316, 'Bullets or Ballots', false, '2020/09/05', 'book-kqa-480',
        'http://dummyimage.com/654x616.png/5fa2dd/ffffff',
        'Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo.',
        445.31, 62, 810);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (317, 'Just Around the Corner', false, '2019/08/29', 'book-ivt-509',
        'http://dummyimage.com/689x541.png/cc0000/ffffff',
        'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.',
        2560.25, 74, 765);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (318, 'Mercenary for Justice', true, '2019/05/03', 'book-iek-265',
        'http://dummyimage.com/758x375.png/cc0000/ffffff',
        'Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique.',
        1052.59, 23, 718);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (319, 'ATM', false, '2019/07/17', 'book-evv-225', 'http://dummyimage.com/429x319.png/ff4444/ffffff',
        'Proin risus. Praesent lectus. Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.',
        2705.10, 41, 392);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (320, 'Defending Your Life', true, '2020/03/22', 'book-ukp-073',
        'http://dummyimage.com/409x403.png/cc0000/ffffff',
        'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo.',
        3551.98, 40, 435);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (321, 'Mom and Dad Save the World', true, '2019/06/30', 'book-tcn-878',
        'http://dummyimage.com/723x275.png/cc0000/ffffff',
        'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo.',
        4852.97, 76, 257);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (322, 'Double Solitaire', true, '2020/07/29', 'book-sys-622', 'http://dummyimage.com/482x652.png/5fa2dd/ffffff',
        'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est.',
        4866.22, 16, 182);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (323, 'Dirty Dozen: The Deadly Mission', true, '2020/10/05', 'book-sdg-595',
        'http://dummyimage.com/643x474.png/dddddd/000000',
        'Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl.',
        848.28, 77, 963);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (324, 'Midsummer Night''s Dream, A', true, '2019/06/30', 'book-cfd-891',
        'http://dummyimage.com/661x607.png/cc0000/ffffff',
        'Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.',
        3662.19, 10, 952);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (325, 'Life of Oharu, The (Saikaku ichidai onna)', false, '2020/06/17', 'book-mfo-524',
        'http://dummyimage.com/476x509.png/5fa2dd/ffffff',
        'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim.',
        3885.02, 46, 478);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (326, 'Source Code', true, '2019/07/23', 'book-nsg-772', 'http://dummyimage.com/431x469.png/cc0000/ffffff',
        'Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus.',
        4096.69, 79, 155);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (327, 'Stolen Summer', true, '2020/01/15', 'book-aug-821', 'http://dummyimage.com/256x472.png/cc0000/ffffff',
        'Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.',
        2885.07, 53, 8);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (328, 'You''ve Been Trumped', false, '2018/04/09', 'book-wdh-342',
        'http://dummyimage.com/734x605.png/5fa2dd/ffffff',
        'Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum.',
        1838.56, 31, 394);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (329, 'Loner (Woetoli)', true, '2020/06/06', 'book-ivs-901', 'http://dummyimage.com/259x347.png/cc0000/ffffff',
        'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim.',
        1540.66, 83, 119);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (330, 'Return of the Living Dead 3', true, '2020/04/04', 'book-lny-097',
        'http://dummyimage.com/575x303.png/dddddd/000000',
        'Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.',
        2019.13, 88, 605);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (331, 'Saving General Yang', false, '2019/03/08', 'book-cdd-561',
        'http://dummyimage.com/336x753.png/ff4444/ffffff',
        'Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat.',
        4118.61, 5, 627);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (332, 'X-Men', true, '2019/07/01', 'book-xwc-911', 'http://dummyimage.com/370x674.png/cc0000/ffffff',
        'Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.',
        3745.97, 56, 822);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (333, 'And So It Is', true, '2019/06/14', 'book-rvv-332', 'http://dummyimage.com/575x400.png/5fa2dd/ffffff',
        'Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam.',
        2800.64, 6, 723);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (334, 'Kafka', true, '2019/04/12', 'book-pwp-563', 'http://dummyimage.com/382x255.png/ff4444/ffffff',
        'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.',
        878.80, 19, 753);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (335, 'Winged Migration (Peuple migrateur, Le)', true, '2020/04/21', 'book-oso-336',
        'http://dummyimage.com/486x482.png/ff4444/ffffff',
        'Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus.',
        3010.19, 6, 154);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (336, 'Family Friend, The (L''amico di famiglia)', false, '2021/02/06', 'book-znw-397',
        'http://dummyimage.com/530x515.png/cc0000/ffffff',
        'Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
        2269.17, 45, 270);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (337, 'Sweetie', true, '2019/10/22', 'book-ejs-457', 'http://dummyimage.com/599x336.png/ff4444/ffffff',
        'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.',
        909.65, 65, 638);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (338, 'It''s a Very Merry Muppet Christmas Movie', false, '2018/05/24', 'book-kiv-273',
        'http://dummyimage.com/322x511.png/5fa2dd/ffffff',
        'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.',
        502.46, 5, 813);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (339, 'Maidens'' Conspiracy, The (Tirante el Blanco)', true, '2018/12/02', 'book-zur-980',
        'http://dummyimage.com/484x641.png/cc0000/ffffff',
        'Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio.',
        1656.29, 79, 12);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (340, 'Silk', false, '2019/01/03', 'book-tiz-403', 'http://dummyimage.com/564x474.png/cc0000/ffffff',
        'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.',
        950.79, 49, 97);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (341, 'Bangkok Dangerous', false, '2020/12/08', 'book-eug-065',
        'http://dummyimage.com/739x691.png/cc0000/ffffff',
        'Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.',
        3165.03, 1, 167);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (342, 'Serrallonga', true, '2018/12/17', 'book-moc-039', 'http://dummyimage.com/453x745.png/cc0000/ffffff',
        'Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat.',
        4733.82, 74, 580);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (343, 'La Morte Rouge', false, '2018/06/09', 'book-rpn-198', 'http://dummyimage.com/348x750.png/cc0000/ffffff',
        'Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy.',
        2549.32, 13, 444);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (344, 'Woodstock', true, '2020/06/04', 'book-wtl-231', 'http://dummyimage.com/512x392.png/ff4444/ffffff',
        'Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien.',
        4128.66, 25, 404);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (345, 'When Ladies Meet', true, '2019/12/26', 'book-ouh-455', 'http://dummyimage.com/773x334.png/5fa2dd/ffffff',
        'Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio.',
        1255.63, 43, 93);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (346, 'Robot Stories', false, '2018/02/27', 'book-vbg-136', 'http://dummyimage.com/690x500.png/cc0000/ffffff',
        'Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci.',
        4785.16, 52, 927);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (347, 'Ulee''s Gold', false, '2018/10/03', 'book-thl-783', 'http://dummyimage.com/338x579.png/ff4444/ffffff',
        'Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.',
        3896.27, 35, 2);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (348, 'Spirit: Stallion of the Cimarron', false, '2018/10/19', 'book-tes-476',
        'http://dummyimage.com/710x487.png/ff4444/ffffff',
        'Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis.',
        2638.52, 15, 748);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (349, 'Siege, The', true, '2018/03/04', 'book-bcs-161', 'http://dummyimage.com/659x611.png/cc0000/ffffff',
        'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum.',
        573.43, 50, 969);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (350, 'Gertie the Dinosaur', true, '2019/10/06', 'book-rjf-019',
        'http://dummyimage.com/404x257.png/ff4444/ffffff',
        'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc.',
        2434.09, 62, 809);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (351, 'Gabriel Over the White House', false, '2019/01/06', 'book-xjq-935',
        'http://dummyimage.com/453x567.png/5fa2dd/ffffff',
        'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.',
        116.52, 74, 664);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (352, 'Swimmer, The', false, '2020/01/15', 'book-umm-972', 'http://dummyimage.com/603x608.png/dddddd/000000',
        'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia.',
        3424.83, 24, 168);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (353, 'Silent Trigger', false, '2020/11/08', 'book-wxe-161', 'http://dummyimage.com/400x398.png/5fa2dd/ffffff',
        'Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.',
        180.62, 99, 979);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (354, 'Men Don''t Leave', true, '2019/06/19', 'book-jti-251', 'http://dummyimage.com/637x788.png/5fa2dd/ffffff',
        'Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia.',
        4720.09, 7, 60);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (355, 'Nadine', true, '2019/06/13', 'book-kez-506', 'http://dummyimage.com/798x627.png/5fa2dd/ffffff',
        'Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus.',
        3184.60, 92, 605);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (356, 'Gray''s Anatomy', true, '2019/06/11', 'book-vpu-206', 'http://dummyimage.com/411x480.png/ff4444/ffffff',
        'Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.',
        1703.38, 30, 61);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (357, 'Marooned in Iraq (Gomgashtei dar Aragh)', true, '2020/04/06', 'book-mkl-580',
        'http://dummyimage.com/589x778.png/dddddd/000000',
        'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam.',
        2604.22, 20, 374);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (358, 'Ernest Goes to Africa', true, '2020/05/13', 'book-ggp-152',
        'http://dummyimage.com/397x794.png/dddddd/000000',
        'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl.',
        451.60, 56, 151);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (359, 'Manhattan', true, '2019/04/14', 'book-mbs-381', 'http://dummyimage.com/458x684.png/dddddd/000000',
        'Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst.',
        2315.65, 43, 663);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (360, 'El malvado Carabel', false, '2018/05/17', 'book-oqd-501',
        'http://dummyimage.com/594x559.png/ff4444/ffffff',
        'Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque.',
        1253.59, 78, 969);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (361, 'Aprile', false, '2019/07/30', 'book-tzk-940', 'http://dummyimage.com/354x709.png/5fa2dd/ffffff',
        'Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti.',
        3498.25, 85, 695);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (362, 'Bertsolari', true, '2019/07/07', 'book-jsi-108', 'http://dummyimage.com/334x653.png/dddddd/000000',
        'Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna.',
        3272.58, 83, 552);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (363, 'Inherit the Wind', true, '2019/12/30', 'book-alx-168', 'http://dummyimage.com/461x605.png/ff4444/ffffff',
        'In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst.',
        1000.14, 82, 159);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (364, 'Bells, The', false, '2019/05/10', 'book-cuk-384', 'http://dummyimage.com/417x529.png/cc0000/ffffff',
        'Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor.',
        947.87, 92, 84);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (365, 'People on Sunday (Menschen am Sonntag)', false, '2020/01/02', 'book-hhf-176',
        'http://dummyimage.com/482x677.png/ff4444/ffffff',
        'Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum.',
        3860.78, 74, 856);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (366, 'Ernest Goes to Jail', true, '2019/08/22', 'book-vli-696',
        'http://dummyimage.com/457x667.png/dddddd/000000',
        'Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo.',
        893.72, 27, 179);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (367, 'Chain Reaction', true, '2020/07/29', 'book-znj-341', 'http://dummyimage.com/636x602.png/ff4444/ffffff',
        'Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor. Duis mattis egestas metus.',
        2825.39, 97, 593);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (368, 'School Daze', false, '2018/07/15', 'book-xyp-106', 'http://dummyimage.com/788x703.png/ff4444/ffffff',
        'Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.',
        4141.51, 7, 125);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (369, 'Last of the Red Hot Lovers', false, '2018/10/30', 'book-kie-596',
        'http://dummyimage.com/478x586.png/dddddd/000000',
        'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim.',
        816.21, 42, 636);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (370, 'Year of Living Dangerously, The', true, '2018/05/01', 'book-uys-493',
        'http://dummyimage.com/540x386.png/5fa2dd/ffffff',
        'Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi.',
        1909.90, 69, 400);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (371, 'Roger & Me', true, '2019/03/27', 'book-inj-191', 'http://dummyimage.com/665x577.png/5fa2dd/ffffff',
        'Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti.',
        2243.18, 22, 770);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (372, 'Prime Suspect', false, '2020/06/04', 'book-oqm-772', 'http://dummyimage.com/462x573.png/ff4444/ffffff',
        'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.',
        961.70, 61, 345);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (373, 'Onechanbara - Zombie Bikini Squad (a.k.a. Oneechanbara: The Movie)', false, '2018/10/19', 'book-jrn-158',
        'http://dummyimage.com/413x340.png/dddddd/000000',
        'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius.',
        4518.06, 57, 683);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (374, 'Faces of Schlock', false, '2019/07/20', 'book-rds-142', 'http://dummyimage.com/331x680.png/dddddd/000000',
        'Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla.',
        2624.23, 89, 190);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (375, 'A.K.', true, '2018/07/05', 'book-qcw-567', 'http://dummyimage.com/352x502.png/cc0000/ffffff',
        'Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim.',
        1896.38, 18, 975);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (376, 'Magic Mike', true, '2019/06/06', 'book-emc-106', 'http://dummyimage.com/311x682.png/dddddd/000000',
        'Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo.',
        2205.76, 99, 363);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (377, 'It''s a Wonderful Afterlife', true, '2018/09/19', 'book-eoa-553',
        'http://dummyimage.com/441x459.png/cc0000/ffffff',
        'Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst.',
        1053.20, 41, 607);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (378, 'Just Friends', false, '2018/04/21', 'book-zlq-652', 'http://dummyimage.com/398x577.png/5fa2dd/ffffff',
        'Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst.',
        2999.53, 96, 23);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (379, 'Chocolat', false, '2019/09/22', 'book-dhn-293', 'http://dummyimage.com/563x416.png/ff4444/ffffff',
        'Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat.',
        1704.22, 76, 157);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (380, 'Road to Zanzibar', true, '2019/03/18', 'book-fcz-874', 'http://dummyimage.com/639x325.png/dddddd/000000',
        'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus.',
        486.54, 62, 384);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (381, 'Ewoks: The Battle for Endor', true, '2020/05/03', 'book-xxm-186',
        'http://dummyimage.com/693x673.png/dddddd/000000',
        'Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula.',
        3241.55, 56, 679);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (382, 'Heights', false, '2020/06/30', 'book-nvx-627', 'http://dummyimage.com/746x369.png/ff4444/ffffff',
        'Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique.',
        1881.88, 37, 408);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (383, 'Fall of the House of Usher, The (chute de la maison Usher, La)', true, '2019/09/21', 'book-igc-105',
        'http://dummyimage.com/418x270.png/cc0000/ffffff',
        'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus.',
        2191.51, 40, 924);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (384, 'Olivier, Olivier', true, '2019/01/15', 'book-ipe-696', 'http://dummyimage.com/607x293.png/dddddd/000000',
        'Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis.',
        4099.92, 28, 438);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (385, 'Angel of Death', true, '2018/10/09', 'book-yam-770', 'http://dummyimage.com/391x549.png/5fa2dd/ffffff',
        'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl.',
        3861.60, 99, 881);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (386, 'This Is Not a Film (In film nist)', false, '2020/10/07', 'book-kyo-771',
        'http://dummyimage.com/509x733.png/dddddd/000000',
        'Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis.',
        738.69, 58, 307);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (387, 'Breach, The (Rupture, La)', false, '2020/08/07', 'book-fug-321',
        'http://dummyimage.com/408x419.png/cc0000/ffffff',
        'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl.',
        1173.31, 57, 622);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (388, 'Ilsa, She Wolf of the SS', false, '2019/11/09', 'book-wcc-012',
        'http://dummyimage.com/481x641.png/cc0000/ffffff',
        'Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.',
        3953.22, 74, 659);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (389, 'Age of Stupid, The', false, '2020/01/04', 'book-mub-791',
        'http://dummyimage.com/740x523.png/5fa2dd/ffffff',
        'Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.',
        2717.38, 47, 256);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (390, 'Arrival of a Train, The', false, '2019/01/25', 'book-qdl-748',
        'http://dummyimage.com/534x611.png/dddddd/000000',
        'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc.',
        3368.54, 21, 557);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (391, '7 Virgins (7 vírgenes)', true, '2019/05/16', 'book-use-812',
        'http://dummyimage.com/648x341.png/dddddd/000000',
        'Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est.',
        1792.04, 19, 150);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (392, 'Nightwing', false, '2018/08/07', 'book-wyr-450', 'http://dummyimage.com/728x297.png/dddddd/000000',
        'Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.',
        2640.22, 53, 180);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (393, 'Other One, The', false, '2018/08/21', 'book-xkr-047', 'http://dummyimage.com/547x340.png/5fa2dd/ffffff',
        'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim.',
        3408.22, 23, 171);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (394, 'Presidio, The', false, '2020/07/14', 'book-ddu-919', 'http://dummyimage.com/493x621.png/dddddd/000000',
        'Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum.',
        1179.97, 5, 680);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (395, 'Practical Magic', false, '2020/01/03', 'book-cce-708', 'http://dummyimage.com/526x333.png/ff4444/ffffff',
        'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim.',
        776.46, 26, 188);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (396, 'Covenant, The', false, '2018/04/05', 'book-uol-205', 'http://dummyimage.com/463x362.png/dddddd/000000',
        'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.',
        3498.11, 75, 321);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (397, 'Nightcrawler', true, '2019/06/20', 'book-kwg-328', 'http://dummyimage.com/470x763.png/dddddd/000000',
        'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.',
        1504.26, 92, 418);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (398, 'Katyn', true, '2020/06/27', 'book-zdq-638', 'http://dummyimage.com/349x688.png/5fa2dd/ffffff',
        'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo.',
        3151.94, 72, 471);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (399, 'Beach Boys: An American Family, The', true, '2020/07/17', 'book-ujn-616',
        'http://dummyimage.com/448x635.png/cc0000/ffffff',
        'In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh.',
        4373.91, 33, 591);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (400, 'Return of the Vampire, The', false, '2020/09/05', 'book-dil-980',
        'http://dummyimage.com/790x294.png/ff4444/ffffff',
        'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue.',
        3873.18, 20, 443);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (401, 'Raising Arizona', false, '2019/12/09', 'book-rpl-132', 'http://dummyimage.com/716x721.png/ff4444/ffffff',
        'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.',
        452.30, 77, 481);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (402, 'Benny & Joon', false, '2020/01/18', 'book-req-180', 'http://dummyimage.com/305x708.png/dddddd/000000',
        'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci.',
        2799.97, 0, 975);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (403, 'Ace Attorney (Gyakuten saiban)', true, '2018/07/05', 'book-qjd-917',
        'http://dummyimage.com/574x777.png/cc0000/ffffff',
        'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui.',
        4806.91, 90, 13);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (404, 'DiG!', true, '2018/11/30', 'book-dje-561', 'http://dummyimage.com/707x628.png/dddddd/000000',
        'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla.',
        4493.40, 99, 368);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (405, 'Bringing Down the House', true, '2020/05/17', 'book-wbk-057',
        'http://dummyimage.com/629x726.png/dddddd/000000',
        'Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est.',
        2281.39, 36, 380);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (406, 'New York Ripper, The (Squartatore di New York, Lo)', false, '2018/09/28', 'book-bxw-244',
        'http://dummyimage.com/321x756.png/cc0000/ffffff',
        'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat.',
        306.30, 11, 479);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (407, 'Fires on the Plain (Nobi)', false, '2021/02/02', 'book-ceb-630',
        'http://dummyimage.com/592x495.png/cc0000/ffffff',
        'Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo.',
        1958.53, 56, 462);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (408, 'Bloody Murder 2: Closing Camp', false, '2019/10/13', 'book-txm-126',
        'http://dummyimage.com/524x551.png/5fa2dd/ffffff',
        'In congue. Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna.',
        3748.11, 76, 249);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (409, 'Children of Invention', true, '2019/05/27', 'book-cuv-035',
        'http://dummyimage.com/577x368.png/ff4444/ffffff',
        'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.',
        3367.32, 27, 909);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (410, 'Bullet for Joey, A', true, '2019/09/23', 'book-hsn-605',
        'http://dummyimage.com/497x659.png/dddddd/000000',
        'In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh.',
        1319.06, 11, 420);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (411, 'The Oblong Box', true, '2020/03/23', 'book-pur-271', 'http://dummyimage.com/296x573.png/dddddd/000000',
        'Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt.',
        412.98, 94, 565);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (412, 'Brown''s Requiem', true, '2019/05/25', 'book-twd-910', 'http://dummyimage.com/586x723.png/dddddd/000000',
        'Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.',
        1649.97, 78, 667);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (413, 'Somebody Up There Likes Me', true, '2018/06/27', 'book-svg-760',
        'http://dummyimage.com/277x409.png/5fa2dd/ffffff',
        'Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim.',
        4159.42, 76, 385);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (414, 'Gold Diggers of 1937', true, '2020/01/30', 'book-rdk-132',
        'http://dummyimage.com/628x273.png/cc0000/ffffff',
        'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.',
        2962.68, 94, 532);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (415, 'Boxing Gym', false, '2019/09/17', 'book-tnt-989', 'http://dummyimage.com/509x268.png/dddddd/000000',
        'Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis.',
        3533.81, 54, 236);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (416, 'Dangerous Moves (La diagonale du fou)', true, '2018/11/13', 'book-csc-761',
        'http://dummyimage.com/278x574.png/cc0000/ffffff',
        'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl.',
        2613.76, 50, 170);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (417, 'Place Beyond the Pines, The', false, '2019/12/22', 'book-khs-036',
        'http://dummyimage.com/530x520.png/ff4444/ffffff',
        'Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.',
        2295.07, 27, 376);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (418, 'One Hundred Mornings', true, '2018/12/18', 'book-qce-150',
        'http://dummyimage.com/347x420.png/5fa2dd/ffffff',
        'Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.',
        2240.65, 6, 913);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (419, 'Night Train Murders (Last Stop on the Night Train) (Ultimo treno della notte, L'')', true, '2020/02/13',
        'book-tcv-871', 'http://dummyimage.com/685x524.png/5fa2dd/ffffff',
        'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam.',
        2691.32, 16, 944);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (420, 'Off Limits', true, '2018/12/15', 'book-uvs-889', 'http://dummyimage.com/593x579.png/5fa2dd/ffffff',
        'Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis.',
        3229.62, 37, 206);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (421, 'Skirt Day (La journée de la jupe)', true, '2020/08/22', 'book-utp-242',
        'http://dummyimage.com/717x648.png/cc0000/ffffff',
        'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl. Nunc nisl.',
        3654.23, 65, 98);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (422, 'What Price Hollywood?', true, '2019/11/18', 'book-vod-163',
        'http://dummyimage.com/795x550.png/ff4444/ffffff',
        'Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus.',
        1890.84, 77, 603);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (423, 'Leonard Cohen: I''m Your Man', false, '2018/03/01', 'book-lnb-945',
        'http://dummyimage.com/566x541.png/dddddd/000000',
        'Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc.',
        1387.18, 4, 862);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (424, 'Accused, The', true, '2018/09/12', 'book-amg-381', 'http://dummyimage.com/585x717.png/ff4444/ffffff',
        'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio.',
        1042.46, 50, 101);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (425, 'Machine Gun Preacher', true, '2020/10/14', 'book-jhk-386',
        'http://dummyimage.com/557x558.png/dddddd/000000',
        'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus.',
        3683.20, 37, 691);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (426, 'Beauty and the Beast: The Enchanted Christmas', false, '2019/03/27', 'book-gdk-366',
        'http://dummyimage.com/462x603.png/5fa2dd/ffffff',
        'Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam. Nam tristique tortor eu pede.',
        796.84, 34, 757);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (427, 'Killer Meteors, The (Feng yu shuang liu xing)', false, '2018/04/27', 'book-lsl-027',
        'http://dummyimage.com/405x718.png/ff4444/ffffff',
        'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh.',
        4437.48, 91, 500);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (428, 'Into the Abyss', false, '2019/07/18', 'book-lgu-584', 'http://dummyimage.com/511x732.png/dddddd/000000',
        'Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum.',
        554.54, 25, 618);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (429, 'Elysium', true, '2019/12/10', 'book-zkg-733', 'http://dummyimage.com/469x678.png/dddddd/000000',
        'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.',
        1041.59, 91, 143);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (430, 'Family Law (Derecho de familia)', true, '2020/05/10', 'book-xec-244',
        'http://dummyimage.com/644x439.png/5fa2dd/ffffff',
        'Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.',
        633.67, 53, 498);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (431, 'Plaza Suite', false, '2018/10/25', 'book-noy-576', 'http://dummyimage.com/464x736.png/ff4444/ffffff',
        'Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula.',
        2874.56, 35, 235);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (432, 'Best Defense', false, '2020/03/14', 'book-keq-092', 'http://dummyimage.com/399x545.png/dddddd/000000',
        'In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi.',
        2797.24, 66, 531);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (433, 'Deck the Halls', true, '2018/07/16', 'book-tij-153', 'http://dummyimage.com/298x356.png/5fa2dd/ffffff',
        'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
        4888.72, 69, 975);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (434, 'Day Night Day Night', false, '2018/11/03', 'book-onh-530',
        'http://dummyimage.com/759x657.png/5fa2dd/ffffff',
        'Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.',
        4908.83, 98, 836);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (435, 'Laurence Anyways', false, '2019/05/23', 'book-ycw-410', 'http://dummyimage.com/424x798.png/cc0000/ffffff',
        'Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla.',
        1200.88, 72, 203);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (436, 'Profit, The', false, '2019/07/12', 'book-rgi-724', 'http://dummyimage.com/687x655.png/5fa2dd/ffffff',
        'Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus.',
        4951.97, 13, 306);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (437, 'Jason X', true, '2020/09/03', 'book-usx-695', 'http://dummyimage.com/705x666.png/dddddd/000000',
        'Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl.',
        4325.75, 10, 998);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (438, 'Soft Toilet Seats', false, '2019/10/07', 'book-qsm-782',
        'http://dummyimage.com/570x312.png/5fa2dd/ffffff',
        'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.',
        335.58, 14, 171);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (439, 'Great King, The (Der große König)', false, '2020/02/14', 'book-ytd-227',
        'http://dummyimage.com/297x626.png/dddddd/000000',
        'Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante.',
        1756.38, 16, 433);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (440, 'When Time Ran Out... (Day the World Ended, The)', false, '2018/11/30', 'book-ail-873',
        'http://dummyimage.com/410x465.png/cc0000/ffffff',
        'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi.',
        2261.60, 87, 544);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (441, 'Some Mother''s Son', false, '2020/09/05', 'book-wcs-088',
        'http://dummyimage.com/324x599.png/dddddd/000000',
        'Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat.',
        4721.18, 33, 776);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (442, 'Three Lives of Thomasina, The', true, '2018/02/18', 'book-tzp-921',
        'http://dummyimage.com/535x630.png/dddddd/000000',
        'Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo.',
        681.39, 52, 87);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (443, 'Dead Reckoning', true, '2019/08/31', 'book-mmn-114', 'http://dummyimage.com/798x637.png/5fa2dd/ffffff',
        'Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique.',
        3320.41, 29, 553);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (444, 'Incubus, The', true, '2020/03/20', 'book-hiv-594', 'http://dummyimage.com/503x264.png/ff4444/ffffff',
        'Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.',
        4335.67, 75, 384);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (445, 'Skin Game, The', false, '2019/06/02', 'book-zpw-425', 'http://dummyimage.com/404x287.png/ff4444/ffffff',
        'Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum.',
        585.68, 73, 813);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (446, 'Big Circus, The', true, '2020/01/28', 'book-nyc-277', 'http://dummyimage.com/691x522.png/dddddd/000000',
        'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.',
        3433.26, 39, 954);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (447, 'Four Rooms', false, '2020/06/05', 'book-ikk-369', 'http://dummyimage.com/644x513.png/cc0000/ffffff',
        'Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo.',
        1650.67, 39, 790);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (448, 'Sum of Us, The', false, '2018/10/02', 'book-dsz-951', 'http://dummyimage.com/757x262.png/5fa2dd/ffffff',
        'Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis.',
        771.26, 30, 522);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (449, 'Fallen Art (Sztuka spadania)', true, '2019/03/16', 'book-yoh-130',
        'http://dummyimage.com/727x496.png/cc0000/ffffff',
        'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.',
        1150.87, 9, 210);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (450, 'Ivanhoe', true, '2018/03/04', 'book-sbc-216', 'http://dummyimage.com/756x412.png/cc0000/ffffff',
        'Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus.',
        1505.19, 39, 495);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (451, 'Dead Space: Downfall', false, '2018/04/19', 'book-bha-881',
        'http://dummyimage.com/755x567.png/cc0000/ffffff',
        'Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl.',
        3935.49, 79, 688);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (452, 'Romance on the High Seas', false, '2020/06/10', 'book-cci-296',
        'http://dummyimage.com/763x564.png/dddddd/000000',
        'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.',
        1375.30, 73, 903);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (453, 'Preaching to the Perverted', true, '2020/06/08', 'book-csa-481',
        'http://dummyimage.com/334x656.png/5fa2dd/ffffff',
        'Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.',
        265.94, 93, 949);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (454, 'Dead Space: Downfall', true, '2019/09/01', 'book-yyv-075',
        'http://dummyimage.com/783x419.png/dddddd/000000',
        'Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.',
        2896.27, 27, 826);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (455, 'Fearless Freaks, The', false, '2019/09/26', 'book-xpl-971',
        'http://dummyimage.com/512x402.png/dddddd/000000',
        'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien.',
        1885.40, 32, 333);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (456, 'Dinosaur', false, '2020/03/25', 'book-fxz-416', 'http://dummyimage.com/299x485.png/ff4444/ffffff',
        'Proin risus. Praesent lectus. Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend.',
        4184.39, 73, 871);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (457, 'Army of Shadows (L''armée des ombres)', true, '2018/03/19', 'book-yuf-681',
        'http://dummyimage.com/780x451.png/dddddd/000000',
        'Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor. Duis mattis egestas metus.',
        484.17, 64, 236);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (458, 'Speed Racer', true, '2020/12/13', 'book-dsp-247', 'http://dummyimage.com/798x430.png/cc0000/ffffff',
        'Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum.',
        3037.38, 54, 333);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (459, 'Lords of Salem, The', false, '2019/04/15', 'book-wsb-457',
        'http://dummyimage.com/734x721.png/cc0000/ffffff',
        'Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo.',
        1600.03, 19, 780);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (460, 'Desi Boyz', true, '2018/08/15', 'book-dxh-080', 'http://dummyimage.com/733x575.png/5fa2dd/ffffff',
        'Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis.',
        4003.68, 64, 528);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (461, 'Tough Enough (Knallhart)', true, '2018/06/14', 'book-uuv-487',
        'http://dummyimage.com/428x598.png/dddddd/000000',
        'Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.',
        2486.62, 90, 343);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (462, 'Pool, The (Swimming Pool - Der Tod feiert mit) ', true, '2019/02/14', 'book-rrm-254',
        'http://dummyimage.com/633x477.png/ff4444/ffffff',
        'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci.',
        2101.71, 73, 815);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (463, 'Airport 1975', false, '2019/03/06', 'book-gru-867', 'http://dummyimage.com/745x381.png/5fa2dd/ffffff',
        'Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.',
        4290.92, 76, 949);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (464, 'Ollie Hopnoodle''s Haven of Bliss', false, '2018/10/03', 'book-hlr-667',
        'http://dummyimage.com/506x459.png/ff4444/ffffff',
        'Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl.',
        3129.80, 23, 913);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (465, 'In-Laws, The', true, '2018/11/09', 'book-rki-773', 'http://dummyimage.com/522x284.png/ff4444/ffffff',
        'Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh.',
        3862.27, 49, 89);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (466, 'Swimming with Sharks', true, '2018/07/06', 'book-gxw-631',
        'http://dummyimage.com/758x494.png/dddddd/000000',
        'Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.',
        1453.82, 11, 166);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (467, 'Sweet Bird of Youth', false, '2019/08/30', 'book-dqn-068',
        'http://dummyimage.com/624x663.png/dddddd/000000',
        'Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat.',
        4973.21, 2, 540);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (468, 'Lion King II: Simba''s Pride, The', true, '2020/11/05', 'book-dhr-532',
        'http://dummyimage.com/651x575.png/cc0000/ffffff',
        'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum.',
        2814.45, 12, 882);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (469, 'All Quiet on the Western Front', false, '2019/12/05', 'book-gjx-180',
        'http://dummyimage.com/633x564.png/ff4444/ffffff',
        'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.',
        1999.37, 69, 783);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (470, 'Fuck Up', false, '2020/07/15', 'book-gjc-310', 'http://dummyimage.com/542x514.png/ff4444/ffffff',
        'In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.',
        2889.33, 47, 876);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (471, 'Pippi on the Run (På rymmen med Pippi Långstrump)', true, '2020/02/04', 'book-kfb-857',
        'http://dummyimage.com/527x482.png/cc0000/ffffff',
        'Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla.',
        4030.69, 50, 694);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (472, 'Heavy Metal 2000', false, '2018/10/01', 'book-xjv-027', 'http://dummyimage.com/450x744.png/5fa2dd/ffffff',
        'Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst.',
        158.24, 88, 289);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (473, 'State Affairs (Une affaire d''état)', false, '2020/07/29', 'book-diu-449',
        'http://dummyimage.com/644x289.png/dddddd/000000',
        'Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.',
        414.26, 37, 300);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (474, 'Marooned in Iraq (Gomgashtei dar Aragh)', true, '2020/01/27', 'book-jpm-664',
        'http://dummyimage.com/628x428.png/ff4444/ffffff',
        'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.',
        574.74, 77, 695);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (475, 'The Search for Santa Paws', false, '2018/07/05', 'book-olz-189',
        'http://dummyimage.com/775x511.png/ff4444/ffffff',
        'Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla.',
        2624.27, 94, 989);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (476, 'Lili Marleen', true, '2020/07/10', 'book-fzd-205', 'http://dummyimage.com/545x594.png/5fa2dd/ffffff',
        'Proin risus. Praesent lectus. Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend.',
        4591.98, 86, 863);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (477, 'Winter in Wartime', true, '2019/12/01', 'book-wjm-652', 'http://dummyimage.com/600x521.png/5fa2dd/ffffff',
        'Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum.',
        3056.27, 15, 498);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (478, 'This Gun for Hire', false, '2020/12/21', 'book-hfv-634',
        'http://dummyimage.com/314x336.png/5fa2dd/ffffff',
        'Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio.',
        542.36, 53, 956);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (479, 'Look', false, '2019/09/13', 'book-dif-715', 'http://dummyimage.com/317x312.png/5fa2dd/ffffff',
        'Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.',
        3165.56, 94, 195);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (480, 'Penalty, The', false, '2019/12/30', 'book-djc-421', 'http://dummyimage.com/513x565.png/dddddd/000000',
        'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus.',
        1868.59, 25, 560);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (481, 'Airport', false, '2018/07/01', 'book-yav-279', 'http://dummyimage.com/519x738.png/ff4444/ffffff',
        'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo.',
        3664.48, 89, 578);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (482, 'Midnight', false, '2018/04/30', 'book-ajj-034', 'http://dummyimage.com/435x761.png/ff4444/ffffff',
        'Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc.',
        2273.16, 93, 524);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (483, 'Pot v raj', true, '2018/07/08', 'book-nlb-928', 'http://dummyimage.com/552x728.png/5fa2dd/ffffff',
        'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.',
        3126.24, 65, 776);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (484, 'Lost Souls', false, '2019/06/03', 'book-hos-103', 'http://dummyimage.com/757x696.png/ff4444/ffffff',
        'Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo.',
        4073.27, 38, 964);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (485, 'The Scapegoat', true, '2020/01/27', 'book-aui-683', 'http://dummyimage.com/547x367.png/ff4444/ffffff',
        'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus. Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio.',
        3934.33, 36, 306);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (486, 'Mothlight', false, '2018/12/19', 'book-ljg-981', 'http://dummyimage.com/338x617.png/5fa2dd/ffffff',
        'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.',
        1608.25, 29, 143);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (487, 'Hard Sun', false, '2019/10/19', 'book-dpd-968', 'http://dummyimage.com/772x658.png/5fa2dd/ffffff',
        'Proin risus. Praesent lectus. Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.',
        207.70, 95, 406);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (488, 'Snake of June, A (Rokugatsu no hebi)', true, '2018/10/06', 'book-ejc-590',
        'http://dummyimage.com/491x648.png/dddddd/000000',
        'Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam.',
        2302.95, 55, 154);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (489, 'Day the Sun Turned Cold, The (Tianguo niezi)', true, '2018/10/31', 'book-tse-314',
        'http://dummyimage.com/643x368.png/5fa2dd/ffffff',
        'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci.',
        4906.61, 12, 629);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (490, 'Zombie Holocaust (a.k.a. Doctor Butcher M.D.) (Zombi Holocaust)', false, '2018/03/24', 'book-zyx-804',
        'http://dummyimage.com/295x431.png/ff4444/ffffff',
        'Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.',
        277.58, 88, 550);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (491, 'Last Unicorn, The', true, '2019/03/16', 'book-qkr-180', 'http://dummyimage.com/727x364.png/ff4444/ffffff',
        'Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros.',
        3456.13, 55, 218);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (492, 'Shinobi No Mono 2: Vengeance (Zoku shinobi no mono)', false, '2019/10/28', 'book-yxq-637',
        'http://dummyimage.com/650x679.png/5fa2dd/ffffff',
        'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.',
        1738.77, 91, 172);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (493, 'Half Baked', false, '2019/05/08', 'book-vmx-245', 'http://dummyimage.com/700x665.png/cc0000/ffffff',
        'Proin risus. Praesent lectus. Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend.',
        1239.31, 82, 192);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (494, 'Identification Marks: None (Rysopis) ', true, '2021/01/20', 'book-pfz-464',
        'http://dummyimage.com/775x460.png/ff4444/ffffff',
        'Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis. Donec semper sapien a libero.',
        3516.55, 30, 313);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (495, 'State of Grace', true, '2020/11/30', 'book-fzc-579', 'http://dummyimage.com/314x645.png/5fa2dd/ffffff',
        'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci.',
        251.91, 17, 679);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (496, 'Simpsons: The Longest Daycare, The', false, '2019/02/20', 'book-xsu-771',
        'http://dummyimage.com/536x526.png/5fa2dd/ffffff',
        'Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien.',
        2330.33, 0, 852);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (497, 'Broken Trail', true, '2019/03/01', 'book-tqo-988', 'http://dummyimage.com/383x357.png/cc0000/ffffff',
        'Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.',
        3775.97, 23, 313);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (498, 'Where Eagles Dare', true, '2019/12/24', 'book-fdl-135', 'http://dummyimage.com/723x757.png/cc0000/ffffff',
        'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.',
        773.77, 29, 302);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (499, 'My Name Is Joe', false, '2019/04/07', 'book-klj-151', 'http://dummyimage.com/580x750.png/ff4444/ffffff',
        'Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus.',
        793.41, 24, 324);
insert into books (id, title, is_bestseller, pub_date, slug, image, description, price, discount, author_id)
values (500, 'September Issue, The', false, '2019/02/09', 'book-oux-713',
        'http://dummyimage.com/610x386.png/ff4444/ffffff',
        'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum.',
        4938.40, 20, 966);
