insert into book_file (id, hash, type_value, path, book_id)
values (1, 'tag-uu6-ooj', 'epub', 'Lady Takes a Chance, A.epub', 2);
insert into book_file (id, hash, type_value, path, book_id)
values (2, 'tag-7d2-fxp', 'fb2', 'Lady Takes a Chance, A.fb2', 2);
insert into book_file (id, hash, type_value, path, book_id)
values (3, 'tag-8ir-jix', 'pdf', 'Lady Takes a Chance, A.pdf', 2);
