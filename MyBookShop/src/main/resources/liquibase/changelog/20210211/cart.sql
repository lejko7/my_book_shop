create table cart
(
    id      BIGSERIAL not null,
    user_id numeric   not null,
    book_id numeric   not null
);

insert into cart (id, user_id, book_id)
values (1, 38, 336);
insert into cart (id, user_id, book_id)
values (2, 13, 251);
insert into cart (id, user_id, book_id)
values (3, 70, 282);
insert into cart (id, user_id, book_id)
values (4, 30, 415);
insert into cart (id, user_id, book_id)
values (5, 29, 359);
insert into cart (id, user_id, book_id)
values (6, 3, 140);
insert into cart (id, user_id, book_id)
values (7, 15, 391);
insert into cart (id, user_id, book_id)
values (8, 14, 101);
insert into cart (id, user_id, book_id)
values (9, 53, 141);
insert into cart (id, user_id, book_id)
values (10, 64, 389);
insert into cart (id, user_id, book_id)
values (11, 69, 126);
insert into cart (id, user_id, book_id)
values (12, 12, 214);
insert into cart (id, user_id, book_id)
values (13, 21, 115);
insert into cart (id, user_id, book_id)
values (14, 23, 39);
insert into cart (id, user_id, book_id)
values (15, 38, 422);
insert into cart (id, user_id, book_id)
values (16, 75, 185);
insert into cart (id, user_id, book_id)
values (17, 21, 19);
insert into cart (id, user_id, book_id)
values (18, 40, 280);
insert into cart (id, user_id, book_id)
values (19, 17, 343);
insert into cart (id, user_id, book_id)
values (20, 64, 444);
insert into cart (id, user_id, book_id)
values (21, 54, 112);
insert into cart (id, user_id, book_id)
values (22, 53, 28);
insert into cart (id, user_id, book_id)
values (23, 25, 231);
insert into cart (id, user_id, book_id)
values (24, 4, 118);
insert into cart (id, user_id, book_id)
values (25, 32, 443);
insert into cart (id, user_id, book_id)
values (26, 32, 318);
insert into cart (id, user_id, book_id)
values (27, 5, 126);
insert into cart (id, user_id, book_id)
values (28, 75, 405);
insert into cart (id, user_id, book_id)
values (29, 20, 162);
insert into cart (id, user_id, book_id)
values (30, 40, 475);
insert into cart (id, user_id, book_id)
values (31, 56, 479);
insert into cart (id, user_id, book_id)
values (32, 68, 44);
insert into cart (id, user_id, book_id)
values (33, 21, 56);
insert into cart (id, user_id, book_id)
values (34, 69, 173);
insert into cart (id, user_id, book_id)
values (35, 77, 440);
insert into cart (id, user_id, book_id)
values (36, 5, 144);
insert into cart (id, user_id, book_id)
values (37, 58, 385);
insert into cart (id, user_id, book_id)
values (38, 17, 490);
insert into cart (id, user_id, book_id)
values (39, 17, 411);
insert into cart (id, user_id, book_id)
values (40, 26, 217);
insert into cart (id, user_id, book_id)
values (41, 15, 355);
insert into cart (id, user_id, book_id)
values (42, 80, 438);
insert into cart (id, user_id, book_id)
values (43, 28, 399);
insert into cart (id, user_id, book_id)
values (44, 77, 180);
insert into cart (id, user_id, book_id)
values (45, 25, 80);
insert into cart (id, user_id, book_id)
values (46, 55, 231);
insert into cart (id, user_id, book_id)
values (47, 59, 224);
insert into cart (id, user_id, book_id)
values (48, 64, 45);
insert into cart (id, user_id, book_id)
values (49, 14, 418);
insert into cart (id, user_id, book_id)
values (50, 68, 403);
insert into cart (id, user_id, book_id)
values (51, 32, 217);
insert into cart (id, user_id, book_id)
values (52, 59, 66);
insert into cart (id, user_id, book_id)
values (53, 60, 45);
insert into cart (id, user_id, book_id)
values (54, 34, 440);
insert into cart (id, user_id, book_id)
values (55, 26, 42);
insert into cart (id, user_id, book_id)
values (56, 67, 130);
insert into cart (id, user_id, book_id)
values (57, 34, 338);
insert into cart (id, user_id, book_id)
values (58, 5, 498);
insert into cart (id, user_id, book_id)
values (59, 22, 148);
insert into cart (id, user_id, book_id)
values (60, 69, 38);
insert into cart (id, user_id, book_id)
values (61, 17, 467);
insert into cart (id, user_id, book_id)
values (62, 61, 42);
insert into cart (id, user_id, book_id)
values (63, 44, 174);
insert into cart (id, user_id, book_id)
values (64, 58, 54);
insert into cart (id, user_id, book_id)
values (65, 27, 113);
insert into cart (id, user_id, book_id)
values (66, 77, 492);
insert into cart (id, user_id, book_id)
values (67, 63, 263);
insert into cart (id, user_id, book_id)
values (68, 48, 192);
insert into cart (id, user_id, book_id)
values (69, 12, 79);
insert into cart (id, user_id, book_id)
values (70, 59, 210);
insert into cart (id, user_id, book_id)
values (71, 34, 416);
insert into cart (id, user_id, book_id)
values (72, 22, 443);
insert into cart (id, user_id, book_id)
values (73, 59, 195);
insert into cart (id, user_id, book_id)
values (74, 79, 265);
insert into cart (id, user_id, book_id)
values (75, 36, 48);
insert into cart (id, user_id, book_id)
values (76, 34, 140);
insert into cart (id, user_id, book_id)
values (77, 76, 71);
insert into cart (id, user_id, book_id)
values (78, 66, 219);
insert into cart (id, user_id, book_id)
values (79, 55, 303);
insert into cart (id, user_id, book_id)
values (80, 35, 326);
insert into cart (id, user_id, book_id)
values (81, 7, 420);
insert into cart (id, user_id, book_id)
values (82, 49, 392);
insert into cart (id, user_id, book_id)
values (83, 55, 26);
insert into cart (id, user_id, book_id)
values (84, 73, 55);
insert into cart (id, user_id, book_id)
values (85, 65, 200);
insert into cart (id, user_id, book_id)
values (86, 62, 412);
insert into cart (id, user_id, book_id)
values (87, 39, 372);
insert into cart (id, user_id, book_id)
values (88, 33, 159);
insert into cart (id, user_id, book_id)
values (89, 72, 35);
insert into cart (id, user_id, book_id)
values (90, 34, 255);
insert into cart (id, user_id, book_id)
values (91, 74, 132);
insert into cart (id, user_id, book_id)
values (92, 66, 450);
insert into cart (id, user_id, book_id)
values (93, 47, 473);
insert into cart (id, user_id, book_id)
values (94, 40, 351);
insert into cart (id, user_id, book_id)
values (95, 65, 432);
insert into cart (id, user_id, book_id)
values (96, 41, 62);
insert into cart (id, user_id, book_id)
values (97, 58, 347);
insert into cart (id, user_id, book_id)
values (98, 51, 87);
insert into cart (id, user_id, book_id)
values (99, 78, 164);
insert into cart (id, user_id, book_id)
values (100, 50, 422);
