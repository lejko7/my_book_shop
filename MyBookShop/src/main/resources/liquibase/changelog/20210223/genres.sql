insert into genres (id, genre, slug, genre_id)
values (1, 'Лёгкое чтение', 'genre_iap_koy', null);
insert into genres (id, genre, slug, genre_id)
values (2, 'Фантастика', 'genre_kea_3k8', 1);
insert into genres (id, genre, slug, genre_id)
values (3, 'Боевики', 'genre_xif_tlq', 1);
insert into genres (id, genre, slug, genre_id)
values (4, 'Детективы', 'genre_adg_xlc', 1);
insert into genres (id, genre, slug, genre_id)
values (5, 'Триллер', 'genre_bgu_w27', 4);
insert into genres (id, genre, slug, genre_id)
values (6, 'Крутой детектив', 'genre_tyw_1oz', 4);
insert into genres (id, genre, slug, genre_id)
values (7, 'Иронический детектив', 'genre_gzq_s3q', 4);
insert into genres (id, genre, slug, genre_id)
values (8, 'Про маньяков', 'genre_vtq_shh', 4);
insert into genres (id, genre, slug, genre_id)
values (9, 'Шпионский детектив', 'genre_vew_vzm', 4);
insert into genres (id, genre, slug, genre_id)
values (10, 'Криминальный детектив', 'genre_qdp_415', 4);
insert into genres (id, genre, slug, genre_id)
values (11, 'Классический детектив', 'genre_gnp_3qj', 4);
insert into genres (id, genre, slug, genre_id)
values (12, 'Политический детектив', 'genre_quw_mi9', 4);
insert into genres (id, genre, slug, genre_id)
values (13, 'Фентези', 'genre_roa_qpt', 1);
insert into genres (id, genre, slug, genre_id)
values (14, 'Романы', 'genre_kzz_6pb', 1);
insert into genres (id, genre, slug, genre_id)
values (15, 'Ужасы', 'genre_ykt_re6', 1);
insert into genres (id, genre, slug, genre_id)
values (16, 'Приключения', 'genre_rjz_rab', 1);
insert into genres (id, genre, slug, genre_id)
values (17, 'Серьёзное чтение', 'genre_uev_nhn', null);
insert into genres (id, genre, slug, genre_id)
values (18, 'Биографии', 'genre_ter_9t8', 17);
insert into genres (id, genre, slug, genre_id)
values (19, 'Драматургия', 'genre_xvt_6p5', null);
insert into genres (id, genre, slug, genre_id)
values (20, 'Античная драма', 'genre_miw_j5b', 19);
insert into genres (id, genre, slug, genre_id)
values (21, 'Комедия', 'genre_eyv_73h', 19);
insert into genres (id, genre, slug, genre_id)
values (22, 'Сценарий', 'genre_bkr_w2j', 19);
insert into genres (id, genre, slug, genre_id)
values (23, 'Драма,пьеса', 'genre_lid_tn3', 19);
insert into genres (id, genre, slug, genre_id)
values (24, 'Деловая литература', 'genre_amu_zqs', null);
insert into genres (id, genre, slug, genre_id)
values (25, 'Управление экономикой', 'genre_dwv_rl1', 24);
insert into genres (id, genre, slug, genre_id)
values (26, 'Карьера', 'genre_tek_746', 24);
insert into genres (id, genre, slug, genre_id)
values (27, 'Маркетинг, реклама, PR', 'genre_wda_z16', 24);
insert into genres (id, genre, slug, genre_id)
values (28, 'Финансы', 'genre_mll_w4m', 24);
insert into genres (id, genre, slug, genre_id)
values (29, 'Менеджмент', 'genre_qlj_cuz', 24);
insert into genres (id, genre, slug, genre_id)
values (30, 'Личные финансы', 'genre_mvb_mrb', 24);
insert into genres (id, genre, slug, genre_id)
values (31, 'Недвижимость', 'genre_jaa_an2', 24);
insert into genres (id, genre, slug, genre_id)
values (32, 'Стартапы', 'genre_lmj_ha3', 24);
insert into genres (id, genre, slug, genre_id)
values (33, 'Привлечение клиентов', 'genre_ten_xky', 24);
insert into genres (id, genre, slug, genre_id)
values (34, 'Ведение бизнеса', 'genre_hfb_j62', 24);
insert into genres (id, genre, slug, genre_id)
values (35, 'Бизнес-процессы', 'genre_nwi_qgy', 24);
insert into genres (id, genre, slug, genre_id)
values (36, 'Проектный менеджмент', 'genre_jdr_z03', 24);
insert into genres (id, genre, slug, genre_id)
values (37, 'Делопроизводство', 'genre_oas_pvr', 24);
insert into genres (id, genre, slug, genre_id)
values (38, 'Переговоры', 'genre_xvv_tsp', 24);
insert into genres (id, genre, slug, genre_id)
values (39, 'Малый бизнес', 'genre_ukx_g8f', 24);
insert into genres (id, genre, slug, genre_id)
values (40, 'Продажи', 'genre_jsh_u2y', 24);
