insert into tags (id, tag, slug)
values (1, 'современная литература', 'tag-uu6-ooj');
insert into tags (id, tag, slug)
values (2, 'классическая литература', 'tag-7d2-fxp');
insert into tags (id, tag, slug)
values (3, 'зарубежная литература', 'tag-8ir-jix');
insert into tags (id, tag, slug)
values (4, 'фэнтези', 'tag-r5p-ems');
insert into tags (id, tag, slug)
values (5, 'английская литература', 'tag-vna-wgs');
insert into tags (id, tag, slug)
values (6, 'русская литература', 'tag-s8l-tcy');
insert into tags (id, tag, slug)
values (7, 'американская литература', 'tag-zou-quw');
insert into tags (id, tag, slug)
values (8, 'фантастика', 'tag-y4o-qru');
insert into tags (id, tag, slug)
values (9, 'детская литература', 'tag-10t-ose');
insert into tags (id, tag, slug)
values (10, 'детектив', 'tag-ofb-zry');
insert into tags (id, tag, slug)
values (11, 'любовь', 'tag-lf1-qwa');
insert into tags (id, tag, slug)
values (12, 'мистика', 'tag-enl-kjs');
insert into tags (id, tag, slug)
values (13, 'юмор', 'tag-p3v-lof');
insert into tags (id, tag, slug)
values (14, 'приключения', 'tag-xdq-ohg');
insert into tags (id, tag, slug)
values (15, 'сказка', 'tag-qjz-xqe');
insert into tags (id, tag, slug)
values (16, 'французская литература', 'tag-o2i-lfz');
insert into tags (id, tag, slug)
values (17, 'англия', 'tag-h7n-jqn');
insert into tags (id, tag, slug)
values (18, 'художественная литература', 'tag-228-sdn');
insert into tags (id, tag, slug)
values (19, 'психология', 'tag-knj-mql');
insert into tags (id, tag, slug)
values (20, 'философия', 'tag-2m4-pzs');
insert into tags (id, tag, slug)
values (21, 'экранизовано', 'tag-gl6-khb');
insert into tags (id, tag, slug)
values (22, 'любимое', 'tag-l1h-pgp');
insert into tags (id, tag, slug)
values (23, 'антиутопия', 'tag-4ow-qfl');
insert into tags (id, tag, slug)
values (24, 'роман', 'tag-g9a-rgm');
insert into tags (id, tag, slug)
values (25, 'франция', 'tag-fxy-ojx');
insert into tags (id, tag, slug)
values (26, 'дайте две', 'tag-rsk-htz');
insert into tags (id, tag, slug)
values (27, 'война', 'tag-n4a-zcq');
insert into tags (id, tag, slug)
values (28, 'история', 'tag-l9d-lbq');
insert into tags (id, tag, slug)
values (29, 'детство', 'tag-atw-pmg');
insert into tags (id, tag, slug)
values (30, 'жизнь', 'tag-45p-eoo');
insert into tags (id, tag, slug)
values (31, 'драма', 'tag-roe-nwe');
insert into tags (id, tag, slug)
values (32, 'россия', 'tag-fhb-wsn');
insert into tags (id, tag, slug)
values (33, 'советская литература', 'tag-xom-mna');
insert into tags (id, tag, slug)
values (34, 'путешествие', 'tag-skx-vpd');
insert into tags (id, tag, slug)
values (35, 'триллер', 'tag-uxc-nlj');
insert into tags (id, tag, slug)
values (36, 'америка', 'tag-5vy-gha');
insert into tags (id, tag, slug)
values (37, 'вампиры', 'tag-yzq-igq');
insert into tags (id, tag, slug)
values (38, 'магия', 'tag-e71-rwx');
insert into tags (id, tag, slug)
values (39, 'биография', 'tag-09j-ool');
insert into tags (id, tag, slug)
values (40, 'star wars', 'tag-794-wvk');
