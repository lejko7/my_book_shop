package com.example.MyBookShop.service;

import com.example.MyBookShop.data.entities.Author;
import com.example.MyBookShop.data.entities.Book;
import com.example.MyBookShop.data.entities.Genre;
import com.example.MyBookShop.data.entities.Tag;
import com.example.MyBookShop.data.repositories.AuthorRepository;
import com.example.MyBookShop.data.repositories.BookRepository;
import com.example.MyBookShop.data.repositories.GenreRepository;
import com.example.MyBookShop.data.repositories.TagRepository;
import com.example.MyBookShop.web.dto.BookDto;
import com.example.MyBookShop.web.dto.BooksPageDto;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class BookServiceImplTest {

    ResourceStorage resourceStorage;
    BookService bookService;
    @MockBean
    BookRepository bookRepository;
    @MockBean
    GenreRepository genreRepository;
    @MockBean
    TagRepository tagRepository;
    @MockBean
    AuthorRepository authorRepository;
    @MockBean
    ConvertEntityToDtoStrategy convertEntityToDtoStrategy;
    Pageable pageable;

    @Autowired
    public BookServiceImplTest(ResourceStorage resourceStorage,
                               BookService bookService) {
        this.resourceStorage = resourceStorage;
        this.bookService = bookService;
    }

    @BeforeEach
    void setUp() {

        pageable = PageRequest.of(0, 10);
    }

    @AfterEach
    void tearDown() {

        pageable = null;
    }

    @Test
    void getBooks() {

        Set<BookDto> books = bookService.getBooks();

        assertNotNull(books);

        Mockito.verify(bookRepository, Mockito.times(1)).findAll();
    }

    @Test
    void getPaginatedBooks() {

        Page<Book> books = new PageImpl<>(Collections.emptyList());

        Mockito.doReturn(books).when(bookRepository).findAll(pageable);

        BooksPageDto paginatedBooks = bookService.getPaginatedBooks(pageable);

        assertNotNull(paginatedBooks);

        assertNotNull(paginatedBooks.getBooks());
        assertEquals(paginatedBooks.getCount(), 0);

        Mockito.verify(bookRepository, Mockito.times(1)).findAll(pageable);
        Mockito.verify(convertEntityToDtoStrategy, Mockito.times(1)).convert(Mockito.anyList());
    }

    @Test
    void getBooksByAuthor() {

        List<Book> booksMock = Collections.emptyList();

        Mockito.doReturn(booksMock).when(bookRepository).findBooksByAuthorNameContaining(anyString());

        Set<BookDto> books = bookService.getBooksByAuthor(anyString());

        assertNotNull(books);

        assertEquals(books.size(), 0);

        Mockito.verify(bookRepository, Mockito.times(1)).findBooksByAuthorNameContaining(anyString());
        Mockito.verify(convertEntityToDtoStrategy, Mockito.times(1)).convert(Mockito.anyList());
    }

    @Test
    void getBooksByTitle() {

        List<Book> booksMock = Collections.emptyList();

        Mockito.doReturn(booksMock).when(bookRepository).findBooksByTitleContaining(anyString());

        Set<BookDto> books = bookService.getBooksByTitle(anyString());

        assertNotNull(books);

        assertEquals(books.size(), 0);

        Mockito.verify(bookRepository, Mockito.times(1)).findBooksByTitleContaining(anyString());
        Mockito.verify(convertEntityToDtoStrategy, Mockito.times(1)).convert(Mockito.anyList());
    }

    @Test
    void getBooksByPriceBetween() {

        List<Book> booksMock = Collections.emptyList();

        Mockito.doReturn(booksMock).when(bookRepository).findByPriceBetween(anyDouble(), anyDouble());

        Set<BookDto> books = bookService.getBooksByPriceBetween(anyDouble(), anyDouble());

        assertNotNull(books);

        assertEquals(books.size(), 0);

        Mockito.verify(bookRepository, Mockito.times(1)).findByPriceBetween(anyDouble(), anyDouble());
        Mockito.verify(convertEntityToDtoStrategy, Mockito.times(1)).convert(Mockito.anyList());
    }

    @Test
    void getBooksByPrice() {

        List<Book> booksMock = Collections.emptyList();

        Mockito.doReturn(booksMock).when(bookRepository).findBooksByPrice(anyDouble());

        Set<BookDto> books = bookService.getBooksByPrice(anyDouble());

        assertNotNull(books);

        assertEquals(books.size(), 0);

        Mockito.verify(bookRepository, Mockito.times(1)).findBooksByPrice(anyDouble());
        Mockito.verify(convertEntityToDtoStrategy, Mockito.times(1)).convert(Mockito.anyList());
    }

    @Test
    void getBestsellers() {

        List<Book> booksMock = Collections.emptyList();

        Mockito.doReturn(booksMock).when(bookRepository).getBookBestsellers();

        Set<BookDto> books = bookService.getBestsellers();

        assertNotNull(books);

        assertEquals(books.size(), 0);

        Mockito.verify(bookRepository, Mockito.times(1)).getBookBestsellers();
        Mockito.verify(convertEntityToDtoStrategy, Mockito.times(1)).convert(Mockito.anyList());
    }

    @Test
    void getBooksWithMaxDiscount() {

        List<Book> booksMock = Collections.emptyList();

        Mockito.doReturn(booksMock).when(bookRepository).getBooksWithMaxDiscount();

        Set<BookDto> books = bookService.getBooksWithMaxDiscount();

        assertNotNull(books);

        assertEquals(books.size(), 0);

        Mockito.verify(bookRepository, Mockito.times(1)).getBooksWithMaxDiscount();
        Mockito.verify(convertEntityToDtoStrategy, Mockito.times(1)).convert(Mockito.anyList());
    }

    @Test
    void findBooksByFilter() {

        List<Book> booksMock = Collections.emptyList();

        Mockito.doReturn(booksMock).when(bookRepository).findBooksByTitleContaining(anyString(), eq(pageable));
        Mockito.doReturn(booksMock).when(bookRepository).findBooksByTitleContaining(anyString());

        BooksPageDto paginatedBooks = bookService.findBooksByFilter("anyString", pageable);

        assertNotNull(paginatedBooks);

        assertNotNull(paginatedBooks.getBooks());
        assertEquals(paginatedBooks.getCount(), 0);

        Mockito.verify(bookRepository, Mockito.times(1)).findBooksByTitleContaining(anyString(), eq(pageable));
        Mockito.verify(bookRepository, Mockito.times(1)).findBooksByTitleContaining(anyString());
        Mockito.verify(convertEntityToDtoStrategy, Mockito.times(1)).convert(Mockito.anyList());
    }

    @Test
    void getRecentPaginatedBooks() {

        LocalDate localDateStart = LocalDate.now();
        Date date = Date.valueOf(localDateStart);

        List<Book> booksMock = Collections.emptyList();

        Mockito.doReturn(booksMock).when(bookRepository).findBooksByPublishDateBetween(eq(date), eq(date), eq(pageable));

        BooksPageDto paginatedBooks = bookService.getRecentPaginatedBooks(localDateStart, localDateStart, pageable);

        assertNotNull(paginatedBooks);

        assertNotNull(paginatedBooks.getBooks());
        assertEquals(paginatedBooks.getCount(), 0);

        Mockito.verify(bookRepository, Mockito.times(1)).findBooksByPublishDateBetween(eq(date), eq(date), eq(pageable));
        Mockito.verify(convertEntityToDtoStrategy, Mockito.times(1)).convert(Mockito.anyList());
    }

    @Test
    void getPopularPaginatedBooks() {

        List<Book> booksMock = Collections.emptyList();

        Mockito.doReturn(booksMock).when(bookRepository).findBooksByDownloadCountGreaterThan(anyInt(), eq(pageable));
        Mockito.doReturn(booksMock).when(bookRepository).findBooksByDownloadCountGreaterThan(anyInt());

        BooksPageDto paginatedBooks = bookService.getPopularPaginatedBooks(pageable);

        assertNotNull(paginatedBooks);

        assertNotNull(paginatedBooks.getBooks());
        assertEquals(paginatedBooks.getCount(), 0);

        Mockito.verify(bookRepository, Mockito.times(1)).findBooksByDownloadCountGreaterThan(anyInt(), eq(pageable));
        Mockito.verify(bookRepository, Mockito.times(1)).findBooksByDownloadCountGreaterThan(anyInt());
        Mockito.verify(convertEntityToDtoStrategy, Mockito.times(1)).convert(Mockito.anyList());
    }

    @Test
    void getBooksByGenre() {

        List<Book> booksMock = Collections.emptyList();
        Genre genre = new Genre();

        Mockito.doReturn(booksMock).when(bookRepository).findBooksByBookToGenresIn(anyList(), eq(pageable));
        Mockito.doReturn(genre).when(genreRepository).getBySlugEquals(anyString());

        BooksPageDto paginatedBooks = bookService.getBooksByGenre(anyString(), pageable);

        assertNotNull(paginatedBooks);

        assertNotNull(paginatedBooks.getBooks());
        assertEquals(paginatedBooks.getCount(), 0);

        Mockito.verify(bookRepository, Mockito.times(1)).findBooksByBookToGenresIn(anyList(), eq(pageable));
        Mockito.verify(genreRepository, Mockito.times(1)).getBySlugEquals(anyString());
        Mockito.verify(convertEntityToDtoStrategy, Mockito.times(1)).convert(Mockito.anyList());
    }

    @Test
    void getBooksByTag() {

        List<Book> booksMock = Collections.emptyList();
        Tag genre = new Tag();

        Mockito.doReturn(booksMock).when(bookRepository).findBooksByBookToTagsIn(anyList(), eq(pageable));
        Mockito.doReturn(genre).when(tagRepository).findTagBySlugContaining(anyString());

        BooksPageDto paginatedBooks = bookService.getBooksByTag(anyString(), pageable);

        assertNotNull(paginatedBooks);

        assertNotNull(paginatedBooks.getBooks());
        assertEquals(paginatedBooks.getCount(), 0);

        Mockito.verify(bookRepository, Mockito.times(1)).findBooksByBookToTagsIn(anyList(), eq(pageable));
        Mockito.verify(tagRepository, Mockito.times(1)).findTagBySlugContaining(anyString());
        Mockito.verify(convertEntityToDtoStrategy, Mockito.times(1)).convert(Mockito.anyList());
    }

    @Test
    void getBookBySlug() {

        Book bookMock = new Book();

        Mockito.doReturn(bookMock).when(bookRepository).findBookBySlugContaining(anyString());

        BookDto book = bookService.getBookBySlug(anyString());

        assertNull(book);

        Mockito.verify(bookRepository, Mockito.times(1)).findBookBySlugContaining(anyString());
        Mockito.verify(convertEntityToDtoStrategy, Mockito.times(1)).convert(bookMock);
    }

    @Test
    void getAuthorBooksById() {

        List<Book> booksMock = Collections.emptyList();
        Author genre = new Author();

        Mockito.doReturn(booksMock).when(bookRepository).findBooksByAuthor(eq(genre), eq(pageable));
        Mockito.doReturn(genre).when(authorRepository).getOne(anyLong());

        BooksPageDto paginatedBooks = bookService.getAuthorBooksById(anyLong(), pageable);

        assertNotNull(paginatedBooks);

        assertNotNull(paginatedBooks.getBooks());
        assertEquals(paginatedBooks.getCount(), 0);

        Mockito.verify(bookRepository, Mockito.times(1)).findBooksByAuthor(eq(genre), eq(pageable));
        Mockito.verify(authorRepository, Mockito.times(1)).getOne(anyLong());
        Mockito.verify(convertEntityToDtoStrategy, Mockito.times(1)).convert(Mockito.anyList());
    }

    @Test
    void findBooksBySlugs() {

        List<String> cookieSlugs = Collections.emptyList();

        Mockito.doReturn(cookieSlugs).when(bookRepository).findBooksBySlugIn(anyList());

        Set<BookDto> books = bookService.findBooksBySlugs(anyList());

        assertNotNull(books);

        Mockito.verify(bookRepository, Mockito.times(1)).findBooksBySlugIn(anyList());
        Mockito.verify(convertEntityToDtoStrategy, Mockito.times(1)).convert(anyList());
    }
}
