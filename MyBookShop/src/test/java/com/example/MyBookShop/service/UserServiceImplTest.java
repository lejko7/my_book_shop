package com.example.MyBookShop.service;

import com.example.MyBookShop.data.entities.User;
import com.example.MyBookShop.data.repositories.UserRepository;
import com.example.MyBookShop.security.RegistrationForm;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@AutoConfigureMockMvc
class UserServiceImplTest {

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    @Value("${auth.secret}")
    String authSecret;
    RegistrationForm registrationForm;
    @MockBean
    UserRepository userRepositoryMock;

    @Autowired
    public UserServiceImplTest(UserService userService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    @BeforeEach
    void setUp() {

        registrationForm = new RegistrationForm();
        registrationForm.setEmail("look@look.look");
        registrationForm.setLogin("look");
        registrationForm.setPhone("88005553535");
        registrationForm.setPassword("look");
    }

    @AfterEach
    void tearDown() {
        registrationForm = null;
    }

    @Test
    void registration() {

        User user = userService.registration(registrationForm);

        assertNotNull(user);

        assertTrue(passwordEncoder.matches(registrationForm.getPassword(), user.getPassword()));
        assertTrue(CoreMatchers.is(user.getPhone()).matches(registrationForm.getPhone()));
        assertTrue(CoreMatchers.is(user.getLogin()).matches(registrationForm.getLogin()));
        assertTrue(CoreMatchers.is(user.getEmail()).matches(registrationForm.getEmail()));

        Mockito.verify(userRepositoryMock, Mockito.times(1)).saveAndFlush(Mockito.any(User.class));
    }

    @Test
    void verifyAuthSecret() {
        assertThat(authSecret, Matchers.containsString("box"));
    }

}
