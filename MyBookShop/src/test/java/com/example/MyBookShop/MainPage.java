package com.example.MyBookShop;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MainPage {

    private final String url = "http://localhost:8085/";
    private final ChromeDriver driver;

    public MainPage(ChromeDriver driver) {
        this.driver = driver;
    }

    public MainPage callMainPage() {
        driver.get(url);
        return this;
    }

    public MainPage callPageClickByLinkText(String link) {
        WebElement element = driver.findElement(By.linkText(link));
        element.click();
        return this;
    }

    public MainPage callPageClickById(String link) {
        WebElement element = driver.findElement(By.id(link));
        element.click();
        return this;
    }


    public MainPage pause() throws InterruptedException {
        Thread.sleep(2000);
        return this;
    }

    public MainPage setUpToken(String elementId, String token) {
        WebElement element = driver.findElement(By.id(elementId));
        element.sendKeys(token);

        return this;
    }

    public MainPage acceptFormSubmit(String link) {
        WebElement element = driver.findElement(By.id(link));
        element.submit();
        return this;
    }
}
