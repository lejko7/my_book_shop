package com.example.MyBookShop.web.controllers;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class AuthorControllerTest {

    private final MockMvc mockMvc;

    @Autowired
    public AuthorControllerTest(MockMvc mockMvc) {
        this.mockMvc = mockMvc;
    }

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getSlugAuthors() throws Exception {
        mockMvc.perform(get("/authors/50"))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().string(containsString("Biddleston Tobye")));
    }

    @Test
    void authorsPage() throws Exception {
        mockMvc.perform(get("/authors"))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().string(containsString("Allflatt Brook")))
                .andExpect(xpath("/html/body/div/div/main/div/div/div[2]/div/div[3]/a")
                        .string("Apark Madelyn"));
    }
}
