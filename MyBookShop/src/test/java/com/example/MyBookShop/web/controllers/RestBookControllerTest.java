package com.example.MyBookShop.web.controllers;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class RestBookControllerTest {

    private final MockMvc mockMvc;

    @Autowired
    public RestBookControllerTest(MockMvc mockMvc) {
        this.mockMvc = mockMvc;
    }

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void recommendedBooks() throws Exception {
        mockMvc.perform(get("/api/books/recommendedBooks")
                .param("page", "0")
                .param("size", "10"))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.count", is(10)));
    }

    @Test
    void recentBooks() throws Exception {
        mockMvc.perform(get("/api/books/recentBooks")
                .param("from", "10.10.2010")
                .param("to", "10.10.2030")
                .param("page", "0")
                .param("size", "20"))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.books", hasSize(20)));
    }

    @Test
    void popularBooks() throws Exception {
        mockMvc.perform(get("/api/books/popularBooks")
                .param("page", "0")
                .param("size", "10"))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.books", hasSize(10)));
    }

    @Test
    void booksByAuthorName() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/books/author/or"))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$", hasSize(28)))
                .andReturn();

        String content = result.getResponse().getContentAsString();

        assertThat(content, containsString("id"));
        assertThat(content, containsString("title"));
        assertThat(content, containsString("slug"));
        assertThat(content, containsString("image"));
        assertThat(content, containsString("description"));
        assertThat(content, containsString("publishDate"));
        assertThat(content, containsString("discount"));
        assertThat(content, containsString("discountPrice"));
        assertThat(content, containsString("price"));
        assertThat(content, containsString("downloadCount"));
        assertThat(content, containsString("author"));
        assertThat(content, containsString("fullName"));
        assertThat(content, containsString("genres"));
        assertThat(content, containsString("tags"));
        assertThat(content, containsString("bookFiles"));
        assertThat(content, containsString("rates"));
        assertThat(content, containsString("comments"));
        assertThat(content, containsString("rateCount"));
        assertThat(content, containsString("bestseller"));
    }

    @Test
    void authorBooksPage() throws Exception {
        mockMvc.perform(get("/api/books/authors/25")
                .param("page", "0")
                .param("size", "20"))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.books", hasSize(1)));
    }

    @Test
    void booksByTitle() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/books/title/op"))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$", hasSize(8)))
                .andReturn();

        String content = result.getResponse().getContentAsString();

        assertThat(content, containsString("id"));
        assertThat(content, containsString("title"));
        assertThat(content, containsString("slug"));
        assertThat(content, containsString("image"));
        assertThat(content, containsString("description"));
        assertThat(content, containsString("publishDate"));
        assertThat(content, containsString("discount"));
        assertThat(content, containsString("discountPrice"));
        assertThat(content, containsString("price"));
        assertThat(content, containsString("downloadCount"));
        assertThat(content, containsString("author"));
        assertThat(content, containsString("fullName"));
        assertThat(content, containsString("genres"));
        assertThat(content, containsString("tags"));
        assertThat(content, containsString("bookFiles"));
        assertThat(content, containsString("rates"));
        assertThat(content, containsString("comments"));
        assertThat(content, containsString("rateCount"));
        assertThat(content, containsString("bestseller"));
    }

    @Test
    void booksByPriceBetween() throws Exception {
        mockMvc.perform(get("/api/books/priceBetween")
                .param("minPrice", "800")
                .param("maxPrice", "1500"))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$", hasSize(77)));
    }

    @Test
    void booksByPrice() throws Exception {
        mockMvc.perform(get("/api/books/price/885.01")
                .param("page", "0")
                .param("size", "10"))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$", hasSize(1)));
    }

    @Test
    void booksWithMaxDiscount() throws Exception {
        mockMvc.perform(get("/api/books/maxDiscount"))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$", hasSize(6)));
    }

    @Test
    void testBooksByPrice() throws Exception {
        mockMvc.perform(get("/api/books/bestsellers"))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$", hasSize(260)));
    }

    @Test
    void getGenres() throws Exception {
        mockMvc.perform(get("/api/books/genre/genre_iap_koy")
                .param("page", "0")
                .param("size", "5"))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.books", hasSize(5)));
    }

    @Test
    void searchPage() throws Exception {
        mockMvc.perform(get("/api/books/search/page/op")
                .param("page", "0")
                .param("size", "10"))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.count", is(8)));
    }

    @Test
    void getTagsBook() throws Exception {
        mockMvc.perform(get("/api/books/tag/tag-enl-kjs")
                .param("page", "0")
                .param("size", "10"))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.books", hasSize(10)));
    }
}
