package com.example.MyBookShop;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class SeleniumPagesTest {

    private static ChromeDriver driver;

    @BeforeAll
    static void setUp() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().pageLoadTimeout(5, TimeUnit.SECONDS);
    }

    @AfterAll
    static void tearDown() {
        driver.quit();
    }

    @Test
    public void testMainPageAccess() throws InterruptedException {
        MainPage mainPage = new MainPage(driver);
        mainPage
                .callMainPage()
                .pause();

        assertTrue(driver.getPageSource().contains("BOOKSHOP"));
    }

    @Test
    public void testMainPageSearchByQuery() throws InterruptedException {
        MainPage mainPage = new MainPage(driver);

        mainPage
                .callMainPage()
                .pause()
                .setUpToken("query", "Shoppen")
                .pause()
                .acceptFormSubmit("search")
                .pause();

        assertTrue(driver.getPageSource().contains("Shoppen"));
    }

    @Test
    public void testFollowingOnLink() throws InterruptedException {
        MainPage mainPage = new MainPage(driver);

        mainPage
                .callMainPage()
                .pause()
                .callPageClickById("toGenres")
                .pause()
                .callPageClickById("toNews")
                .pause()
                .callPageClickById("toPopular")
                .pause()
                .callPageClickById("toAuthors")
                .pause()
                .callPageClickById("toMain")
                .pause();
    }

    @Test
    public void testBookComment() throws InterruptedException {
        MainPage mainPage = new MainPage(driver);

        mainPage
                .callMainPage()
                .pause()
                .callPageClickByLinkText("Hail Caesar");

        assertFalse(driver.getPageSource().contains("Написать отзыв"));

        mainPage
                .callPageClickById("signin")
                .pause()
                .callPageClickById("mailtypeauth")
                .pause()
                .setUpToken("mail", "me@me.me")
                .pause()
                .callPageClickById("sendauth")
                .pause()
                .setUpToken("mailcode", "123456")
                .pause()
                .callPageClickById("toComeInMail")
                .pause()
                .callMainPage()
                .pause();

        assertTrue(driver.getPageSource().contains("grow"));

        mainPage
                .callPageClickByLinkText("Hail Caesar")
                .pause();

        assertTrue(driver.getPageSource().contains("Отправить отзыв"));

        mainPage
                .setUpToken("review", "I'm glad to read this book!")
                .pause()
                .acceptFormSubmit("send_comment_button")
                .pause()
                .pause();

        driver.navigate().refresh();

        assertTrue(driver.getPageSource().contains("I'm glad to read this book!"));

    }
}
